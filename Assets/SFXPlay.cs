﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlay : MonoBehaviour {


	public AudioClip[] coinsCollectedSFX;
	public AudioClip[] swipeSFX;
	public AudioClip[] jumpSFX;
	public AudioClip[] slideSFX;

	AudioSource source;

	// Use this for initialization
	void Start () {

		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayCoinSFX () {

		source.PlayOneShot (coinsCollectedSFX [(Random.Range(0, coinsCollectedSFX.Length -1))]);

	}

	public void SlideSFX () {

		source.PlayOneShot (coinsCollectedSFX [(Random.Range(0, slideSFX.Length -1))]);

	}

	public void JumpSFX () {

		source.PlayOneShot (coinsCollectedSFX [(Random.Range(0, jumpSFX.Length -1))]);

	}

	public void SwipeSFX () {

		source.clip = coinsCollectedSFX [(Random.Range (0, swipeSFX.Length - 1))];
		source.Play ();

	}

}
