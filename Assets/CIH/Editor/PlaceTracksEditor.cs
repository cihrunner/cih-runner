﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PlaceTrack))]
public class PlaceTracksEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		PlaceTrack myTarget = (PlaceTrack)target;

		if (GUILayout.Button ("Work On Track")) {
			myTarget.PlaceCurrentTrack ();

		}

		if (GUILayout.Button ("Create prefab")) {
			myTarget.CreateNewTrackPrefab ();

		}

		//EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
	}
}