﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ChangeEnvironementVisual))]
public class ChangeEnvironementVisualEditor : Editor
{

	int toolbarInt = 0;
	string[] toolbarStrings = {"Edit visual", "Add Obstacles"};

    public override void OnInspectorGUI()
    { 
		 
        ChangeEnvironementVisual myTarget = (ChangeEnvironementVisual)target;

		GUILayout.Space (15f);

		toolbarInt = GUILayout.Toolbar(toolbarInt, toolbarStrings);

		if (toolbarInt == 0) {

			GUILayout.Space (25f);


            if (GUILayout.Button ("Change to mosquee")) {
				myTarget.ChangeToMosquee ();


                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);

			if (GUILayout.Button ("Change To Normal")) {

				myTarget.ChangeToNormal ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);



			if (GUILayout.Button ("Change To Sphere")) {

				myTarget.ChangeToSphere ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);


			if (GUILayout.Button ("Change To Twin")) {

				myTarget.ChangeToTween ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);


			if (GUILayout.Button ("Change To Colonial")) {

				myTarget.ChangeToColonial ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);


			if (GUILayout.Button ("Change To Bank")) {

				myTarget.ChangeToBank ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);


			if (GUILayout.Button ("Change To Mosquee fena")) {

				myTarget.ChangeToFena ();
                myTarget.ResetEnvirenementContainer();
            }

			GUILayout.Space (3f);


			if (GUILayout.Button ("Change To Aganou gate")) {

				myTarget.ChangeToAganouGate ();
                myTarget.ResetEnvirenementContainer();

            }

            if (GUILayout.Button("Change To CheckPoint marakech"))
            {

                myTarget.ChangeToCheckPointM();
                myTarget.ResetEnvirenementContainer();

            }

            if (GUILayout.Button("Change To Kotobia"))
            {

                myTarget.ChangeToKotobia();
                myTarget.ResetEnvirenementContainer();

            }

            if (GUILayout.Button("Change To Scene Marakech"))
            {

                myTarget.ChangeToSceneMarakech();
                myTarget.ResetEnvirenementContainer();

            }
            GUILayout.Space (15f);

		}



		if (toolbarInt == 1) {

			GUILayout.Space (25f);

			if (GUILayout.Button ("Add jump fence")) {
				myTarget.AddObstacle (1);

			}

			GUILayout.Space (3f);

			if (GUILayout.Button ("Add slide fence")) {

				myTarget.AddObstacle (2);
			}

			GUILayout.Space (3f);



			if (GUILayout.Button ("Add robot")) {

				myTarget.AddObstacle (3);

			}

			GUILayout.Space (3f);


			if (GUILayout.Button ("Add tram type 1")) {

				myTarget.AddObstacle (4);
			}

			GUILayout.Space (3f);


			if (GUILayout.Button ("Add tram type 2")) {

				myTarget.AddObstacle (5);
			}


			GUILayout.Space (3f);

			if (GUILayout.Button ("Add coin group")) {

				myTarget.AddObstacle (6);
			}

			GUILayout.Space (3f);

			if (GUILayout.Button ("Add wall")) {

				myTarget.AddObstacle (7);
			}

			GUILayout.Space (3f);

			if (GUILayout.Button ("Add fence jump coins")) {

				myTarget.AddObstacle (8);
			}

			GUILayout.Space (3f);

			if (GUILayout.Button ("Add bus")) {

				myTarget.AddObstacle (9);
			}

			GUILayout.Space (3f);

			if (GUILayout.Button ("Add taxi")) {

				myTarget.AddObstacle (10);
			}

		}
			


        //EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
    }
}