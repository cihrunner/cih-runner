﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ObjectPlacement))]
public class ObjectPlacementEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		GUILayout.Space (25f);

		ObjectPlacement myTarget = (ObjectPlacement)target;

		if (GUILayout.Button ("Place Object")) {
			myTarget.PlaceObjectOnTrack ();
		}

		GUILayout.Space (10f);

		GUILayout.BeginHorizontal ();

		if (GUILayout.Button ("Left lane")) {
			myTarget.PlaceLeft ();

		}

		if (GUILayout.Button ("Middle lane")) {
			myTarget.PlaceMiddle ();

		}

		if (GUILayout.Button ("Right lane")) {
			myTarget.PlaceRight ();

		}


		GUILayout.EndHorizontal ();

		//EditorGUILayout.LabelField("Level", myTarget.Level.ToString());
	}
}