﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour, IObstacleMove {

    public float speed;
    [HideInInspector]
    public float currentSpeed;
    [HideInInspector]
    public bool Movable = false;
    float timer;


    private void Start()
    {
        timer = 5f;
        currentSpeed = 0;
    }

    
    public IEnumerator SpeedDown()
    {
        
        while (speed > 0)
        {
            speed -= 0.1f;
            yield return new WaitForSeconds(.3f);
        }
        if(speed == 0)
        {
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ExitTrigger" || other.gameObject.name == "EnterTrigger")
        {
            speed = 0;
            Movable = false;
        }
        
    }

    private void FixedUpdate()
    {
        
        transform.position += new Vector3(0f, 0f, currentSpeed * Time.deltaTime);
        
    }

    public void MoveObstacle()
    {
        currentSpeed = speed;
    }
}
