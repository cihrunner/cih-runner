﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine.UI;
using TMPro;
using System;
using GameSparks.Api;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine;

public class FaceBookSdkManager : MonoBehaviour {

    public TextMeshProUGUI FriendsText;
    public GameObject DialogLoggedIn;
    public GameObject DialogLoggedOut;
    public GameObject DialogProfil;

    public Transform LeaderBoardContainer;


    public TextMeshProUGUI dialogUserName;
    public Image dialogProfilePicture;
    public TextMeshProUGUI ScoreDebug;

    public float offset;

    private void Awake()
    {
        //logoutGo.SetActive(false);
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    Debug.Log("Couldn't initialize");
                }
            },
            isGameShown =>
            {
                if (!isGameShown)
                {
                    Time.timeScale = 0;
                }
                else
                {
                    Time.timeScale = 1;
                }
            }

            );
        }
        else
        {
            FB.ActivateApp();
        }

        dealWithFbMenus(FB.IsLoggedIn);
    }


    #region Login / LogOut


    public void FaceBookLogin()
    {
        if (DataHolder.instance.AreYouSure.activeSelf)
        {
            var permissions = new List<string>();
            permissions.Add("public_profile");

            FB.LogInWithReadPermissions(permissions, AuthCallback);
        }
        else
        {
            DataHolder.instance.OpenAreYouSure();
            DataHolder.mydelegate = FaceBookLogin;
        }

    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            new FacebookConnectRequest()
                .SetAccessToken(aToken.TokenString)
                .Send((response) => {
                    string authToken = response.AuthToken;
                    string displayName = response.DisplayName;
                    bool? newPlayer = response.NewPlayer;
                    GSData scriptData = response.ScriptData;
                    AuthenticationResponse._Player switchSummary = response.SwitchSummary;
                    string userId = response.UserId;
                });


            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            SetScoreOnEvent();
            GetLeaderBoard();
            //  logoutGo.SetActive(true);
        }
        else
        {
            Debug.Log("User cancelled login");
        }

        dealWithFbMenus(FB.IsLoggedIn);
    }

    //void GamesparksAuthCallBack(AuthenticationResponse response)
    //{
    //    if(response.Errors != null)
    //    {
    //        string authToken = response.AuthToken;
    //        string displayName = response.DisplayName;
    //        bool? newPlayer = response.NewPlayer;
    //        GSData scriptData = response.ScriptData;
    //        AuthenticationResponse._Player switchSummary = response.SwitchSummary;
    //        string userId = response.UserId;
    //    }
    //}

    public void FacebookLogout()
    {
        if (DataHolder.instance.AreYouSure.activeSelf)
        {
            FB.LogOut();
            GS.Reset();
            Debug.Log("LOGOUT" + FB.IsLoggedIn);

            dealWithFbMenus(FB.IsLoggedIn);
        }
        else
        {
            DataHolder.instance.OpenAreYouSure();
            DataHolder.mydelegate = FacebookLogout;
        }
        // logoutGo.SetActive(false);
    }

    #endregion

    #region share

    public void FacebookShare()
    {
        FB.ShareLink(new System.Uri("https://www.altplay.ma/"), "Check It out", "hello google", new System.Uri("https://www.altplay.ma/"));
    }

    #endregion

    #region Invites

    public void FacebookGameRequest()
    {
        FB.AppRequest("Hey come join us in the journey", title: "altplay");
    }



    public void GetFriendsPlayingThisGame()
    {
        string query = "/me/friends";
        FB.API(query, HttpMethod.GET, result =>
        {
            var dictionnary = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);
            var friendsList = (List<object>)dictionnary["data"];
            FriendsText.text = string.Empty;
            foreach (var dict in friendsList)
            {
                FriendsText.text += ((Dictionary<string, object>)dict)["name"];
            }
        });
    }


    #endregion

    #region ui handling functions
    public void ExitProfil()
    {
        DialogProfil.SetActive(false);
        if (LeaderBoardContainer.childCount >= 1)
        {
            for (int i = 1; i < LeaderBoardContainer.childCount; i++)
            {
                GameObject.Destroy(LeaderBoardContainer.GetChild(i));
            }
        }
    }

    public void OpenProfil()
    {
        DialogProfil.SetActive(true);
    }


    void dealWithFbMenus(bool isLoggedIn)
    {
        if (isLoggedIn)
        {
            DialogLoggedIn.SetActive(true);
            DialogLoggedOut.SetActive(false);
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUserName);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePicture);
        }
        else
        {
            DialogLoggedIn.SetActive(false);
            DialogLoggedOut.SetActive(true);
            GS.Reset();
            
        }
    }

    void DisplayUserName(IResult result)
    {
         if(result.Error == null)
        {
            dialogUserName.text = "Hi, " + result.ResultDictionary["first_name"];
        }
        else
        {
            Debug.Log(result.Error);
        }
    }

    void DisplayProfilePicture(IGraphResult result)
    {
        if (result.Texture != null)
        {
            dialogProfilePicture.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
        }
        else
        {
            Debug.Log(result.Error);
        }
    }
    #endregion

    #region score Handling

    public void SetScoreOnEvent()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LEADERBOARD_SCORER").SetEventAttribute("SCORER", (int)PlayerPrefs.GetFloat("MoneyAvailable")).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Score Posted Successfully...");
            }
            else
            {
                Debug.Log("Error Posting Score...");
            }
        });
    }


    #endregion

    #region leaderBoard

    public void GetLeaderBoard()
    {
        new GameSparks.Api.Requests.LeaderboardDataRequest().SetLeaderboardShortCode("HIGH_SCORE").SetEntryCount(100).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Found Leaderboard Data...");
                
                foreach (LeaderboardDataResponse._LeaderboardData entry in response.Data)
                {
                    
                    int rank = (int)entry.Rank;
                    var thePlayer = LeaderBoardContainer.GetChild(rank - 1);
                    thePlayer.GetChild(1).GetComponent<Text>().text = rank.ToString();
                    string playerName = entry.UserName;
                    thePlayer.GetChild(0).GetComponent<Text>().text = playerName;
                    string score = entry.JSONData["SCORER"].ToString();
                    thePlayer.GetChild(2).GetComponent<Text>().text = score;

                    Debug.Log("Rank:" + rank + " Name:" + playerName + " \n Score:" + score);
                }
            }
            else
            {
                Debug.Log("Error Retrieving Leaderboard Data...");
            }
        });

    }

    #endregion


}
