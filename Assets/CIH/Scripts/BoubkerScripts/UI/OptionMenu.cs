﻿using UnityEngine.UI;
using UnityEngine;

public class OptionMenu : MonoBehaviour {

    [SerializeField] private Sprite muted, notmuted;
    [SerializeField] private Image muteImg;
    [SerializeField] private GameObject option;
    public GameObject apropos;

    private void Start()
    {
        muteImg.sprite = notmuted;
        Music.muted = false;
    }

    public void MuteSound()
    {
        AudioManager.Instance.muteeverything(muteImg, muted, notmuted);
    }

    public void CloseOption()
    {
        option.SetActive(false);
    }

    public void openOption()
    {
        option.SetActive(true);
    }

    public void OpenApropos()
    {
        apropos.SetActive(true);
    }

    public void CloseApropos()
    {
        apropos.SetActive(false);
    }
}
