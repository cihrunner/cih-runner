﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseCharacter : MonoBehaviour {

    //gender male = 0
    //gender Female = 1
    public GameObject CharactersSelection;
    public GameObject maleAlienInPWG, femaleAlienInPWG, maleInUI, FemaleInUI;
    public GameObject MaleIco, femaleIco;
    public Transform CuteRobot;

    public PlayerControls Pc;
    public GameObject firstTimePopUp, shadowmale, shadowfemale;


    private void Start()
    {
       
        Pc = Pc.GetComponent<PlayerControls>();
        if (!PlayerPrefs.HasKey("ChoosenGender"))
        {
            firstTimePopUp.SetActive(true);
           
        }
        if (PlayerPrefs.GetInt("ChoosenGender") == 0)
        {
            maleInUI.SetActive(true);
            FemaleInUI.SetActive(false);
            femaleAlienInPWG.SetActive(false);
            maleAlienInPWG.SetActive(true);
            MaleIco.SetActive(true);
            femaleIco.SetActive(false);
            CuteRobot.parent = maleAlienInPWG.transform;
            Pc.animator = maleAlienInPWG.GetComponent<Animator>();
        }
        else if (PlayerPrefs.GetInt("ChoosenGender") == 1)
        {
            maleInUI.SetActive(false);
            FemaleInUI.SetActive(true);
            femaleAlienInPWG.SetActive(true);
            maleAlienInPWG.SetActive(false);
            MaleIco.SetActive(false);
            femaleIco.SetActive(true);
            CuteRobot.parent = femaleAlienInPWG.transform;
            Pc.animator = femaleAlienInPWG.GetComponent<Animator>();
        }
    }

    public void OpenCharacterSelection()
    {
        CharactersSelection.SetActive(true);
    }

    public void CloseCharacterSelection()
    {
        CharactersSelection.SetActive(false);
    }

    public void SwitchToNext()
    {
        if (!PlayerPrefs.HasKey("ChoosenGender"))
        {
            PlayerPrefs.SetInt("ChoosenGender", 0);
        }
        else
        {
            if (PlayerPrefs.GetInt("ChoosenGender") == 1)
            {
                maleInUI.SetActive(true);
                FemaleInUI.SetActive(false);
                femaleAlienInPWG.SetActive(false);
                maleAlienInPWG.SetActive(true);
                MaleIco.SetActive(true);
                femaleIco.SetActive(false);
                CuteRobot.parent = maleAlienInPWG.transform;
                Pc.animator = maleAlienInPWG.GetComponent<Animator>();
                PlayerPrefs.SetInt("ChoosenGender", 0);
            }
            else if (PlayerPrefs.GetInt("ChoosenGender") == 0)
            {
                maleInUI.SetActive(false);
                FemaleInUI.SetActive(true);
                femaleAlienInPWG.SetActive(true);
                maleAlienInPWG.SetActive(false);
                MaleIco.SetActive(false);
                femaleIco.SetActive(true);
                CuteRobot.parent = femaleAlienInPWG.transform;
                Pc.animator = femaleAlienInPWG.GetComponent<Animator>();
                PlayerPrefs.SetInt("ChoosenGender", 1);
            }
        }
    }

    public void FirstTimeChoosingGender(int index)
    {
        if(index == 0)
        {
            //choose male
            shadowfemale.SetActive(false);
            shadowmale.SetActive(true);
            maleInUI.SetActive(true);
            FemaleInUI.SetActive(false);
            femaleAlienInPWG.SetActive(false);
            maleAlienInPWG.SetActive(true);
            MaleIco.SetActive(true);
            femaleIco.SetActive(false);
            CuteRobot.parent = maleAlienInPWG.transform;
            Pc.animator = maleAlienInPWG.GetComponent<Animator>();
            PlayerPrefs.SetInt("ChoosenGender", 0);
        }
        else
        {
            shadowfemale.SetActive(true);
            shadowmale.SetActive(false);
            maleInUI.SetActive(false);
            FemaleInUI.SetActive(true);
            femaleAlienInPWG.SetActive(true);
            maleAlienInPWG.SetActive(false);
            MaleIco.SetActive(false);
            femaleIco.SetActive(true);
            CuteRobot.parent = femaleAlienInPWG.transform;
            Pc.animator = femaleAlienInPWG.GetComponent<Animator>();
            PlayerPrefs.SetInt("ChoosenGender", 1);
        }

    }

    public void ExitFirstTimePopUp()
    {
        firstTimePopUp.SetActive(false);
    }
}
