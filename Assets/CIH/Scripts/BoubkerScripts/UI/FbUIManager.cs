﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FbUIManager : MonoBehaviour {

    public GameObject leaderBord;
    

    public void OpenLeaderBoard()
    {
        leaderBord.SetActive(true);
    }


    public void CloseLeaderBoard()
    {
        leaderBord.SetActive(false);
    }
}
