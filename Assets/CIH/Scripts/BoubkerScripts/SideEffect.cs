﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideEffect : MonoBehaviour {

    private static Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void StopRightEffectAnim()
    {
        animator.SetBool("GoRight", false);
        this.gameObject.SetActive(false);
    }
    public void StopLeftEffectAnim()
    {
        animator.SetBool("GoLeft", false);
        this.gameObject.SetActive(false);
    }

    public static void ActivateOnGoRight()
    {
        animator.SetBool("GoLeft", true);
    }

    public static void ActivateOnGoLeft()
    {
        animator.SetBool("GoRight", true);
    }
}
