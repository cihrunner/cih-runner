﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameAnalyticsSDK;
using UnityEngine;

public class ShopWorldMapController : MonoBehaviour {

    //shpop singeleton
    public static ShopWorldMapController instance;

    //UI stuff
    [Header("UI")]
    public Text NotEnoughCoins;
    public Text CurrentCoinAvailableForPlayer;
    public Text bgText;
    public GameObject WorldMapShop;
    public GameObject alienDancer, skinContainer, powerUpContainer;

    //player
    [Header("Player")]
    private float currentPlayerCoins;

    //power Ups && consomablew
    [Header("Consomable")]
    public List<Consomable> consomables;
    [Header("PowerUps")]
    public List<PowerUp> powerUps;

    


    private void Awake()
    {
        instance = GetComponent<ShopWorldMapController>();
        SetConsomableUpgrade();
        currentPlayerCoins = PlayerPrefs.GetFloat("MoneyAvailable");
        SetUI();
        //set EveryThing In his place

    }

    public void Upgrade(string itemName)
    {
        NotEnoughCoins.gameObject.SetActive(false);
        Consomable consomable;
        PowerUp power;

        if (GetPowerUpByName(itemName) == null)
        {
            var itemClicked = GetConsomableByName(itemName);
            consomable = itemClicked;
        }
        else
        {
            consomable = null;
        }
        if (GetConsomableByName(itemName) == null)
        {
            var itemClicked = GetPowerUpByName(itemName);
            power = itemClicked;
        }
        else
        {
            power = null;
        }

        //when click on button up grade
        //check player's current coin
        currentPlayerCoins = PlayerPrefs.GetFloat("MoneyAvailable");
        //handling consomable
        if (power == null && consomable != null)
        {
            int consomableLevel = PlayerPrefs.GetInt(consomable.playerPrefsItemLevel);
            if (consomable.MaxLevelReached())
            {
                return;
            }
            else if (currentPlayerCoins <= consomable.priceOfUpGradePerLevel[consomableLevel])
            {
                //remind the player that he don't have enough coins
                NotEnoughCoins.gameObject.SetActive(true);
                SetUI();
            }
            else if(!consomable.MaxLevelReached())
            {
                NotEnoughCoins.gameObject.SetActive(false);
                //subtract the price of the upgrade
                currentPlayerCoins -= consomable.priceOfUpGradePerLevel[consomableLevel];
                PlayerPrefs.SetFloat("MoneyAvailable", currentPlayerCoins);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                //update UI
                CurrentCoinAvailableForPlayer.text = currentPlayerCoins.ToString();
                consomable.UpgradeItem();
                SetUI();
                SetConsomableUpgrade();
                //To Get Info about the upgrades call the respective array at the level'eme element
            }            
            else
            {
                consomable.SetUIInMaxLevel();
            }

        }

        //handling Active item
        //handling consomable
        if (power != null && consomable == null)
        {
            int ActiveLevel = PlayerPrefs.GetInt(power.playerPrefsItemLevel);
            if (power.MaxLevelReached())
            {
                return;
            }
            else if (currentPlayerCoins <= power.priceOfUpGradePerLevel[ActiveLevel])
            {
                //remind the player that he don't have enough coins
                NotEnoughCoins.gameObject.SetActive(true);
                SetUI();
            }
            else if (!power.MaxLevelReached())
            {
                NotEnoughCoins.gameObject.SetActive(false);
                //subtract the price of the upgrade
                currentPlayerCoins -= power.priceOfUpGradePerLevel[ActiveLevel];
                PlayerPrefs.SetFloat("MoneyAvailable", currentPlayerCoins);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                //update UI
                CurrentCoinAvailableForPlayer.text = currentPlayerCoins.ToString();
                power.UpgradeItem();
                SetUI();
                
                //To Get Info about the upgrades call the respective array at the level'eme element
            }
            else
            {
                power.SetUIInMaxLevel();
            }

        }

    }

    void SetConsomableUpgrade()
    {
        foreach(var item in StarGameShopController.instance.itemsInStore)
        {
            foreach (var j in consomables)
            {
                if(item.PowerUpName == j.itemName)
                {
                   
                    item.MaxTobought = j.maxQuantityPerLevel[PlayerPrefs.GetInt(j.playerPrefsItemLevel)];
                    
                }
            }
        }
    }

    public void GoToSkinPage()
    {
        NotEnoughCoins.gameObject.SetActive(false);
        skinContainer.SetActive(true);
        alienDancer.SetActive(true);
        powerUpContainer.SetActive(false);
        //rightBtn.SetActive(false);
        //LeftBtn.SetActive(true);
        bgText.text = "Dance";
    }

    public void GotoPowerUpPage()
    {
        NotEnoughCoins.gameObject.SetActive(false);
        skinContainer.SetActive(false);
        alienDancer.SetActive(false);
        powerUpContainer.SetActive(true);
        //rightBtn.SetActive(true);
        //LeftBtn.SetActive(false);

        bgText.text = "Amélioration";
    }

    //getting a consomable by his name
    Consomable GetConsomableByName(string PowerUpName)
    {
        foreach (Consomable item in consomables)
        {
            if (item.itemName == PowerUpName)
            {
                return item;
            }
        }
        return null;
    }

    //getting a powerUp by his name
    public PowerUp GetPowerUpByName(string PowerUpName)
    {
        foreach (PowerUp item in powerUps)
        {
            if (item.itemName == PowerUpName)
            {
                return item;
            }
        }
        return null;
    }

    public void SetUI()
    {
        float currentPlayerCoins = PlayerPrefs.GetFloat("MoneyAvailable");
      //   CurrentCoinAvailableForPlayer.text = currentPlayerCoins.ToString();
        foreach (Consomable i in consomables)
        {
            i.SetUI();
        }
        foreach (PowerUp i in powerUps)
        {
            i.SetUI();
        }

    }

 
}

