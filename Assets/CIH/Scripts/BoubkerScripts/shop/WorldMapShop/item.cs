﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class item{

    [Header("Attributs")]
    public string itemName;
    public int maxLevel;
    public int levelToUnlock;
    [Header("Price Per Level")]
    public List<float> priceOfUpGradePerLevel;
    [TextArea]
    public string itemDescription;
    [Header("Player prefs keys")]
    public string playerPrefsItemcountKey;
    public string playerPrefsItemLockKey;
    public string playerPrefsItemLevel;
    //UI stuff
    [Header("UI")]
    public Image itemInShopIcon;
    public Sprite itemSprite;
    public Sprite itemSpriteLock;
    public Text itemNameText;
    public Text itemDescriptionText;
    public Image[] levelJauges;
    public Text itemPriceText;
    public Sprite LockSprite; //the lock sprite for the button
    public GameObject LockForPrice; // this is the price button


    //add Description to the respective item
    void SetUIDescription()
    {
        itemNameText.text = itemName;

        itemDescriptionText.text = itemDescription;
    }

    //adding levels to the respective item
    void SetLevelJaugesUI()
    {
        var currentLevel = PlayerPrefs.GetInt(playerPrefsItemLevel);
        if (currentLevel > maxLevel)
        {
            currentLevel = maxLevel;
        }
        if (PlayerPrefs.GetInt(playerPrefsItemLockKey) == 1)
        {
            return;
        }
        for (int i = 0; i < currentLevel; i++)
        {
            //activate jauge on the level
            levelJauges[i].gameObject.SetActive(true);
        }
    }

    //Updatting the price of the item
    void SetPriceUIByLevel()
    {
        var currentLevel = PlayerPrefs.GetInt(playerPrefsItemLevel);


        if (currentLevel >= maxLevel)
        {
            currentLevel = maxLevel;
            //stop the button from calling upgrade
            itemPriceText.text = "";
            itemPriceText.transform.parent.GetComponent<Button>().interactable = false;
        }
        else if (currentLevel < maxLevel)
        {

            itemPriceText.text = priceOfUpGradePerLevel[currentLevel].ToString();

        }

        //updating the UI To Next Level Price

    }

    //set the Lock
    void SetLockUI()
    {
        var locked = PlayerPrefs.GetInt(playerPrefsItemLockKey) == 1;
        if (locked)
        {
            itemInShopIcon.sprite = itemSpriteLock;
            LockForPrice.GetComponent<Image>().sprite = LockSprite;
            LockForPrice.GetComponent<Button>().interactable = false;
        }

    }

    public void SetUIInMaxLevel()
    {
        if (PlayerPrefs.GetInt(playerPrefsItemLevel) >= maxLevel)
        {
            LockForPrice.SetActive(false);
        }
    }

    public void SetUI()
    {
        SetLockUI();
        var locked = PlayerPrefs.GetInt(playerPrefsItemLockKey) == 1;
        if (!locked)
        {
            SetLevelJaugesUI();
            SetPriceUIByLevel();
            SetUIDescription();
            SetImageUI();
        }
    }

    public void UpgradeItem()
    {
        //Increment Level
        int currentLevel = PlayerPrefs.GetInt(playerPrefsItemLevel);
        currentLevel++;
        PlayerPrefs.SetInt(playerPrefsItemLevel, currentLevel);
        //Update UI
        SetUI();
    }

    public bool MaxLevelReached()
    {
        int currentLevel = PlayerPrefs.GetInt(playerPrefsItemLevel);
        if (currentLevel >= maxLevel)
        {
            return false;
        }

        return false;
    }

    void SetImageUI()
    {
        itemInShopIcon.sprite = itemSprite;
    }

}


[System.Serializable]
public class Consomable : item
{
    [Header("Player prefs keys")]
    public string QuantityPerLevelPlayerPrefsKey;
    public string TimesToUseInGamePlayerPrefsKey;
    [Header("Quantity per level")]
    public int[] maxQuantityPerLevel;
    [Header("Use Time per level")]
    public int[] timesToUseInGamePerLevel;
    

    void UpgradeConsomable()
    {
        int currentlevel = PlayerPrefs.GetInt(playerPrefsItemLevel);
        PlayerPrefs.SetInt(QuantityPerLevelPlayerPrefsKey, timesToUseInGamePerLevel[currentlevel]);
    }
}

[System.Serializable]
public class PowerUp : item
{
    [Header("Coolddown per level")]
    public float[] cooldownPerLevel;
    [Header("timer per level")]
    public float[] TimersPerLevel;

}
