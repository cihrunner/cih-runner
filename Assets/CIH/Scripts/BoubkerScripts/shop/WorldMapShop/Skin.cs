﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

[CreateAssetMenu]
public class Skin : ScriptableObject {

    public float price;
    public Material SkinMaterialMale;
    public Material SkinMaterialFemale;
    public Transform SkinDance;
	public Transform girlSkinDance;
    public Sprite lockedSprite;
    public Sprite UnlockedSprite;

    

}
