﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShopController : MonoBehaviour {

    public GameObject WorldMapShop;
    public GameObject MainUI;
    public Text PlayerCoinsAvailable;
    public GameObject powerupui;
    public GameObject skinUi;
    public GameObject dancer;

    Canvas worldmapcanvas;

	GameObject dataHolder;
	SkinShopController skinController;

    public void Awake()
    {
        worldmapcanvas = WorldMapShop.GetComponent<Canvas>();
        PlayerCoinsAvailable.text = ((int)PlayerPrefs.GetFloat("MoneyAvailable")).ToString();
    }

	void Start () {

		dataHolder = GameObject.Find ("DataHolder");
		skinController = dataHolder.GetComponent<SkinShopController> ();
	}

    public void Exit()
    {
        WorldMapShop.SetActive(false);
        MainUI.SetActive(true);
        dancer.SetActive(false);
        powerupui.SetActive(false);
        skinUi.SetActive(true);
        PlayerCoinsAvailable.text = ((int)PlayerPrefs.GetFloat("MoneyAvailable")).ToString();

		Destroy (GameObject.Find ("NewDancer"));
    }

    public void ActivateShop()
    {
       WorldMapShop.SetActive(true);
        dancer.SetActive(true);
        // MainUI.SetActive(false);
        ShopWorldMapController.instance.SetUI();
        PlayerCoinsAvailable.text = ((int)PlayerPrefs.GetFloat("MoneyAvailable")).ToString();

		skinController.OnOpeningShopFirstTime();
    }

}
