﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameAnalyticsSDK;
using UnityEngine;

public class SkinShopController : MonoBehaviour
{
    [Header("Skins Configuration")]
    public Skin[] SkinsAvaialble;
    [Header("UI")]
    public Button[] skinButtons;
    
    public GameObject notEnoughCoins;
    public GameObject AlienMale, AlienFemale;
    public Transform DancesHolder;
    public Transform DancePosition;
    public Text CoinsAvailableText;
   
    private float coinAvailable;

	Transform DanceClone;

	[HideInInspector] int genderIndex;


    private void Awake()
    {
        
        
        coinAvailable = PlayerPrefs.GetFloat("MoneyAvailable");
        notEnoughCoins.SetActive(false);
        CheckSkinAvailability();
        ChooseSkinToDisplay(PlayerPrefs.GetInt("SkinChoosen"));
        
    }

    public void OnOpeningShopFirstTime()
    {
        if (!PlayerPrefs.HasKey("SkinChoosen"))
        {
            PlayerPrefs.SetInt("SkinChoosen", 0);
            BuySkin(PlayerPrefs.GetInt("SkinChoosen"));
            
        }
        ChooseSkinToDisplay(PlayerPrefs.GetInt("SkinChoosen"));
    }



    private void OnEnable()
    {
        OnOpeningShopFirstTime();
        coinAvailable = PlayerPrefs.GetFloat("MoneyAvailable");
        notEnoughCoins.SetActive(false);
    }


    public void BuySkin(int index)
    {
        if (!PlayerPrefs.HasKey("HasSkin" + index))
        {
            PlayerPrefs.SetInt("HasSkin" + index, 0);
            if (coinAvailable < SkinsAvaialble[index].price)
            {
                //NotEnoughCoins
                notEnoughCoins.SetActive(true);
            }
            else
            {
                coinAvailable -= SkinsAvaialble[index].price;
                PlayerPrefs.SetFloat("MoneyAvailable", coinAvailable);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                //CoinsAvailableText.text = PlayerPrefs.GetFloat("MoneyAvailable").ToString();
                PlayerPrefs.SetInt("HasSkin" + index, 1);
                skinButtons[index].GetComponent<SetSkinUI>().buyButton.gameObject.SetActive(false);
                skinButtons[index].GetComponent<SetSkinUI>().skinButtonImage.sprite = SkinsAvaialble[index].UnlockedSprite;
                ChooseSkinToDisplay(index);
            }
        }
        else
        {
            if (PlayerPrefs.GetInt("HasSkin" + index) == 0)
            {
                if (coinAvailable < SkinsAvaialble[index].price)
                {
                    //NotEnoughCoins
                    notEnoughCoins.SetActive(true);
                }
                else
                {
                    coinAvailable -= SkinsAvaialble[index].price;
                    PlayerPrefs.SetFloat("MoneyAvailable", coinAvailable);
                    GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                    // CoinsAvailableText.text = PlayerPrefs.GetFloat("MoneyAvailable").ToString();
                    PlayerPrefs.SetInt("HasSkin" + index, 1);
                    skinButtons[index].GetComponent<SetSkinUI>().buyButton.gameObject.SetActive(false);
                    skinButtons[index].GetComponent<SetSkinUI>().skinButtonImage.sprite = SkinsAvaialble[index].UnlockedSprite;
                    ChooseSkinToDisplay(index);
                }
            }
           
        }

    }


    public void ChooseSkinToDisplay(int index)
    {

		Debug.Log ("ChooseSkinToDisplay");

		genderIndex = PlayerPrefs.GetInt ("ChoosenGender");

        if (PlayerPrefs.GetInt("HasSkin" + index) == 1)
        {
            if (DancesHolder.childCount > 1)
            {
                Destroy(DancesHolder.GetChild(1).gameObject);
            }


            changePlayerSkin(index);

			if (genderIndex == 0) {
				DanceClone = Instantiate (SkinsAvaialble [index].SkinDance);
			} else {
				DanceClone = Instantiate (SkinsAvaialble [index].girlSkinDance);
			}
            DanceClone.parent = DancesHolder;
            DanceClone.position = DancePosition.position;
            DanceClone.rotation = DancePosition.rotation;
            DanceClone.localScale = DancePosition.localScale;
			DanceClone.name = "NewDancer";

            skinButtons[index].interactable = false;
            for (int i = 0; i < skinButtons.Length; i++)
            {
                if (i != index && PlayerPrefs.GetInt("HasSkin" + index) == 1)
                {
                    skinButtons[i].interactable = true;
                }
            }

            PlayerPrefs.SetInt("SkinChoosen", index);

        }

       
    }

    void CheckSkinAvailability()
    {
        for (int i = 0; i < skinButtons.Length; i++)
        {
            if (PlayerPrefs.GetFloat("HasSkin" + i) == 0)
            {
                skinButtons[i].interactable = false;
            }
            else if (PlayerPrefs.GetFloat("HasSkin" + i) == 1)
            {
                skinButtons[i].interactable = true;
            }
            else if( i != PlayerPrefs.GetInt("SkinChoosen"))
            {
                skinButtons[i].interactable = true;
            }

            skinButtons[PlayerPrefs.GetInt("SkinChoosen")].interactable = false;
        }

    }


    void changePlayerSkin(int index)
    {
        AlienMale.GetComponent<SkinnedMeshRenderer>().material = SkinsAvaialble[index].SkinMaterialMale;
        AlienFemale.GetComponent<SkinnedMeshRenderer>().material = SkinsAvaialble[index].SkinMaterialFemale;
    }
   
}


