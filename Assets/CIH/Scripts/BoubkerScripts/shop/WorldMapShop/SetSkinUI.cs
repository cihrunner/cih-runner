﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SetSkinUI : MonoBehaviour {

    public SkinShopController SkinShopController;
    public Image skinButtonImage;
    public int skinNumber;
    public Text nameText;
    public Text Price;
    public Button buyButton;

    private void Awake()
    {
        SkinShopController = SkinShopController.GetComponent<SkinShopController>();
        skinButtonImage = GetComponent<Image>();
    }


    private void Start()
    {
       
        Price.text = SkinShopController.SkinsAvaialble[skinNumber].price.ToString();
        if(PlayerPrefs.GetInt("HasSkin" + skinNumber) == 0)
        {
            skinButtonImage.sprite = SkinShopController.SkinsAvaialble[skinNumber].lockedSprite;
        }
        else if (PlayerPrefs.GetInt("HasSkin" + skinNumber) == 1)
        {
            skinButtonImage.sprite = SkinShopController.SkinsAvaialble[skinNumber].UnlockedSprite;
            buyButton.gameObject.SetActive(false);
        }
        
        if(!PlayerPrefs.HasKey("HasSkin"+ skinNumber))
            skinButtonImage.sprite = SkinShopController.SkinsAvaialble[skinNumber].lockedSprite;
    }


}
