﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine;
using System.Globalization;

public class CreditController : MonoBehaviour
{

    public List<Credit> creditsAvailable;
    public Button creditButton;
    public Button ExitButton;
    public Sprite creditNotAvailableSprite, creditAvailableSprite;
    public GameObject CreditPopUp;
    public TextMeshProUGUI timer;

    private DateTime creditStartDate;
    private DateTime creditTopayTime;
    private DateTime lastPayTime;
    private DateTime CreditFinishDate;
    private int roundofReemboursment;


    public void Awake()
    {
        SetUI();
        setCreditTimers();
    }

    private void OnEnable()
    {
        setCreditTimers();
        SetUI();
    }

    void setCreditTimers()
    {
        if (PlayerPrefs.GetInt("CreditTaken") == 1)
        {
           
            creditStartDate = DateTime.Parse(PlayerPrefs.GetString("CreditDate"));
            Debug.Log(creditStartDate);
            CreditFinishDate = DateTime.ParseExact(PlayerPrefs.GetString("CreditFinishdate"), "o", CultureInfo.InvariantCulture); 
            roundofReemboursment = PlayerPrefs.GetInt("RoundOfReemboursement");
        }
    }

    //UI
    public void SetUI()
    {
        // for all player prefs 0 =false, 1= true
        if (!PlayerPrefs.HasKey("CreditTaken"))
        {
            PlayerPrefs.SetInt("CreditTaken", 0);
        }
        if (PlayerPrefs.GetInt("CreditTaken") == 1)
        {
            creditButton.interactable = false;
            creditButton.image.sprite = creditNotAvailableSprite;
            // show timer
        }
        else
        {
            creditButton.interactable = true;
            creditButton.image.sprite = creditAvailableSprite;
        }
    }

    public void GetCredit(string CreditName)
    {
        Credit myCredit = GetCreditByName(CreditName);
        //credit is available
        if (PlayerPrefs.GetInt("CreditTaken") == 0)
        {
            //setting player prefs
            timer.gameObject.SetActive(true);
            PlayerPrefs.SetInt("CreditTaken", 1);
            PlayerPrefs.SetString("CreditName", myCredit.creditName);
            float coinAvailable = PlayerPrefs.GetFloat("MoneyAvailable");
            coinAvailable += myCredit.topayglobal;
            PlayerPrefs.SetFloat("MoneyAvailable", coinAvailable);
            PlayerPrefs.SetString("CreditDate", System.DateTime.Now.ToString("o"));
            creditStartDate = Convert.ToDateTime(PlayerPrefs.GetString("CreditDate"));
            PlayerPrefs.SetFloat("ToReembourseTotal", myCredit.topayglobal);
            PlayerPrefs.SetFloat("ToReemboursePerDay", (float)(myCredit.topayglobal / myCredit.TimeToRembourseInDays));
            PlayerPrefs.SetFloat("ReembourseTime", myCredit.TimeToRembourseInDays);
            CreditFinishDate = creditStartDate.AddDays(myCredit.TimeToRembourseInDays);
            PlayerPrefs.SetString("CreditFinishdate", CreditFinishDate.ToString("o"));
            PlayerPrefs.SetInt("RoundsOfReemboursement", myCredit.TimeToRembourseInDays);
            PlayerPrefs.SetInt("RoundReemboursed",0);
            myCredit.getCreeditButton.interactable = false;
        }
    }

    Credit GetCreditByName(string CreditName)
    {
        Credit myCredit = new Credit();
        for (int i = 0; i < creditsAvailable.Count; i++)
        {
            if (CreditName == creditsAvailable[i].creditName)
            {
                myCredit = creditsAvailable[i];
            }
        }
        return myCredit;
    }

    public void OpenCreditPopUp()
    {
        if (PlayerPrefs.GetFloat("MoneyAvailable") == 0)
        {
            return;
        }
        CreditPopUp.SetActive(true);
        ExitButton.interactable = true;
        for (int i = 0; i < creditsAvailable.Count; i++)
        {
            
            creditsAvailable[i].SetTextForPopUp();
        }
    }

    public void ExitPopUp()
    {
        CreditPopUp.SetActive(false);
        ExitButton.interactable = false;

    }


    private void Update()
    {
        if (PlayerPrefs.GetInt("CreditTaken") == 1)
        {
            DateTime t1 = CreditFinishDate;
            DateTime t2 = DateTime.Now;
            TimeSpan ts = new TimeSpan(t1.Ticks - t2.Ticks);
            float coinAvailable = PlayerPrefs.GetFloat("MoneyAvailable");
            Credit myCredit = GetCreditByName(PlayerPrefs.GetString("CreditName"));
            float toPayInRound = PlayerPrefs.GetFloat("ToReemboursePerDay");

            timer.text = String.Format("{0}d{1}h{2}m", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
            for(int i=0; i<PlayerPrefs.GetInt("RoundsOfReemboursement"); i++)
            {
                DateTime ti = creditStartDate.AddDays(i);
                TimeSpan checkDue = new TimeSpan(ti.Ticks - t2.Ticks);
                if(checkDue.Ticks <= 0)
                {
                    
                    if(coinAvailable > toPayInRound * i && i>PlayerPrefs.GetInt("RoundReemboursed"))
                    {
                        
                        coinAvailable -= toPayInRound * (i- PlayerPrefs.GetInt("RoundReemboursed"));
                        PlayerPrefs.SetInt("RoundReemboursed", i);
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }

        }
        else
        {
            timer.gameObject.SetActive(false);
        }
    }
}
  

[System.Serializable]
public class Credit
{
    public string creditName;
    public int TimeToRembourseInDays;
    [Range(0,1)]
    public float percentage;
    [Range(0,1)]
    public float interetPourcentage;

    [HideInInspector]
    public float topayglobal, interetGlobal;
    //ui
    public TextMeshProUGUI coinsThatPlayerWillGet;
    public TextMeshProUGUI TimeRequired;
    public TextMeshProUGUI ToReembourseEveryHour;
    public Button getCreeditButton;

    public void SetTextForPopUp()
    {
        
        float topay = PlayerPrefs.GetFloat("MoneyAvailable");
        float interet = (topay * percentage) * interetPourcentage;
        topay = topay * percentage + interet;
        coinsThatPlayerWillGet.text = "credit de " + percentage + " d'un total de " + topay;
        TimeRequired.text = "le temp de reemboursement est " + TimeToRembourseInDays + "jours";
        ToReembourseEveryHour.text ="Chaque jour, " + topay / TimeToRembourseInDays + "sera débité de votre compte";
        topayglobal = topay;
        interetGlobal = interet;
        if(PlayerPrefs.GetInt("CreditTaken") == 0)
        {
            getCreeditButton.interactable = true;
        }

    }

    

}
