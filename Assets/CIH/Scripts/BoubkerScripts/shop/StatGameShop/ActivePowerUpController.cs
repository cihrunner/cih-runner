﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using GameAnalyticsSDK;

public class ActivePowerUpController : MonoBehaviour {

    public GameObject LazerButton;
    public GameObject bulletTimeButton;
    public GameObject G;
    public GameObject GlowWhenChoosenB, GlowWhenChoosenL;
    public float ChooseLazerPrice;
    public float chooseBulletTimePrice;
    public bool choosenL;
    public bool choosenB;

    //UI
    public Text PriceLazerText;
    public Text PriceBulletTimeText;
    public Text LazerNameText;
    public Text BulletTimeText;


    public void Awake()
    {
        SetUIActive();
        choosenL = false;
        choosenB = false;
        G.SetActive(false);
    }

    public void OnEnable()
    {
        SetUIActive();
        choosenL = false;
        choosenB = false;
    }

    public void SetUIActive()
    {
        PriceLazerText.text = ChooseLazerPrice.ToString();
        PriceBulletTimeText.text = chooseBulletTimePrice.ToString();
        LazerNameText.text = "Lazer";
        BulletTimeText.text = "BulletTime";

        G.GetComponent<Image>().enabled = false;
    }

   

    public void ChooseActive(string active)
    {
        float currentMoney = PlayerPrefs.GetFloat("MoneyAvailable");
 
        if (choosenB)
        {
            if (active == "BulletTime")
            {
                //refundBullet Time
                Debug.Log(choosenB);
                GlowWhenChoosenB.SetActive(false);
                currentMoney += chooseBulletTimePrice;
                PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                StarGameShopController.instance.Money.text = currentMoney.ToString();
                bulletTimeButton.SetActive(false);
                G.GetComponent<Image>().enabled = false;
                choosenB = false;
                AudioManager.Instance.PlaySound("PowerUpRemove");
            }
            else if (active == "Lazer")
            {
                //refund bullet time
                currentMoney += chooseBulletTimePrice;
                GlowWhenChoosenB.SetActive(false);
                bulletTimeButton.SetActive(false);
                AudioManager.Instance.PlaySound("PowerUpRemove");
                choosenB = false;
                //buy lazer
                if (currentMoney < ChooseLazerPrice)
                {
                    StarGameShopController.instance.notenoughCoins.gameObject.SetActive(true);
                }
                else
                {
                    G.GetComponent<Image>().enabled = true;
                    LazerButton.SetActive(true);
                    bulletTimeButton.SetActive(false);
                    currentMoney -= ChooseLazerPrice;
                    PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                    GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                    StarGameShopController.instance.Money.text =currentMoney.ToString();
                    choosenL = true;
                    GlowWhenChoosenL.SetActive(true);
                    AudioManager.Instance.PlaySound("PowerUpAdd");
                }
                
            }
            
        }
        else if (choosenL)
        {
            if (active == "Lazer")
            {
                //Refund lazer
                currentMoney += ChooseLazerPrice;
                GlowWhenChoosenL.SetActive(false);
                PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                StarGameShopController.instance.Money.text =currentMoney.ToString();
                LazerButton.SetActive(false);
                G.GetComponent<Image>().enabled = false;
                AudioManager.Instance.PlaySound("PowerUpRemove");
                choosenL = false;
            }
            else if (active == "BulletTime")
            {
                //refund Lazer
                currentMoney += ChooseLazerPrice;
                GlowWhenChoosenL.SetActive(false);
                PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                StarGameShopController.instance.Money.text = currentMoney.ToString();
                LazerButton.SetActive(false);
                G.GetComponent<Image>().enabled = false;
                AudioManager.Instance.PlaySound("PowerUpRemove");
                choosenL = false;
                //buy bulletTime
                if (currentMoney < chooseBulletTimePrice)
                {
                    StarGameShopController.instance.notenoughCoins.gameObject.SetActive(true);
                }
                else
                {
                    G.GetComponent<Image>().enabled = true;
                    LazerButton.SetActive(false);
                    bulletTimeButton.SetActive(true);
                    currentMoney -= chooseBulletTimePrice;
                    GlowWhenChoosenB.SetActive(true);
                    PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                    GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                    StarGameShopController.instance.Money.text = currentMoney.ToString();
                    choosenB = true;
                    AudioManager.Instance.PlaySound("PowerUpAdd");
                }
                
            }
        }
        else
        {
            if (active == "Lazer")
            {
                if (currentMoney < ChooseLazerPrice)
                {
                    StarGameShopController.instance.notenoughCoins.gameObject.SetActive(true);
                }
                else
                {
                    G.GetComponent<Image>().enabled = true;
                    LazerButton.SetActive(true);
                    bulletTimeButton.SetActive(false);
                    currentMoney -= ChooseLazerPrice;
                    PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                    GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                    StarGameShopController.instance.Money.text = currentMoney.ToString();
                    choosenL = true;
                    GlowWhenChoosenL.SetActive(true);
                    AudioManager.Instance.PlaySound("PowerUpAdd");
                }

            }
            else if (active == "BulletTime")
            {
                if (currentMoney < chooseBulletTimePrice)
                {
                    StarGameShopController.instance.notenoughCoins.gameObject.SetActive(true);
                }
                else
                {
                    G.GetComponent<Image>().enabled = true;
                    LazerButton.SetActive(false);
                    bulletTimeButton.SetActive(true);
                    currentMoney -= chooseBulletTimePrice;
                    GlowWhenChoosenB.SetActive(true);
                    PlayerPrefs.SetFloat("MoneyAvailable", currentMoney);
                    GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
                    StarGameShopController.instance.Money.text = currentMoney.ToString();
                    choosenB = true;
                    AudioManager.Instance.PlaySound("PowerUpAdd");
                }
            }
        }
        
    }
}
