﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameAnalyticsSDK;
using UnityEngine;

public class StarGameShopController : MonoBehaviour {

    //singleton starGameShopController
    public static StarGameShopController instance;

    public bool choosen;

    //To Control InGame State********************
    [Header("Player")]
    public static Item PowerUpActivated;
    public PlayerControls playerControls;
    public Image PowerUpIconInGame;
    public Text PowerUpCountInGame;
    GameObject player;
    PlayerControls controls;
    PowerUpController powerUpController;
    //***********************************************
    //To Control shop UI*****************************
    [Header("UI")]
    public Text Money;
    private float CurrentPlayerCoin;
    public GameObject notenoughCoins;
    public GameObject ShopUI;
    public Sprite ItemBoughtSprite;
    public Sprite itemNotBoughtSprite;
    public Image BackgroundImage;
    public Sprite LockedPowerUpSprite;
    [Header("Goods")]
    public List<Item> itemsInStore;
    //**********************************************

    public GameObject PauseUI;
    [Header("Containers")]
    public GameObject tracksContainer;
	public GameObject emptyLevel;


    private void Awake()
    {
        instance = GetComponent<StarGameShopController>();
        CurrentPlayerCoin = PlayerPrefs.GetFloat("MoneyAvailable");
        stopGame();
        SetMoneyOnUI();
        playerControls = playerControls.gameObject.GetComponent<PlayerControls>();
        SetPowerUpsUiText();
        powerUpController = GetComponent<PowerUpController>();
    }
    private void Start()
    {
        //Time.timeScale = 0;
        powerUpController.enabled = false;

    }
    //stop the start game
    public void stopGame()
    {
        playerControls.CurrentGameState = PlayerControls.GameState.Pause;
        ShopUI.SetActive(true);
        
    }

    //start game on click button 
    public void startGame()
    {
        
        playerControls.StartGame();
        ShopUI.SetActive(false);
        playerControls.enabled = true;
		playerControls.laneMovementBlocked = true;
        powerUpController.enabled = true;

        StartCoroutine ("ChangePlayerGameState");

		SetEmptyTrack ();

    }

    public void pauseGame()
    {
        Time.timeScale = 0;
        PauseUI.SetActive(true);
        controls.CurrentGameState = PlayerControls.GameState.Pause;
    }

    public void unpauseGame()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1;
        controls.CurrentGameState = PlayerControls.GameState.Playing;
    }

    

    //start Game wait coroutine
	IEnumerator ChangePlayerGameState () {

		yield return new WaitForEndOfFrame ();
		player = GameObject.Find ("Player Without GUI");
		controls = player.GetComponent<PlayerControls> ();

		yield return new WaitForSeconds(3f);

		controls.CurrentGameState = PlayerControls.GameState.Playing;
		controls.StartCharacterAnimations ();
		player.GetComponent<Rigidbody> ().isKinematic = false;
		controls.laneMovementBlocked = false;

	}
    
    IEnumerator CoinsAddAnimation(int coins, float time)
    {   
        for(int i=0; i<coins; i++)
        {
            int MoneyAvailable = (int)PlayerPrefs.GetFloat("MoneyAvailable");
            //set UI
            MoneyAvailable++;
            Money.text = MoneyAvailable.ToString();
            yield return new WaitForSeconds(time);
            //reset UI
        }            
    }

    //IEnumerator CoinsSubAnimation(int coins, float time)
    //{
    //    for (int i = coins; i > 0; i--)
    //    {
    //        int MoneyAvailable = (int)PlayerPrefs.GetFloat("MoneyAvailable");
    //        //set UI
    //        MoneyAvailable++;
    //        Money.text = "CIHCoins : " + MoneyAvailable;
    //        yield return new WaitForSeconds(time);
    //        //reset UI
    //    }
    //}

    //Refund item
    public void Refund(string powerUpName)
    {
        Item powerUp = GetPowerUpByName(powerUpName);
        float moneyAvailable = PlayerPrefs.GetFloat("MoneyAvailable");
        int powerUpCount = PlayerPrefs.GetInt(powerUp.playerPrefsItemCountKey);
        //if item is already bought and not locked
        if (powerUpCount > 0)
        {
            powerUpCount--;
            moneyAvailable += powerUp.price;
            CurrentPlayerCoin = moneyAvailable;
            //reset prefs
            //start coroutine to add coins on animation
            PlayerPrefs.SetFloat("MoneyAvailable", moneyAvailable);
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
            PlayerPrefs.SetInt(powerUp.playerPrefsItemCountKey, powerUpCount);
            SetPowerUpsUiText();
            SetMoneyOnUI();
            if (powerUpCount == 0)
            {
                //unlock others
                foreach (Item i in itemsInStore)
                {
                    i.locked = false;
                }
                UnlockPowerUpUI();
                choosen = false;
            }
        }
        
    }
    //buy item handler
    public void BuyItem(string powerUpName)
    {
        Item PowerUp = GetPowerUpByName(powerUpName);
        //parse btntext string to float
        var price = PowerUp.price;
        Debug.Log(price);
        //Check for coins, is the powerUp Locked ? and the Maximum PowerUps that the player can have
        if (price <= CurrentPlayerCoin && !PowerUp.locked && PowerUp.MaxTobought > PlayerPrefs.GetInt(PowerUp.playerPrefsItemCountKey) )
        {
            choosen = true;
            notenoughCoins.SetActive(false);
            //Lock wvery other item in order for the player to use only one
            foreach (Item powerUp in itemsInStore)
            {
                if(powerUp.PowerUpName != PowerUp.PowerUpName)
                    powerUp.locked = true;
            }
            //lock every other powerUp
            LockPowerUpUi();
            //Set the activated Power As the first item bought
            PowerUpActivated = PowerUp;
            PowerUp.bought = true;
            CurrentPlayerCoin -= price;
            //incrementing the PowerUp Comsumable count in his playerPrefs
            int itemBoughtCount = PlayerPrefs.GetInt(PowerUp.playerPrefsItemCountKey);
            itemBoughtCount++;
            PlayerPrefs.SetInt(PowerUp.playerPrefsItemCountKey, itemBoughtCount);
            ChangePowerUpInGame(PowerUp);
            SetPowerUpsUiText();
            Debug.Log(PlayerPrefs.GetInt(PowerUp.playerPrefsItemCountKey));
            // saving coin count state after buying item
            PlayerPrefs.SetFloat("MoneyAvailable", CurrentPlayerCoin);
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");
            SetMoneyOnUI();
            
        }
        else if (PowerUp.MaxTobought == PlayerPrefs.GetInt(PowerUp.playerPrefsItemCountKey))
        {
            //if max reached lock power up
            notenoughCoins.GetComponent<Text>().text = "You can't Buy more !";
            PowerUpActivated = PowerUp;
            ChangePowerUpInGame(PowerUp);
            notenoughCoins.SetActive(true);
            PowerUp.locked = true;

        }
        else if (PowerUp.locked)
        {
            notenoughCoins.GetComponent<Text>().text = "You can't Buy its locked!";
            notenoughCoins.SetActive(true);
        }
        else
        {
            //if not enough coin show notenoughcoins message
            notenoughCoins.SetActive(true);
        }
    }

	public void SetEmptyTrack () {

		GameObject newEmptyTrack = Instantiate (emptyLevel) as GameObject;
		newEmptyTrack.transform.SetParent (tracksContainer.transform);

	}



    //setting the current coin count in ui
    public void SetMoneyOnUI()
    {
        Money.text = ((int)CurrentPlayerCoin).ToString();
    }

    //add bought item to player prefs;
    public void AddItemToPlayerPrefs(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            int count = PlayerPrefs.GetInt(key);
            count++;
            PlayerPrefs.SetInt(key, count);
        }
        else
        {
            Debug.Log("Key Not Found");
        }
    }

    //change item background color on bought
    public void ChangeSprite(Item PowerUp)
    {
        if (PowerUp.bought)
        {
            PowerUp.backgroundImage.sprite = ItemBoughtSprite;
        }
    }

    // change evry Locked powerup sprite
    public void LockPowerUpUi()
    {
        foreach (Item item in itemsInStore)
        {
            if (item.locked)
            {
                item.LockedImage.sprite = item.LockedSprite;
            }
        }
    }

    public void UnlockPowerUpUI()
    {
        foreach (Item item in itemsInStore)
        {
            if (!item.locked)
            {
                item.LockedImage.sprite = item.InGameSprite;
            }
        }
    }

    //setting the powerup ui in Ui popUp
    void SetPowerUpsUiText()
    {
        foreach (Item item in itemsInStore)
        {
            item.SetUiText();
        }
    }

    //Set Power Up In Game Ui
    public void ChangePowerUpInGame(Item PowerUp)
    {

        powerUpController.powerUpButton.gameObject.SetActive(true);
    }

    //Getting PowerByName
    Item GetPowerUpByName(string PowerUpName)
    {
        foreach (Item item in itemsInStore)
        {
            if(item.PowerUpName == PowerUpName)
            {
                return item;
            }
        }
        return null;
    }


}


[System.Serializable]
public class Item
{
    [Header("Attributs")]
    public string PowerUpName;
    public int price;
    [HideInInspector]
    public bool bought;
    [HideInInspector]
    public int MaxTobought;
    public string playerPrefsItemCountKey;
    [HideInInspector]
    public bool locked;
    [Header("UI")]
    public Text priceText;
    public Text powerUpNameText;
    public Text CountText;
    public Image backgroundImage;
    public Image LockedImage;
    public Sprite InGameSprite;
    public Sprite LockedSprite;
    


    public void SetUiText()
    {
        priceText.text = price.ToString();
        powerUpNameText.text = PowerUpName;
        CountText.text = PlayerPrefs.GetInt(playerPrefsItemCountKey).ToString();
    }
}

