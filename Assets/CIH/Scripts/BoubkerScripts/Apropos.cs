﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apropos : MonoBehaviour {

    [SerializeField] private Transform PagesHolder;

	public void OpenPage(string pageName)
    {
        foreach (Transform child in PagesHolder)
        {
            if(child.name == pageName)
            {
                child.gameObject.SetActive(true);
            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }
    }
}
