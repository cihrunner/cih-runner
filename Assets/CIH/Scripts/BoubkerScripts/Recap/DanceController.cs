﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanceController : MonoBehaviour {

    public List<GameObject> MaleDancesGo, femaleDancesGo;
    public GameObject sadmaleAnim, sadFemaleAnim;
    public GameObject dh;
    private SkinShopController skinController;
    public Transform DanceHolder;
    public bool death;

    private void Awake()
    {
        skinController = dh.GetComponent<SkinShopController>();
        death = dh.GetComponent<OnLevelFinishSendData>().death;
    }

    void ChooseADanceToAnimate(List<GameObject> DancesGo, GameObject sadAnim)
    {
        int rand = PlayerPrefs.GetInt("SkinChoosen");

        
		if (death && DataHolder.NmberOfStars == 0)
        {
            AudioManager.Instance.PlayMusic("Music_recap_sad");
            GameObject danceCl = Instantiate(sadAnim);
            danceCl.transform.position = transform.position - Vector3.forward*.5f;
            danceCl.transform.parent = this.transform;
            danceCl.transform.localScale = DanceHolder.localScale * 3;
            if (PlayerPrefs.GetInt("ChoosenGender") == 1)
                danceCl.transform.GetChild(1).GetComponent<SkinnedMeshRenderer>().material = skinController.SkinsAvaialble[rand].SkinMaterialFemale;
            if (PlayerPrefs.GetInt("ChoosenGender") == 0)
                danceCl.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = skinController.SkinsAvaialble[rand].SkinMaterialMale;
        }
        else
        {
            GameObject danceClone = Instantiate(DancesGo[rand]);
            AudioManager.Instance.PlayMusic("Music_recap_happy");
            danceClone.transform.parent = this.transform;
            danceClone.transform.position = DanceHolder.position - Vector3.forward * 1.5f;
            danceClone.transform.localScale = DanceHolder.localScale;
        }
    }

    private void Start()
    {
        if(PlayerPrefs.GetInt("ChoosenGender") == 0)
        {
            ChooseADanceToAnimate(MaleDancesGo, sadmaleAnim);
        }
        else
        {
            ChooseADanceToAnimate(femaleDancesGo, sadFemaleAnim);
        }
       
    }
}
