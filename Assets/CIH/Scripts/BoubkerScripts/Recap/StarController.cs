﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StarController : MonoBehaviour {

    public int NumberOfStarsCollected;

    public static StarController instance;
    float coinCollected;
    float distance;
    public Text coinText;
    public Text DistanceCount;
    public Sprite YellowStar;
    public Sprite EmptyStar;
    public Text Win;
    public GameObject Recap;
    public GameObject InGameUI;
    public Image[] starsimage;
    public float timeBetweenStars;

	public GameObject animatedStar1;
	public GameObject animatedStar2;
	public GameObject animatedStar3;

	public GameObject[] animatedStar;
    
	public GameObject confettis;


    private void Awake()
    {
        instance = GetComponent<StarController>();
    }


    public void setRecapUI()
    {
        
        distance = DataHolder.instance.distance;
        NumberOfStarsCollected = DataHolder.NmberOfStars;
        coinCollected = DataHolder.instance.coinCount;

        coinCollected = DataHolder.instance.coinCount;
        distance = DataHolder.instance.distance;

        StartCoroutine(FillCounter((int)coinCollected, coinText, true));
        StartCoroutine(FillCounter((int)distance, DistanceCount, false));

        InGameUI.SetActive(false);

        if (NumberOfStarsCollected == 0)
        {
            StartCoroutine(ShowStarsOneByOne(NumberOfStarsCollected, timeBetweenStars));
            Win.text = "OOPS";
            
        }
        else if (NumberOfStarsCollected == 1)
        {
            StartCoroutine(ShowStarsOneByOne(NumberOfStarsCollected, timeBetweenStars));
            Win.text = "NOT BAD";
            
        }
        else if (NumberOfStarsCollected == 2)
        {
            StartCoroutine(ShowStarsOneByOne(NumberOfStarsCollected, timeBetweenStars));
            Win.text = "GOOD";
           
        }
        else if (NumberOfStarsCollected == 3)
        {
            StartCoroutine(ShowStarsOneByOne(NumberOfStarsCollected, timeBetweenStars));
            Win.text = "WOOOW!";
            
        }
    }


   

    IEnumerator FillCounter(int count, Text textToFill, bool withAudio)
    {
        yield return new WaitForSeconds(4f);
        
        for (int i = 0; i < count+1; i++)
        {
            if(i%20 == 0 && withAudio && count != 0)
            {
                AudioManager.Instance.PlaySound("RemplissageCoin");
            }
            textToFill.text = i.ToString();
         
            yield return new WaitForSeconds(.005f);
        }
        
        if (count <= 1)
        {
            textToFill.text = count.ToString();
        }
    }

    IEnumerator ShowStarsOneByOne(int starCount, float timeBetweenStars)
    {
        yield return new WaitForSeconds(4f);
        if (starCount != 0) 
        {
            for (int i = 0; i < starCount; i++)
            {
				/*
				if (starCount >= 1) {
					animatedStar1.SetActive (true);
				}

				if (starCount >= 2) {
					animatedStar2.SetActive (true);
				}

				if (starCount >= 3) {
					animatedStar3.SetActive (true);
				}
				*/

				if (starCount >= 3) {
					confettis.SetActive (true);
				}

				starsimage [i].sprite = YellowStar;
				animatedStar [i].SetActive (true);
                AudioManager.Instance.PlaySound("Etoile");
                yield return new WaitForSeconds(timeBetweenStars);
               
            }
        }
        
    }

}
