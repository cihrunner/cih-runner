﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneTransitionController : MonoBehaviour {

    private int PreviousLevelIndex;
    public int WorldMapBuildIndex;

	public GameObject map;
	public GameObject gameWorld;
	public GameObject recap;
	public GameObject shopUI;

	public GameObject player;
	public GameObject firstTrack;
	public GameObject playerCamera;

	public GameObject tracksContainer;

    private void Awake()
    {
        PreviousLevelIndex = DataHolder.instance.LevelIndex;
    }

    public void GoToWorldMap()
    {
        //SceneManager.LoadScene(WorldMapBuildIndex);
        AudioManager.Instance.PlayMusic("Music_WorldMap");
		ReinitializeWorld ();

		//Destroy (GameObject.Find ("WorldManager"));

		map.SetActive (true);
		recap.SetActive (false);
		gameWorld.SetActive (false);
		shopUI.SetActive (true);


        ReinitializePlayer ();
    }

	public void ReinitializePlayer () {

		player.SetActive (false);
		player.SetActive (true);

		//firstTrack.SetActive (true);
		recap.SetActive (false);

		playerCamera.GetComponent<Camera> ().enabled = true;
		player.transform.position = new Vector3 (player.transform.position.x, player.transform.position.y, firstTrack.transform.position.z - 20f);

		player.GetComponent<PlayerControls> ().CurrentGameState = PlayerControls.GameState.Stopped;

}

	public void ReinitializeWorld () {

		// Deleting the tracks that have not been travalled

		GameObject[] tracks = GameObject.FindGameObjectsWithTag ("Track");

		tracksContainer.transform.gameObject.Clear ();

		//GameObject world = GameObject.Find ("World");
		//GameObject currentWorldManager = world.transform.Find ("WorldManager").gameObject;

		//Destroy (currentWorldManager);


	}

    public void RetryLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
       //AudioManager.Instance.PlayMusic("Music_WorldMap");
    }
}

static public class GameObjectExtensions
{    
	static public void Clear(this GameObject go)
	{
		for (int i = go.transform.childCount - 1; i >= 0; i--)
		{
			GameObject.Destroy(go.transform.GetChild(i).gameObject);
		}
	}   
}