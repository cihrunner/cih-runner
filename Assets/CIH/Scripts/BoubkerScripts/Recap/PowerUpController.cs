﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PowerUpController : MonoBehaviour {

    public PlayerPoweUpsUGUI playerPowerUpsController;
    private StarGameShopController shopController;
    public PlayerControls pc;
    //public GameObject InGamePowerUpUI;
    public Image bar;

	public Button powerUpButton;
	public Sprite[] powerUpButtonSprites;

	public GameObject shield;
	public GameObject speedUpFX;
	public GameObject magnetFX;

    private void Awake()
    {
        playerPowerUpsController = playerPowerUpsController.GetComponent<PlayerPoweUpsUGUI>();
        shopController = GetComponent<StarGameShopController>();
      //  InGamePowerUpUI.SetActive(false);
        pc = pc.GetComponent<PlayerControls>();
    }

	void Start () {
	
		ChooseButtonSprite ();

	}
	
    public static bool IsDoubleTap()
    {
        bool result = false;
        float MaxTimeWait = 1;
        float VariancePosition = 1;
        
        if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            float DeltaTime = Input.GetTouch(0).deltaTime;
            float DeltapositionLength = Input.GetTouch(0).deltaPosition.magnitude;
            if(DeltaTime>0 && DeltaTime<MaxTimeWait && DeltapositionLength < VariancePosition)
            {
                result = true;
            }
        }
        return result;
    }

    private void Update()
    {
        
        //In Adnroid mode 
//        if (IsDoubleTap() && StarGameShopController.instance.choosen && pc.CurrentGameState == PlayerControls.GameState.Playing)
//        {
//            //activate chosen powerUp
//            if(playerPowerUpsController.CurrentPowerState == PlayerPoweUpsUGUI.State.None)
//                UsePowerUp();
            
//        }
//        //In Editor Mode for test 
//#if UNITY_EDITOR
//        if (Input.GetButtonDown("Fire1") && StarGameShopController.instance.choosen && pc.CurrentGameState == PlayerControls.GameState.Playing)
//        {
//            if(playerPowerUpsController.CurrentPowerState == PlayerPoweUpsUGUI.State.None)
//                 UsePowerUp();

//        }
//#endif
    }

	public void UsePowerUp()
    {
       
        //activate chosen powerUp
        if (StarGameShopController.PowerUpActivated.PowerUpName == "Magnet" && PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey)>0 )
        {
            //use Magnete from controller
            playerPowerUpsController.ActivateState(PlayerPoweUpsUGUI.State.CoinMagnet);
            //show UI
            //InGamePowerUpUI.SetActive(true);
            //desactivate inGameUI PowerUp after 3seconds
            Invoke("DesactivateInGamePowerUpUI", playerPowerUpsController.CoinMagnetDuration);
            //coroutine fill bar
			StartCoroutine(fillbar(playerPowerUpsController.GetConsomable("Magnet"),  0));
            //reduce count of the respective powerUP
            ReduceCountValueInPrefs(StarGameShopController.PowerUpActivated);
            Debug.Log("CoinMagnete activated");
            shopController.ChangePowerUpInGame(StarGameShopController.PowerUpActivated);
            if (PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey) == 0)
            {
                powerUpButton.gameObject.SetActive(false);
            }
        }
        else if (StarGameShopController.PowerUpActivated.PowerUpName == "Invincibilité" && PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey) > 0)
        {
            //use Invincibilite
            playerPowerUpsController.ActivateState(PlayerPoweUpsUGUI.State.Invincible);
            //coroutine fill bar
            StartCoroutine(fillbar(playerPowerUpsController.GetConsomable("Invincibilité"), 1));
            //reduce count of the respective powerUP
            ReduceCountValueInPrefs(StarGameShopController.PowerUpActivated);
            //activate inGame ui 
            //InGamePowerUpUI.SetActive(true);

            //desactivate inGameUI PowerUp after 3seconds
            Invoke("DesactivateInGamePowerUpUI", playerPowerUpsController.InvincibleDuration);
            if (PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey) == 0)
            {
                powerUpButton.gameObject.SetActive(false);
            }
        }
        else if (StarGameShopController.PowerUpActivated.PowerUpName == "Accélération" && PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey) > 0)
        {
            //use CoinPlus
            playerPowerUpsController.ActivateState(PlayerPoweUpsUGUI.State.FastRun);
            //coroutine fill bar
            StartCoroutine(fillbar(playerPowerUpsController.GetConsomable("Accélération"), 2));
            //reduce count of the respective powerUP
            ReduceCountValueInPrefs(StarGameShopController.PowerUpActivated);
            //show UI
           // InGamePowerUpUI.SetActive(true);
            //desactivate inGameUI PowerUp after 3seconds
            StartCoroutine(AudioManager.Instance.PitchUpMusic(1.5f));
            Invoke("DesactivateInGamePowerUpUI", playerPowerUpsController.FastRunDuration);
            Invoke("PitchDesactivateUp", playerPowerUpsController.FastRunDuration);
            if (PlayerPrefs.GetInt(StarGameShopController.PowerUpActivated.playerPrefsItemCountKey) == 0)
            {
                powerUpButton.gameObject.SetActive(false);
            }
        }

    }



    private void ReduceCountValueInPrefs(Item item)
    {
        int newCount = PlayerPrefs.GetInt(item.playerPrefsItemCountKey);
        newCount--;
        PlayerPrefs.SetInt(item.playerPrefsItemCountKey, newCount);
    }

    void DesactivateInGamePowerUpUI()
    {
       // InGamePowerUpUI.SetActive(false);
    }

    void PitchDesactivateUp()
    {
        StartCoroutine(AudioManager.Instance.PitchDownMusic(1));
    }

    IEnumerator fillbar(float time, int powerIndex)
    {
        yield return new WaitForEndOfFrame();
        float totalRechargeTime = time;
        while (totalRechargeTime >= 0)
        {
            //bar.gameObject.SetActive(true);
            totalRechargeTime -= Time.deltaTime;
            //bar.fillAmount = totalRechargeTime / time;
            yield return new WaitForEndOfFrame();

            if (powerIndex == 1)
            {
                shield.SetActive(true);
            }

            if (powerIndex == 2)
                speedUpFX.SetActive(true);

            if (powerIndex == 0)
                magnetFX.SetActive(true);
        }

        shield.SetActive (false);
		speedUpFX.SetActive (false);
		magnetFX.SetActive (false);
		Debug.Log ("End of power");
    }
  
	void ChooseButtonSprite () {

		if (StarGameShopController.PowerUpActivated.PowerUpName == "Magnet") {
			DefineButtonSprite (0);
		}
		else if (StarGameShopController.PowerUpActivated.PowerUpName == "Invincibilité") {
			DefineButtonSprite (1);
		}
		else if (StarGameShopController.PowerUpActivated.PowerUpName == "Accélération") {
			DefineButtonSprite (2);
		}
		

	}

	void DefineButtonSprite (int spriteIndex) {

		powerUpButton.GetComponent<Image>().sprite = powerUpButtonSprites [spriteIndex];
		//powerUpButton.GetComponent<Image> ().SetNativeSize ();


	}
   
}
