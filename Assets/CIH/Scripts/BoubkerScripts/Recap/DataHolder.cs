﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DataHolder : MonoBehaviour {

    public static DataHolder instance;

    public delegate void AreYouSureAboutThat();
    public static AreYouSureAboutThat mydelegate;

    public GameObject AreYouSure;
    static bool sure;

    [HideInInspector]
    public int LevelIndex;
    [HideInInspector]
    public static int NmberOfStars;
    [HideInInspector]
    public int WorldIndex;
    [HideInInspector]
    public float coinCount;
    [HideInInspector]
    public float palier1Coins;
    [HideInInspector]
    public float palier2Coins;
    [HideInInspector]
    public float palier3Coins;
    [HideInInspector]
    public float distance;
    [HideInInspector]
    public bool CLetter = false;
    [HideInInspector]
    public bool ILetter = false;
    [HideInInspector]
    public bool HLetter = false;


    private void Awake()
    {
        QualitySettings.SetQualityLevel(5, true);
        Time.timeScale = 1;
        //get data holder component
        instance = GetComponent<DataHolder>();
        
        //let the instance to not restart on reloading or going to another scene
       // DontDestroyOnLoad(instance);
    }

    public void ResetAllPlayerPrefs()
    {
        
        if (!AreYouSure.activeSelf)
        {
            AudioManager.Instance.PlaySound("InBoutton");
            OpenAreYouSure();
            mydelegate = ResetAllPlayerPrefs;
        }
        else
        {
            AudioManager.Instance.PlaySound("OutBoutton");
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene(0);
        }  
    }

    public void OpenAreYouSure()
    {
        AudioManager.Instance.PlaySound("InBoutton");
        AreYouSure.SetActive(true);
    }
    
    public void NotImNot()
    {
        AreYouSure.SetActive(false);
        AudioManager.Instance.PlaySound("OutBoutton");
        sure = false;
    }

    public void Imsure()
    {
        AudioManager.Instance.PlaySound("OutBoutton");
        mydelegate();
        AreYouSure.SetActive(false);
        sure = true;
    }



}
