﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionMoveObject : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ZoneToStartMoving")
        {
            ObstacleMovement obstacleMovementComponent = other.gameObject.transform.parent.GetComponent<ObstacleMovement>();
            obstacleMovementComponent.Movable = true;
            obstacleMovementComponent.StartCoroutine(obstacleMovementComponent.SpeedDown());
        }
    }
}
