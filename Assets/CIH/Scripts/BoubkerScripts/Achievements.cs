﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Achievements : MonoBehaviour {

	public GameObject achievement;
	public GameObject achievementContainer;

	public PlayerAchievements[] achievements;

	List<GameObject> achievementsGameObject = new List<GameObject>(); 
		
	// Use this for initialization
	void Start () {
	
		int totalCoins =  (int) PlayerPrefs.GetFloat ("MoneyAvailable");
		Debug.Log (totalCoins);
		InstantiateAchievements ();
		CheckForAchievement (totalCoins);
	}

	public void DisplayAchievements () {

		if (!achievementContainer.activeInHierarchy)
			achievementContainer.SetActive (true);
		else {
			achievementContainer.SetActive (false);
		}
	}

	void InstantiateAchievements () {
		

		for (int i = 0; i < achievements.Length; i++) {

			GameObject newAchievement = Instantiate (achievement) as GameObject;
			newAchievement.transform.SetParent (achievementContainer.transform);
			newAchievement.gameObject.name = "Achievement " + i.ToString ();
			SetInstantiatedAchievements (newAchievement, i);
			achievementsGameObject.Add (newAchievement);
			newAchievement.SetActive (true);
		}

	}

	void SetInstantiatedAchievements (GameObject achievement, int index) {

		Text title = achievement.transform.Find ("AchievementTitle").gameObject.GetComponent<Text> ();
		title.text = achievements [index].achievementName;

		if (achievements [index].achievementLoot == PlayerAchievements.loot.coins) {
			Text reward = achievement.transform.Find ("Reward").gameObject.GetComponent<Text> ();
			reward.text = "Reward " + achievements [index].reward.ToString ();
		}

		Text coinsNeeded = achievement.transform.Find ("Coins needed").gameObject.GetComponent<Text> ();
		coinsNeeded.text = "Collect " + achievements [index].coinsToCollect + " coins";
			
	}



	public void CheckForAchievement (int coinAmount) {

		for (int i = 0; i < achievements.Length; i++) {

			if (coinAmount >= achievements [i].coinsToCollect && !achievements [i].achievementDone) {
				UnlockAchievement (i);
				Debug.Log ("Unlocked achievement   " + i);
			}
		}

	}

	void UnlockAchievement (int indexToUnlock) {

		achievements [indexToUnlock].achievementDone = true;

		for (int i = 0; i < achievementsGameObject.Capacity; i++) {
			
			Text title = achievementsGameObject[indexToUnlock].transform.Find ("AchievementTitle").gameObject.GetComponent<Text> ();
			title.color = Color.green;

			Text reward = achievementsGameObject[indexToUnlock].transform.Find ("Reward").gameObject.GetComponent<Text> ();
			reward.color = Color.green;
			

			Text coinsNeeded = achievementsGameObject[indexToUnlock].transform.Find ("Coins needed").gameObject.GetComponent<Text> ();
			coinsNeeded.color = Color.green;
		}


	}

}


[System.Serializable]
public class PlayerAchievements {

	public string achievementName;
	public float coinsToCollect;
	public enum loot {Costume, coins, powerUp};
	public loot achievementLoot = loot.coins;
	public int reward;

	public bool achievementDone;

}
