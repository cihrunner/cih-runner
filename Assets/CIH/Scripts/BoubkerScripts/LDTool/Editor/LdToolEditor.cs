﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LdToolEditor : EditorWindow {

    LdTool ldToolScript;
    int toolBarInt;
    public enum Environements
    {
        MosqueCasa,
        Normal,
        Sphere,
        Twin,
        Colonial,
        BankCasa,
        Fena,
        AganouGate,
        CheckPointMarackesh,
        Kotobia,
        SceneMarakesh_1,
        SceneMarakesh_2,
        Agency
    }

    Object TrackToLoad;

    public enum Obstacles
    {
        Fence_Jump,
        Fence_Slide,
        Fence_Block,
        Bus_C,
        Bus_Moving_C,
        Taxi_C,
        Taxi_Moving_C,
        Tram_Forward_C,
        Tram_Backward_C,
        Tram_Forward_Moving_C,
        Tram_Backward_Moving_C,
        CoinGroup,
        Bad_Robot_C,
        Bus_M,
        Bus_Moving_M,
        Taxi_M,
        Taxi_Moving_M,
        Tram_Forward_M,
        Tram_Backward_M,
        Tram_Forward_Moving_M,
        Tram_Backward_Moving_M,
        Bad_Robot_M,
        Cletter,
        Iletter,
        Hletter,

    }

    Vector2 scrollPos = Vector2.zero;
    string[] toolBarstring = {"Acceuil", "EditMode"};
    Environements optionsEnv;
    Obstacles optionsObstacles;

    private void OnEnable()
    {
        var LdScript = GameObject.Find("LdManager");
        ldToolScript = LdScript.GetComponent<LdTool>();
    }


    [MenuItem("Window/LevelDesigner")]
    public static void ShowWindow()
    {
         GetWindow<LdToolEditor>("LevelDesigner");
    }

    private void OnGUI()
    {
        
        toolBarInt = GUILayout.Toolbar(toolBarInt, toolBarstring);

        GUILayout.Label("LevelDesigner", EditorStyles.boldLabel);

        //Acceuil
        if(toolBarInt == 0)
        {
            if (GUILayout.Button("new track"))
            {
                ldToolScript.AddNewTrack();
                ldToolScript.EditTrack(ldToolScript.Tracks[ldToolScript.Tracks.Count-1]);
                toolBarInt = 1;
            }

            EditorGUILayout.BeginHorizontal();
            TrackToLoad = EditorGUILayout.ObjectField((Object)TrackToLoad, typeof(GameObject), true);
            if (GUILayout.Button("Load Track") && TrackToLoad !=null)
            {
                ldToolScript.LoadExistingTrackToTheSystem(TrackToLoad as GameObject);
            }
            EditorGUILayout.EndHorizontal();
            //the scroll of the existing tracks
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            
            for (int i = 0; i < ldToolScript.Tracks.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Track " + i);
                if (GUILayout.Button("edit this track"))
                {
                    ldToolScript.EditTrack(ldToolScript.Tracks[i]);
                    toolBarInt = 1;
                }
                EditorGUILayout.EndHorizontal();     
            }

            EditorGUILayout.EndScrollView();

        }
        else
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("currentSize "+ ldToolScript.CurrentTrackOnEdition.size, EditorStyles.boldLabel);
            if(GUILayout.Button("Add Size"))
            {
                ldToolScript.AddSize();
            }

            if (GUILayout.Button("subtract Size") && ldToolScript.CurrentTrackOnEdition.size >0)
            {
                ldToolScript.substractSize();
            }
            if (GUILayout.Button("Reset Size"))
            {
                ldToolScript.resetSize();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            optionsEnv = (Environements)EditorGUILayout.EnumPopup(optionsEnv);
            if(GUILayout.Button("Change To " + optionsEnv.ToString()))
            {
                InstantiateEnvironement(optionsEnv);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            optionsObstacles = (Obstacles)EditorGUILayout.EnumPopup(optionsObstacles);
            if (GUILayout.Button("Add " + optionsObstacles.ToString()))
            {
                InstantiateObstacle(optionsObstacles);
            }
            if(GUILayout.Button("Reset ObstacleContainer"))
            {
                ldToolScript.ResetObstacleContainer();
            }
            EditorGUILayout.EndHorizontal();


            //saveButton
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save Track"))
            {
                ldToolScript.SaveTrack();
                toolBarInt = 0;
            }

            if (GUILayout.Button("Save as new Track"))
            {
                ldToolScript.SaveAsNewTrack();
                toolBarInt = 0;
            }
            EditorGUILayout.EndHorizontal();
        }
        
        
    }


    void InstantiateEnvironement(Environements op)
    {
        switch (op)
        {
            case Environements.MosqueCasa:
                ldToolScript.AddEnvironement("MosqueCasa");
                break;
            case Environements.Normal:
                ldToolScript.AddEnvironement("Normal");
                break;
            case Environements.Sphere:
                ldToolScript.AddEnvironement("Sphere");
                break;
            case Environements.Twin:
                ldToolScript.AddEnvironement("Twin");
                break;
            case Environements.Colonial:
                ldToolScript.AddEnvironement("Colonial");
                break;
            case Environements.BankCasa:
                ldToolScript.AddEnvironement("BankCasa");
                break;
            case Environements.Fena:
                ldToolScript.AddEnvironement("Fena");
                break;
            case Environements.AganouGate:
                ldToolScript.AddEnvironement("AganouGate");
                break;
            case Environements.CheckPointMarackesh:
                ldToolScript.AddEnvironement("CheckPointMarackesh");
                break;
            case Environements.Kotobia:
                ldToolScript.AddEnvironement("Kotobia");
                break;
            case Environements.SceneMarakesh_1:
                ldToolScript.AddEnvironement("SceneMarakesh_1");
                break;
            case Environements.SceneMarakesh_2:
                ldToolScript.AddEnvironement("SceneMarakesh_2");
                break;
            case Environements.Agency:
                ldToolScript.AddEnvironement("Agency");
                break;
            default:
                break;
        }
    }

    void InstantiateObstacle(Obstacles op)
    {
        switch (op)
        {
            case Obstacles.Fence_Jump:
                ldToolScript.AddObstacle("Fence_Jump");
                break;
            case Obstacles.Fence_Slide:
                ldToolScript.AddObstacle("Fence_Slide");
                break;
            case Obstacles.Fence_Block:
                ldToolScript.AddObstacle("Fence_Block");
                break;
            case Obstacles.Bus_C:
                ldToolScript.AddObstacle("Bus_C");
                break;
            case Obstacles.Bus_Moving_C:
                ldToolScript.AddObstacle("Bus_Moving_C");
                break;
            case Obstacles.Taxi_C:
                ldToolScript.AddObstacle("Taxi_C");
                break;
            case Obstacles.Taxi_Moving_C:
                ldToolScript.AddObstacle("Taxi_Moving_C");
                break;
            case Obstacles.Tram_Forward_C:
                ldToolScript.AddObstacle("Tram_Forward_C");
                break;
            case Obstacles.Tram_Backward_C:
                ldToolScript.AddObstacle("Tram_Backward_C");
                break;
            case Obstacles.Tram_Forward_Moving_C:
                ldToolScript.AddObstacle("Tram_Forward_Moving_C");
                break;
            case Obstacles.Tram_Backward_Moving_C:
                ldToolScript.AddObstacle("Tram_Backward_Moving_C");
                break;
            case Obstacles.CoinGroup:
                ldToolScript.AddObstacle("CoinGroup");
                break;
            case Obstacles.Bad_Robot_C:
                ldToolScript.AddObstacle("Bad_Robot_C");
                break;
            case Obstacles.Bus_M:
                ldToolScript.AddObstacle("Bus_M");
                break;
            case Obstacles.Bus_Moving_M:
                ldToolScript.AddObstacle("Bus_Moving_M");
                break;
            case Obstacles.Taxi_M:
                ldToolScript.AddObstacle("Taxi_M");
                break;
            case Obstacles.Taxi_Moving_M:
                ldToolScript.AddObstacle("Taxi_Moving_M");
                break;
            case Obstacles.Tram_Forward_M:
                ldToolScript.AddObstacle("Tram_Forward_M");
                break;
            case Obstacles.Tram_Backward_M:
                ldToolScript.AddObstacle("Tram_Backward_M");
                break;
            case Obstacles.Tram_Forward_Moving_M:
                ldToolScript.AddObstacle("Tram_Forward_Moving_M");
                break;
            case Obstacles.Tram_Backward_Moving_M:
                ldToolScript.AddObstacle("Tram_Backward_Moving_M");
                break;
            case Obstacles.Bad_Robot_M:
                ldToolScript.AddObstacle("Bad_Robot_M");
                break;
            case Obstacles.Cletter:
                ldToolScript.AddObstacle("Cletter");
                break;
            case Obstacles.Iletter:
                ldToolScript.AddObstacle("Iletter");
                break;
            case Obstacles.Hletter:
                ldToolScript.AddObstacle("Hletter");
                break;
            default:
                break;
        }
    }

}
#endif