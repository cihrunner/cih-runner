﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LdTool : MonoBehaviour {

    //this variables can be shown on the edit mode
    public List<Obstacle> obstaclesO;
    public List<Environement> EnvironementsO;
    public Transform EmptyTrack;
    public Transform EnterTrigger, ExitTrigger, RoadCollider;
    //in the acceuil mode so that we can choose one
    public List<Track> Tracks;

    public GameObject currentTrackEditedGo;
    public Track CurrentTrackOnEdition;



    public void AddNewTrack()
    {
        ClearTracks();
        Transform TrackClone = Instantiate(EmptyTrack);
        TrackClone.parent = this.transform;
        Track newTrack = CreateMySO("Track " + Tracks.Count);
        newTrack.TrackName = "Track " + Tracks.Count;
        ToZero(TrackClone);
        newTrack.root = TrackClone;

        

        GameObject newEnvironement = new GameObject("EnvironementContainer");
        newEnvironement.transform.parent = newTrack.root;
        ToZero(newEnvironement.transform);
        

        GameObject newObstacleContainer = new GameObject("ObstacleContainer");
        newObstacleContainer.transform.parent = newTrack.root;
        ToZero(newObstacleContainer.transform);
        

        Transform enterTriggerClone = Instantiate(EnterTrigger);
        enterTriggerClone.parent = newTrack.root;
        ToZero(enterTriggerClone);
        

        Transform exitTriggerClone = Instantiate(ExitTrigger);
        exitTriggerClone.parent = newTrack.root;
        ToZero(exitTriggerClone);
        

        Transform RoadColliderClone = Instantiate(RoadCollider);
        RoadColliderClone.parent = newTrack.root;
        ToZero(RoadColliderClone);
        


        GameObject trackPrefab = CreatePrefab(newTrack.root.gameObject, "Track " + Tracks.Count ) as GameObject;
        trackPrefab.tag = "Track";
        newTrack.theTrack = trackPrefab;

        newTrack.root = trackPrefab.transform;

        newTrack.environementContainer = trackPrefab.transform.GetChild(0).gameObject;
        newTrack.ObstaclesContainer = trackPrefab.transform.GetChild(1).gameObject;
        newTrack.enter = trackPrefab.transform.GetChild(2);
        newTrack.Exit = trackPrefab.transform.GetChild(3);
        newTrack.RoadCollider = trackPrefab.transform.GetChild(4);

        Tracks.Add(newTrack);

    }

    public void EditTrack(Track thisTrack)
    {
        ClearTracks();
        GameObject trackClone = Instantiate(thisTrack.theTrack);
        trackClone.transform.parent = this.transform;
        CurrentTrackOnEdition = thisTrack;
        currentTrackEditedGo = this.transform.GetChild(0).gameObject;
    }

    public void SaveTrack()
    {
        GameObject trackPrefab = CreatePrefab(this.transform.GetChild(0).gameObject, CurrentTrackOnEdition.TrackName) as GameObject;

        CurrentTrackOnEdition.theTrack = trackPrefab;
        CurrentTrackOnEdition.root = trackPrefab.transform;
        CurrentTrackOnEdition.environementContainer = trackPrefab.transform.GetChild(0).gameObject;
        CurrentTrackOnEdition.ObstaclesContainer = trackPrefab.transform.GetChild(1).gameObject;
        CurrentTrackOnEdition.enter = trackPrefab.transform.GetChild(2);
        CurrentTrackOnEdition.Exit = trackPrefab.transform.GetChild(3);
        CurrentTrackOnEdition.RoadCollider = trackPrefab.transform.GetChild(4);
       
    }

    public void SaveAsNewTrack()
    {
        Track newTrack = CreateMySO("Track " + Tracks.Count);
        newTrack.TrackName = "Track " + Tracks.Count;
        GameObject trackPrefab = CreatePrefab(this.transform.GetChild(0).gameObject, newTrack.TrackName) as GameObject;

        newTrack.theTrack = trackPrefab;
        newTrack.root = trackPrefab.transform;
        newTrack.environementContainer = trackPrefab.transform.GetChild(0).gameObject;
        newTrack.ObstaclesContainer = trackPrefab.transform.GetChild(1).gameObject;
        newTrack.enter = trackPrefab.transform.GetChild(2);
        newTrack.Exit = trackPrefab.transform.GetChild(3);
        newTrack.RoadCollider = trackPrefab.transform.GetChild(4);
        Tracks.Add(newTrack);
    }

    public void AddSize()
    {
        CurrentTrackOnEdition.size++;
    }

    public void substractSize()
    {
        CurrentTrackOnEdition.size--;
    }

    public void resetSize()
    {
        CurrentTrackOnEdition.size = 0;
    }

    public void AddEnvironement(string environementName)
    {
        foreach (var env in EnvironementsO)
        {
            if(environementName == env.EnvironementName)
            {
                //add the specifique environement
                while (transform.GetChild(0).GetChild(0).childCount > 0)
                {
                    ResetEnvironementContainer();
                }
                ChangeToEnv(CurrentTrackOnEdition.size, env.EnvironementPrefab);
            }
        }
    }

    private void ResetEnvironementContainer()
    {
        //clear environementContainer
        foreach (Transform child in this.transform.GetChild(0).GetChild(0))
        {
            DestroyImmediate(child.gameObject);
        }
    }

    public void ChangeToEnv(int size, GameObject env)
    {
        
        
        List<EnvExitEnter> clones = new List<EnvExitEnter>();

        //Instantiate all env needed and collect there enter and exit
        for (int i = 0; i < size; i++)
        {
            clones.Add(new EnvExitEnter(PrefabUtility.InstantiatePrefab(env) as GameObject));
            //set the clone as child of the EnvironementContainer
            clones[i].Env.transform.parent = currentTrackEditedGo.transform.GetChild(0);
            clones[i].Env.transform.position = Vector3.zero;
           
        }

        for (int i = 0; i < size-1; i++)
        {
            clones[i+1].Env.transform.position = clones[i].Env.transform.position - Vector3.forward * Vector3.Distance(clones[i].enter, clones[i].exit) - Vector3.right * ((clones[i].Env.transform.position.x - clones[i].exit.x) - (clones[i+1].Env.transform.position.x - clones[i+1].enter.x)) - Vector3.up * ((clones[i].Env.transform.position.y - clones[i].exit.y) - (clones[i+1].Env.transform.position.y - clones[i+1].enter.y));
        }
        //EnterTrigger
        currentTrackEditedGo.transform.GetChild(2).position = clones[0].enter;
        currentTrackEditedGo.transform.GetChild(3).position = clones[clones.Count - 1].exit + clones[clones.Count-1].Env.transform.position;
        currentTrackEditedGo.transform.GetChild(4).localScale = new Vector3(currentTrackEditedGo.transform.GetChild(4).localScale.x, currentTrackEditedGo.transform.GetChild(4).localScale.y, Vector3.Distance(currentTrackEditedGo.transform.GetChild(3).position, currentTrackEditedGo.transform.GetChild(2).position)/5);

    }


    public void AddObstacle(string ObstacleName)
    {
        foreach (var ob in obstaclesO)
        {
            if (ObstacleName == ob.ObstacleName)
            {
                
                GameObject newOb = PrefabUtility.InstantiatePrefab(ob.ObstaclePrefab) as GameObject;
                
                newOb.transform.parent = this.transform.GetChild(0).GetChild(1);
                newOb.transform.position = new Vector3(0, 0.7f, 0);
                EditorGUIUtility.PingObject(newOb);
            }
        }
    }

    public void ResetObstacleContainer()
    {
        while (transform.GetChild(0).GetChild(1).childCount > 0)
        {
            foreach (Transform child in this.transform.GetChild(0).GetChild(1))
            {
                DestroyImmediate(child.gameObject);
            }
        }
    }

    void ToZero(Transform thing)
    {
        thing.position = Vector3.zero;
    }

    private void ClearTracks()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

    public void LoadExistingTrackToTheSystem(GameObject TrackToLoad)
    {
        ClearTracks();
        //load the Track On The Scene
        GameObject TrackToLoadInstance = Instantiate(TrackToLoad);
        ToZero(TrackToLoadInstance.transform);
        TrackToLoadInstance.transform.parent = this.transform;
        //new SO track to collect the loaded track information
        Track NewTrack = CreateMySO("Track " + Tracks.Count);
        NewTrack.TrackName = "Track " + Tracks.Count;
        Transform thisEnvCon = SearchOneInChildrenByName(TrackToLoadInstance.transform, "EnvironementContainer");
        if (thisEnvCon != null)
        {
            NewTrack.environementContainer = thisEnvCon.gameObject;
            ToZero(thisEnvCon);
        }
        NewTrack.size = thisEnvCon.childCount;
        Transform thisExT = SearchOneInChildrenByName(TrackToLoadInstance.transform, "ExitTrigger");
        if (thisExT != null)
        {
            NewTrack.Exit = thisExT;
        }
        Transform thisET = SearchOneInChildrenByName(TrackToLoadInstance.transform, "EnterTrigger");
        if (thisET != null)
        {
            NewTrack.enter = thisET;
        }
        Transform thisRC = SearchOneInChildrenByName(TrackToLoadInstance.transform, "RoadCollider");
        if (thisRC != null)
        {
            NewTrack.RoadCollider = thisRC;
        }

        GameObject newObstacleContainer = new GameObject("ObstacleContainer");
        newObstacleContainer.transform.parent = TrackToLoadInstance.transform;
        ToZero(newObstacleContainer.transform);

        for (int i = 0; i < obstaclesO.Count; i++)
        {
            PlaceAllObstaclesInTheContainer(TrackToLoadInstance, obstaclesO[i].ObstaclePrefab.name, newObstacleContainer.transform);
        }

        NewTrack.ObstaclesContainer = newObstacleContainer;

        Transform ClearNewTrack = Instantiate(EmptyTrack);

        ClearNewTrack.parent = this.transform;

        NewTrack.environementContainer.transform.parent = ClearNewTrack;
        NewTrack.ObstaclesContainer.transform.parent = ClearNewTrack;
        NewTrack.enter.transform.parent = ClearNewTrack;
        NewTrack.Exit.transform.parent = ClearNewTrack;
        NewTrack.RoadCollider.transform.parent = ClearNewTrack;
        NewTrack.root = ClearNewTrack;

        GameObject trackPrefab = CreatePrefab(NewTrack.root.gameObject, "Track " + Tracks.Count) as GameObject;
        NewTrack.theTrack = trackPrefab;
        NewTrack.root = trackPrefab.transform;
        NewTrack.environementContainer = trackPrefab.transform.GetChild(0).gameObject;
        NewTrack.ObstaclesContainer = trackPrefab.transform.GetChild(1).gameObject;
        NewTrack.enter = trackPrefab.transform.GetChild(2);
        NewTrack.Exit = trackPrefab.transform.GetChild(3);
        NewTrack.RoadCollider = trackPrefab.transform.GetChild(4);

        DestroyImmediate(this.transform.GetChild(1).gameObject
            );
        Tracks.Add(NewTrack);
    }

    Transform SearchOneInChildrenByName(Transform toSearchIn, string TheName)
    {
        foreach (Transform child in toSearchIn)
        {
            if(child.name.Contains(TheName))
            {
                return child;
            }
        }
        return null;
    }

    

    void PlaceAllObstaclesInTheContainer(GameObject mytrack, string ObstacleName, Transform ObstaclContainer)
    {
        GameObject[] AllObjects = FindObjectsOfType<GameObject>();

        for (int i = 0; i < AllObjects.Length; i++)
        {
            if (AllObjects[i].name.Contains(ObstacleName))
            {
                AllObjects[i].transform.parent = ObstaclContainer;
            }
        }
    }

    #region asset  creation
    public static Track CreateMySO(string thisname)
    {


        Track asset = ScriptableObject.CreateInstance<Track>();

       
        AssetDatabase.CreateAsset(asset, "Assets/CIH/Scripts/BoubkerScripts/LDTool/Data/" + thisname +".asset");
        AssetDatabase.SaveAssets();
        
        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;

        return asset;
    }

    static GameObject CreatePrefab(GameObject gameObject, string ObjectName)
    {
        
        //Set the path as within the Assets folder, and name it as the GameObject's name with the .prefab format
        string localPath = "Assets/CIH/Scripts/BoubkerScripts/LDTool/Prefabs/Tests/" + ObjectName + ".prefab";
        

        //Check if the Prefab and/or name already exists at the path
        if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
        {
            //Create dialog to ask if User is sure they want to overwrite existing Prefab
            if (EditorUtility.DisplayDialog("Are you sure?",
                "The Prefab already exists. Do you want to overwrite it?",
                "Yes",
                "No"))
            //If the user presses the yes button, create the Prefab
            {
                return CreateNew(gameObject, localPath);
            }
        }
        //If the name doesn't exist, create the new Prefab
        else
        {
            Debug.Log(gameObject.name + " is not a Prefab, will convert");
            return CreateNew(gameObject, localPath);
        }

        return null;
    }

   
    static bool ValidateCreatePrefab()
    {
        return Selection.activeGameObject != null;
    }

    static GameObject CreateNew(GameObject obj, string localPath)
    {
        //Create a new Prefab at the path given
        Object prefab = PrefabUtility.CreatePrefab(localPath, obj);
        return PrefabUtility.ReplacePrefab(obj, prefab, ReplacePrefabOptions.ConnectToPrefab);
    }
    #endregion

    #region Classes for LdTool
    [System.Serializable]
    public class Environement
    {
        public string EnvironementName;
        public GameObject EnvironementPrefab;
    }
    [System.Serializable]
    public class Obstacle
    {
        public string ObstacleName;
        public GameObject ObstaclePrefab;
    }

    public struct EnvExitEnter
    {
        public Vector3 enter, exit;
        public GameObject Env;

        public EnvExitEnter(GameObject _env) {

            this.enter = _env.transform.GetChild(0).position;
            this.exit = _env.transform.GetChild(1).position;
            this.Env = _env;
        }

    }

    #endregion
}
#endif
