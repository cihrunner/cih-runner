﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : ScriptableObject{

    public string TrackName;
    public int size;
    public Transform enter;
    public Transform Exit;
    public Transform RoadCollider;
    public GameObject environementContainer, ObstaclesContainer;
    public Transform root;
    public GameObject theTrack;

   
}
