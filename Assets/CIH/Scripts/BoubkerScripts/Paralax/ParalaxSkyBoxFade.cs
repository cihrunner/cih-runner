﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxSkyBoxFade : MonoBehaviour {

    public float speed;
    private Vector3 currentPlacement;
    private Vector3 offset;
    public Transform CameraPlayer;
    public Transform plane1, plane2;


    private void Start()
    {
        currentPlacement = transform.position;
        offset = -CameraPlayer.transform.position + currentPlacement;
        StartCoroutine(paralaxFading(speed));
    }

    private void Update()
    {
        transform.position = CameraPlayer.position + offset;
    }

    IEnumerator paralaxFading(float speed)
    {
        yield return new WaitForEndOfFrame();
        while (CameraPlayer.transform.parent.GetComponent<PlayerControls>().CurrentGameState == PlayerControls.GameState.Playing)
        {
            plane1.position += Vector3.right * 0.001f * speed;
            plane2.position += Vector3.left * 0.001f * speed;
            yield return new WaitForEndOfFrame();
        }
    }
}
