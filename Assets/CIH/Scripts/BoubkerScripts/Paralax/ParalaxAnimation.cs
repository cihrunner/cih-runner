﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxAnimation : MonoBehaviour {

    private Material thisMaterial;
    public Camera cameraOnPlayer;


    private void Awake()
    {
        thisMaterial = GetComponent<MeshRenderer>().material;
    }

    private void Start()
    {
        StartCoroutine(OffsetLoop());
    }

    private void Update()
    {
        transform.position = new Vector3(0,cameraOnPlayer.transform.position.y + 17, cameraOnPlayer.transform.position.z - 32.39f);  
    }
    IEnumerator OffsetLoop()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            thisMaterial.mainTextureOffset -= Vector2.up*.001f;

        }
    }
}
