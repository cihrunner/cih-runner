﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Level : ScriptableObject {

    public int worldID;
    public GameObject FirstTrack;
    public PooledObject[] LevelTracks;
    public float[] paliers;
    public List<SpeedandDistance> playerSpeeds;
    public int StarsToUnlockLevel;
    


    
}
