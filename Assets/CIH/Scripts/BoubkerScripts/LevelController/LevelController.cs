using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class LevelController : MonoBehaviour {

    public static LevelController instance;

    [Header("UI")]
    public GameObject map;
    public TextMeshProUGUI[] doorsTonextWorld;
    [SerializeField] private Image psMarakeshdoor;
    [SerializeField] private GameObject ReminderPopUp;
    [SerializeField] private Sprite Color;
    public Text CoinOfLevelInRecap;
    public Transform AvatarSpaceTajin;
    public Button[] levelbtn; // btn 0 is agency button
    [Header("levels")]
    public int[] totalStarsTounlockWorlds;
    public Level[] levels;
    public Level levelTutorial;
    [SerializeField] private int[] PopReminderLevels = new int[2];
    [SerializeField] private Sprite[] PopReminderLevelsSprite = new Sprite[3];
    [TextArea]
    [SerializeField] private string[] PopReminderLevelstext = new string[3];
    [SerializeField] private Image PopReminderLevelsImg;
    [SerializeField] private Text PopReminderLevelsTxt;
    [Header("PlayerData")]
    public GameObject player;
    Paliers paliers;
    GeneratePath generator;
    PlayerControls controls;
    
    [Range(0,20)]
    public float speedAvatarSpaceTajin = 1;

    [HideInInspector]
    public int IndexSelectedLevel;
    public ChargeScene chargeScene;

    
    

    public void Awake()
    {
        
        paliers = player.gameObject.GetComponent<Paliers>();
        generator = player.gameObject.GetComponent<GeneratePath>();
        controls = player.gameObject.GetComponent<PlayerControls>();

        Screen.SetResolution((int)Screen.width, (int)Screen.height, true);
    }

    private void Start()
    {
        
        if (PlayerPrefs.GetInt("Retry") == 1 && PlayerPrefs.GetInt("LevelTutorial")== 1 && PlayerPrefs.GetInt("CurrentLevelSelected") != 0)
        {
            restartLevel();
        }

        if (!PlayerPrefs.HasKey("LevelTutorial") || PlayerPrefs.GetInt("LevelTutorial") == 0)
        {
            map.SetActive(false);
            PlayerPrefs.SetInt("Total stars", 0);
            AudioManager.Instance.PlayMusic("Music_Casa");
            //send data of the respective level to scripts
            //set palier for the choosen level
            paliers.palier1Coins = levelTutorial.paliers[0];
            paliers.palier2Coins = levelTutorial.paliers[1];
            paliers.palier3Coins = levelTutorial.paliers[2];
            //set generator for the choosen level
            generator.tracksToInstantiate = levelTutorial.LevelTracks;
            if (levelTutorial.FirstTrack != null)
            {
                generator.firstTrack = levelTutorial.FirstTrack;
            }

            //set player speed for the choosen level
            controls.SpeedDist = levelTutorial.playerSpeeds;
            PlayerPrefs.SetInt("LevelTutorial", 0);
            chargeScene.SetWorldManager(0);
        }


        for (int i = 1; i < doorsTonextWorld.Length; i++)
        {
            ShowStarsLeftToUnlockWorld(i);
        }
        //checkforWorldAvailabality();
        //checkForLevelAvailability();
        StartCoroutine(CheckForCurrentLevelProgression());
        if (totalStarsTounlockWorlds[1] <= PlayerPrefs.GetInt("Total stars"))
        {

            psMarakeshdoor.sprite = Color;

        }
        for (int i = 0; i < PopReminderLevels.Length; i++)
        {
            OpenPopUpReminder(PopReminderLevels[i], i);
            
        }

    }

    public void CheckForLevelIntroComplition()
    {
        if (PlayerPrefs.GetInt("Total stars") > 0)
        {
            PlayerPrefs.SetInt("LevelTutorial", 1);
        }
        else
        {
            PlayerPrefs.SetInt("LevelTutorial", 0);
        }
    }

    public void restartLevel()
    {
        PlayerPrefs.SetInt("Retry", 1);
        map.SetActive(false);
        IndexSelectedLevel = PlayerPrefs.GetInt("CurrentLevelSelected");
        SetLevel(IndexSelectedLevel);
        chargeScene.SetWorldManager(0);
    }

    public void SetLevel(int index)
    {
        CoinOfLevelInRecap.text = levels[index].paliers[2].ToString();
        //send data of the respective level to scripts
        //set palier for the choosen level
        PlayerPrefs.SetInt("CurrentLevelSelected", index);
        paliers.palier1Coins = levels[index].paliers[0];
        paliers.palier2Coins = levels[index].paliers[1];
        paliers.palier3Coins = levels[index].paliers[2];
        //set generator for the choosen level
        generator.tracksToInstantiate = levels[index].LevelTracks;
        if(levels[index].FirstTrack != null)
        {
            generator.firstTrack = levels[index].FirstTrack;
        }
        
        //set player speed for the choosen level
        controls.SpeedDist = levels[index].playerSpeeds;

        //setting stars for the respective level
        IndexSelectedLevel = index;

    }


    public void ChangeMusic()
    {
        if (IndexSelectedLevel > 10)
        {
            AudioManager.Instance.PlayMusic("Music_Marakesh");
        }
        else
        {
            AudioManager.Instance.PlayMusic("Music_Casa");
        }
    }

    //looping on all levels and disabling levels that arent reached yet
    void checkForLevelAvailability()
    {
        foreach (Level lvl in levels)
        {
            int totalStars = PlayerPrefs.GetInt("Total stars");
            if(totalStars < lvl.StarsToUnlockLevel)
            {
                levelbtn[System.Array.IndexOf(levels, lvl)].interactable = false;
            }
            else
            {
                levelbtn[System.Array.IndexOf(levels, lvl)].interactable = true;
            }
        }
    }

    void OpenPopUpReminder(int levelIndex,int i)
    {
        if(PlayerPrefs.GetInt("CurrentLevelSelected") == levelIndex && PlayerPrefs.GetInt("Retry") == 0)
        {
            ReminderPopUp.SetActive(true);
            PopReminderLevelsImg.sprite = PopReminderLevelsSprite[i];
            PopReminderLevelsTxt.text = PopReminderLevelstext[i];
            
        }
    }

    public void CloseReminderPopUp()
    {
        ReminderPopUp.SetActive(false);
        AudioManager.Instance.PlaySound("OutBoutton");
    }

    void checkforWorldAvailabality()
    {
        foreach (Level lvl in levels)
        {
            int totalStars = PlayerPrefs.GetInt("Total stars");
            if (totalStars < totalStarsTounlockWorlds[lvl.worldID])
            {
                levelbtn[System.Array.IndexOf(levels, lvl)].interactable = false;
            }
            else
            {
                levelbtn[System.Array.IndexOf(levels, lvl)].interactable = true;
            }
        }
    }

    public void ShowStarsLeftToUnlockWorld(int index)
    {
        if (PlayerPrefs.GetInt("Total stars") < totalStarsTounlockWorlds[index])
        {
            doorsTonextWorld[index].text = " " +(totalStarsTounlockWorlds[index] - PlayerPrefs.GetInt("Total stars")).ToString() + "X ";
        }
    }

    IEnumerator CheckForCurrentLevelProgression()
    {
        yield return new WaitForEndOfFrame();
        for (int i = 1; i < levelbtn.Length; i++)
        {
            if (!levelbtn[i].interactable && PlayerPrefs.GetInt("LevelTutorial") == 1)
            {
                
                AvatarSpaceTajin.position = levelbtn[i - 2].transform.position;
                while (Mathf.Abs(Vector2.Distance(AvatarSpaceTajin.position, levelbtn[i - 1].transform.position)) >= 0.001f)
                {
                    AvatarSpaceTajin.position = Vector3.MoveTowards(AvatarSpaceTajin.position, levelbtn[i - 1].transform.position, speedAvatarSpaceTajin * Time.deltaTime);
                    yield return null;
                }

                Debug.Log(i);
                break;
            }
        }
        
    }

    public void toWorldMap()
    {
        PlayerPrefs.SetInt("Retry", 0);
    }
    //IEnumerator AvatarToNextLevel()
    //{

    //}
    
    
}
