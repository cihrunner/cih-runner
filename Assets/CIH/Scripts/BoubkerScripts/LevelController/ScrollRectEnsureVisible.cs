﻿
#region USING

using UnityEngine;
using UnityEngine.UI;

#endregion

[RequireComponent(typeof(ScrollRect))]
public class ScrollRectEnsureVisible : MonoBehaviour
{
    public LevelController LevelController;
    public float AnimTime = 0.15f;


    private ScrollRect _sr;
    Transform _scrollT;
    private RectTransform _SrRect;

    private void Awake()
    {
        _sr = GetComponent<ScrollRect>();
        LevelController = LevelController.GetComponent<LevelController>();
        _scrollT = _sr.transform;
        _SrRect = _sr.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        if (!PlayerPrefs.HasKey("CurrentLevelSelected"))
            return;
        RectTransform levelRect = LevelController.levelbtn[PlayerPrefs.GetInt("CurrentLevelSelected")].GetComponent<RectTransform>();
        CenterOnItem(levelRect);
    }
    public void CenterOnItem(RectTransform target)
    {
        Debug.Log(target.name);
        float targetYposition = target.anchorMax.y;
        float normalizePosition =targetYposition;
        _sr.verticalNormalizedPosition = normalizePosition;
        Debug.Log(normalizePosition);
    }
}
