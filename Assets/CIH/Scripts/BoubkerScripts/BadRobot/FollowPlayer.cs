﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public float speed;
    PlayerControls player;
    ObjectPlacement placement;
    Vector3 target;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControls>();
        placement = GetComponent<ObjectPlacement>();
    }

    private void Update()
    {
       if(player.CurrentSlot == -1)
        {
            target = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
        }
       else if(player.CurrentSlot == 0)
        {
            target = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }
       else if(player.CurrentSlot == 1)
        {
            target = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }
      
    }
}
