﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLeaderBoard : MonoBehaviour {

    public FaceBookSdkManager FaceBookSdkManager;

    private void Awake()
    {
        FaceBookSdkManager = FaceBookSdkManager.GetComponent<FaceBookSdkManager>();
    }

    private void OnEnable()
    {
        FaceBookSdkManager.GetLeaderBoard();
    }
}
