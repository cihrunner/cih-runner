﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisionEffect : MonoBehaviour {

    public float TimeBetweenDisapear;
    public int HowManyTimePlayerGoesInvisible;
    [HideInInspector]
    public bool effectRunning = false;

    private SkinnedMeshRenderer SkinnedMeshRenderer;

    private void Awake()
    {
        SkinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
    }

    public IEnumerator EffectofInvisibility()
    {
        effectRunning = true;
        for(int i = 0; i<HowManyTimePlayerGoesInvisible; i++)
        {
           
            yield return new WaitForSeconds(TimeBetweenDisapear);
            SkinnedMeshRenderer.enabled = true;
            yield return new WaitForSeconds(TimeBetweenDisapear);
            SkinnedMeshRenderer.enabled = false;
        }
        SkinnedMeshRenderer.enabled = true;
        effectRunning = false;
    }
}
