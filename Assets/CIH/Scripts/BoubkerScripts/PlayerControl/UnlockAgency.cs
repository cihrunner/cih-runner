﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UnlockAgency : MonoBehaviour {

    public Button AgencyButton;
    public Image[] CihLetters;
    public Sprite unlockedC, unlockedI, unlockedH;
    public Sprite lockedC, lockedI, lockedH;
    public GameObject PopUpAgency;
    

    private void Start()
    {
        AgencyButton = AgencyButton.gameObject.GetComponent<Button>();
        UnlockAgencyButton();
    }

    //to call on recap
    public void UnlockAgencyButton()
    {
        
        if (PlayerPrefs.GetInt("CLetter") == 1 && PlayerPrefs.GetInt("ILetter") == 1 && PlayerPrefs.GetInt("HLetter") == 1)
       {
            //Animate the unlock
            PopUpAgency.SetActive(true);
            AgencyButton.interactable = true;
            AudioManager.Instance.PlaySound("Unlock");
            //Debug.Log("set To True");
        }
        else
        {
            AgencyButton.interactable = false;
        }
        if (PlayerPrefs.GetInt("CLetter") == 1)
        {
            CihLetters[0].sprite = unlockedC;
        }
        else
        {
            CihLetters[0].sprite =lockedC;
        }
        if (PlayerPrefs.GetInt("ILetter") == 1)
        {
            CihLetters[1].sprite = unlockedI;
        }
        else
        {
            CihLetters[1].sprite = lockedI;
        }
        if (PlayerPrefs.GetInt("HLetter") == 1)
        {
            CihLetters[2].sprite = unlockedH;

        }
        else
        {
            CihLetters[2].sprite = lockedH;
        }
        
    }

    public void ResetCihLettersPlayerprefs()
    {

        CihLetters[0].enabled = false;
        CihLetters[1].enabled = false;
        CihLetters[2].enabled = false;

        AgencyButton.interactable = false;
   

    }


    public void ClosePopUpAgency()
    {
        PopUpAgency.SetActive(false);
        AudioManager.Instance.PlaySound("OutBoutton");
    }



}
