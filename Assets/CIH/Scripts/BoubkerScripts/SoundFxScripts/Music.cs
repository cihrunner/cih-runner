﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameAnalyticsSDK;
using UnityEngine;

public class Music : MonoBehaviour {

    public static Music music = null;

    public AudioSource audioSourceMusic;
    public Sprite volumeMuted, notmuted;
    public static bool muted = false;

    private void Awake()
    {
        GameAnalytics.Initialize();
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game");
        if (music != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            music = GetComponent<Music>();
            audioSourceMusic = GetComponent<AudioSource>();
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    public void MuteSound()
    {
        if(audioSourceMusic.volume == 0)
        {
            audioSourceMusic.volume = 1;
            AudioManager.Instance.MuteButton.sprite = notmuted;
        }
        else
        {
            audioSourceMusic.volume = 0;
            AudioManager.Instance.MuteButton.sprite = volumeMuted;
        }
        
    }
}
