﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {

        AudioManager.Instance.PlaySound("Explosion");

	}
	
	
}
