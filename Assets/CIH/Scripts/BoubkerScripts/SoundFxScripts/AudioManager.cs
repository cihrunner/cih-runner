﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public SoundFx[] Sounds;
    public GameObject Sound;

    public Image MuteButton;
    //static GameObject music;
    //static AudioSource musicSource;

    public static AudioManager Instance;

    private void Awake()
    {
        Instance = GetComponent<AudioManager>();

    }

    public void muteeverything(Image muteImg, Sprite mutedsprite, Sprite notmuted)
    {
        if (Music.muted)
        {
            Music.muted = false;
            Music.music.MuteSound();
            muteImg.sprite = notmuted;
        }
        else
        {
            muteImg.sprite = mutedsprite;
            Music.music.MuteSound();
            Music.muted = true;
        }
    }

    public void muteeverything()
    {
        if (Music.muted)
        {
            Music.muted = false;
            Music.music.MuteSound();
            
        }
        else
        {
            
            Music.music.MuteSound();
            Music.muted = true;
        }
    }

    public void PlaySound(string clip)
    {
        if (Music.muted)
        {
            return;
        }
        else
        {
            var sound = Instantiate(Sound);
            sound.transform.parent = gameObject.transform;
            var audioSrc = sound.GetComponent<AudioSource>();
            for (int i = 0; i < Sounds.Length; i++)
            {
                if (Sounds[i].AudioName == clip)
                {
                    audioSrc.clip = Sounds[i].clip;
                    audioSrc.Play();
                    audioSrc.pitch = Sounds[i].Pitch;
                    audioSrc.volume = Sounds[i].volume;
                    break;
                }
            }
        }
        
    }

    public void PlayMusic(string clip)
    {
        if (Music.muted)
        {
            return;
        }
        for (int i = 0; i < Sounds.Length; i++)
        {
            if(Sounds[i].AudioName == clip)
            {
                if(Music.music.audioSourceMusic.clip == null)
                {
                    Music.music.audioSourceMusic.clip = Sounds[i].clip;
                    Music.music.audioSourceMusic.volume = Sounds[i].volume;
                    Music.music.audioSourceMusic.Play();

                }
                else
                {
                    StartCoroutine(FadeToNextMusic(Music.music.audioSourceMusic, Sounds[i]));
                }
            }
        }
    }

    public IEnumerator PitchUpMusic(float maxValue)
    {
        yield return new WaitForEndOfFrame();
        while (Music.music.audioSourceMusic.pitch < maxValue)
        {
            Music.music.audioSourceMusic.pitch += .01f;
            yield return new WaitForFixedUpdate();
        }

    }

    public IEnumerator PitchDownMusic(float minValue)
    {
        yield return new WaitForEndOfFrame();
        while (Music.music.audioSourceMusic.pitch > minValue)
        {
            Music.music.audioSourceMusic.pitch -= 0.01f;
            yield return new WaitForEndOfFrame();
        }

       
    }



    IEnumerator FadeToNextMusic(AudioSource music, SoundFx clipTochange)
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < 90; i++)
        {

            float radAngle = i * (Mathf.PI / 180);
            music.volume = Mathf.Cos(radAngle);
            yield return new WaitForEndOfFrame();
        }
        music.clip = clipTochange.clip;
        music.Play();
        for (int i = 90; i >= 0; i--)
        {
            float radAngle = i * (Mathf.PI / 180);
            music.volume = Mathf.Cos(radAngle);
            if (music.volume >= clipTochange.volume)
                break;
            yield return new WaitForEndOfFrame();
        }
    }
}
