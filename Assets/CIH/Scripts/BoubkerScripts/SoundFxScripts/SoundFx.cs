﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SoundFx : ScriptableObject {

    public string AudioName;
    public AudioClip clip;
    [Range(0f,1f)]
    public float volume;
    public float Pitch;
}
