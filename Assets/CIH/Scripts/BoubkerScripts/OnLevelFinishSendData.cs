﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using GameAnalyticsSDK;
using TMPro;

public class OnLevelFinishSendData : MonoBehaviour {

    public static OnLevelFinishSendData instance;

    float CoinCount;
    int numberOfStars;
    float palier1Coins;
    float palier2Coins;
    float palier3Coins;
    bool oneTime;
    PlayerScoreUGUI playeruGui;
    PlayerControls playerControl;
    Paliers paliers;
    bool deathAnimationFinished;

    public GameObject recap;
    [Header("UI")]
    public Text MoneyCollectedInLevel;
    public TextMeshProUGUI TotalStarsCollected;
    public GameObject deathMenu;
    [SerializeField] private GameObject[] PowerUpButton;
    [Header("Player")]
    public GameObject player;
    public Camera PlayerCamera;
    [HideInInspector] public bool death;
    [Header("Cihletters")]
    public Image[] CihImages;
    public Sprite[] c, i, h;

    private int toAdd = 0;
   
  


    private void Awake()
    {

        if (instance != null)
        {
            Destroy(instance);
        }
        else
        {
            instance = GetComponent<OnLevelFinishSendData>();
        }

        TotalStarsCollected.text = PlayerPrefs.GetInt("Total stars").ToString();
        //get player score ugui component
        playeruGui = player.GetComponent<PlayerScoreUGUI>();
        //get playercontrols component
        playerControl = player.GetComponent<PlayerControls>();
        //get palier component
        paliers = player.GetComponent<Paliers>();
        //set palier coins variables for this level
        palier1Coins = paliers.palier1Coins;
        palier2Coins = paliers.palier2Coins;
        palier3Coins = paliers.palier3Coins;
        oneTime = false;
        death = false;
    }

   
    //data send to dataholder instance
    public void SendDataToDataHolder()
    {

        foreach (GameObject item in PowerUpButton)
        {
            item.SetActive(false);
        }
        DataHolder.instance.LevelIndex = ChargeScene.instance.levelToCharge;
        DataHolder.instance.palier1Coins = palier1Coins;
        DataHolder.instance.palier2Coins = palier2Coins;
        DataHolder.instance.palier3Coins = palier3Coins;
        DataHolder.instance.distance = PlayerScoreUGUI.playerDistance();
        //set CIH
        DataHolder.instance.CLetter = PlayerPrefs.GetInt("CLetter") == 1;
        DataHolder.instance.ILetter = PlayerPrefs.GetInt("ILetter") == 1;
        DataHolder.instance.HLetter = PlayerPrefs.GetInt("HLetter") == 1;
        //Debug.Log(DataHolder.instance.CLetter);
        //Debug.Log(DataHolder.instance.ILetter);
        //Debug.Log(DataHolder.instance.HLetter);
        int levelIndex = PlayerPrefs.GetInt("CurrentLevelSelected");

        if(levelIndex == 0)
        {
            PlayerPrefs.SetInt("CLetter", 0);
            PlayerPrefs.SetInt("ILetter", 0);
            PlayerPrefs.SetInt("HLetter", 0);
            DataHolder.instance.CLetter = PlayerPrefs.GetInt("CLetter") == 1;
            DataHolder.instance.ILetter = PlayerPrefs.GetInt("ILetter") == 1;
            DataHolder.instance.HLetter = PlayerPrefs.GetInt("HLetter") == 1;
        }

        string starUnlockedPrefs = "Level" + (levelIndex).ToString();

        if (paliers.palier3Reached)
        {
            CoinCount = paliers.palier3Coins;
            DataHolder.NmberOfStars = 3;

            DataHolder.instance.coinCount = CoinCount;
            CheckMaxCoinCollected(CoinCount);
            Debug.Log(PlayerPrefs.GetFloat("MoneyAvailable") + CoinCount);

            toAdd = -PlayerPrefs.GetInt(starUnlockedPrefs) + 3;
           

            //SetCoinsOnLevelUI();
            PlayerPrefs.SetFloat("MoneyAvailable", PlayerPrefs.GetFloat("MoneyAvailable") + CoinCount);
        }
        else if (paliers.palier2Reached && !paliers.palier3Reached)
        {
            CoinCount = paliers.palier2Coins;
            DataHolder.instance.coinCount = CoinCount;
            DataHolder.NmberOfStars = 2;

            CheckMaxCoinCollected(CoinCount);
            Debug.Log(PlayerPrefs.GetFloat("MoneyAvailable") + CoinCount);
            if (PlayerPrefs.GetInt(starUnlockedPrefs) <= 2)
            {
                toAdd = -PlayerPrefs.GetInt(starUnlockedPrefs) + 2;
                
            }
            SetCoinsOnLevelUI();

            PlayerPrefs.SetFloat("MoneyAvailable", PlayerPrefs.GetFloat("MoneyAvailable") + CoinCount);

        }
        else if (paliers.palier1Reached && !paliers.palier2Reached && !paliers.palier3Reached)
        {
            CoinCount = paliers.palier1Coins;
            DataHolder.instance.coinCount = CoinCount;
            DataHolder.NmberOfStars = 1;

            CheckMaxCoinCollected(CoinCount);
            SetCoinsOnLevelUI();
            Debug.Log(this.gameObject);
            if(PlayerPrefs.GetInt(starUnlockedPrefs) <= 1)
            {
                toAdd = -PlayerPrefs.GetInt(starUnlockedPrefs) + 1;
                
            }



            PlayerPrefs.SetFloat("MoneyAvailable", PlayerPrefs.GetFloat("MoneyAvailable") + CoinCount);
        }
        else if (!paliers.palier1Reached && !paliers.palier2Reached && !paliers.palier3Reached)
        {
            CoinCount = 0;
            DataHolder.NmberOfStars = 0;
            SetCoinsOnLevelUI();
            DataHolder.instance.coinCount = CoinCount;
        }

        CihImages[0].sprite = c[PlayerPrefs.GetInt("CLetter")];
        CihImages[1].sprite = i[PlayerPrefs.GetInt("ILetter")];
        CihImages[2].sprite = h[PlayerPrefs.GetInt("HLetter")];

       // recap.GetComponent<StarController>().enabled = true;
        TotalStarsCollected.text = PlayerPrefs.GetInt("Total stars").ToString();

        //while (StarController.instance == null) ;
        StarController.instance.setRecapUI();
        //Debug.Log(PlayerPrefs.GetFloat("MoneyAvailable"));
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coins", PlayerPrefs.GetFloat("MoneyAvailable"), "Coin", "Money");

    }

    //update coins with the max coin collected
    void MaxCoinCollected()
    {
        //get current coin collection and compare it with the last one if bigger change
        float max = playeruGui.CurrentPlayerCoin;
        if (CoinCount < max)
        {
            CoinCount = max;
        }
    }

    void CheckMaxCoinCollected(float coins)
    {
        if (!PlayerPrefs.HasKey("Level" + ChargeScene.instance.levelToCharge + "MaxCoins"))
        {
            PlayerPrefs.SetFloat("Level" + ChargeScene.instance.levelToCharge + "MaxCoins", coins);
        }
        else
        {
            float maxCoins = PlayerPrefs.GetFloat("Level" + ChargeScene.instance.levelToCharge + "MaxCoins");
            if (coins > maxCoins)
            {
                PlayerPrefs.SetFloat("Level" + ChargeScene.instance.levelToCharge + "MaxCoins", coins);
            }
        }
    }
    void EnablingUICamera()
    {
        PlayerCamera.enabled = false;

    }

    public void LateUpdate()
    {
        MaxCoinCollected();
        if (!paliers.palier3Reached || !playerControl.dead)
        {
            oneTime = false;
        }
        if (playerControl.CurrentGameState == PlayerControls.GameState.Dead)
        {
            
            if (playerControl.dead && !oneTime)
            {
                
                death = true;
                playerControl.dead = false;


                //dont show recap till the duration of the animaiton finish
                StartCoroutine("WaitBeforeShowRecap");

            }

            if (deathAnimationFinished)
            {
                recap.SetActive(true);
                //deathMenu.SetActive(false);
                EnablingUICamera();
                deathAnimationFinished = false;
            }
        }

       
    }

	void Start () {

		Screen.SetResolution (800, 1280, true);

	}

    IEnumerator WaitBeforeShowRecap()
    {
        deathAnimationFinished = false;
        yield return new WaitForSeconds(3f);
        deathAnimationFinished = true;
    }

    public void SetCoinsOnLevelUI()
    {
        MoneyCollectedInLevel.text = PlayerPrefs.GetFloat("Level" + ChargeScene.instance.levelToCharge + "MaxCoins").ToString();
    }


    public void SetStarsPrefs()
    {
        if (PlayerPrefs.GetInt("LevelTutorial") ==0)
        {
            if(DataHolder.NmberOfStars > 0)
            {
                PlayerPrefs.SetInt("Total stars", DataHolder.NmberOfStars);
                PlayerPrefs.SetInt("LevelTutorial", 1);
                return;
            }
        }
        else if(PlayerPrefs.GetInt("CurrentLevelSelected") == 0)
        {
            int ts = PlayerPrefs.GetInt("Total stars");
            PlayerPrefs.SetInt("Total stars", ts+3);
        }
        else
        {
            int levelIndex = PlayerPrefs.GetInt("CurrentLevelSelected");
            string starUnlockedPrefs = "Level" + (levelIndex).ToString();

            int TotalStars = PlayerPrefs.GetInt("Total stars");
            if (toAdd > 0)
            {
                if (PlayerPrefs.GetInt(starUnlockedPrefs) < 3)
                {
                    PlayerPrefs.SetInt(starUnlockedPrefs, PlayerPrefs.GetInt(starUnlockedPrefs) + toAdd);
                }
            }

            if (toAdd >= 0)
            {
                TotalStars += toAdd;
            }
            PlayerPrefs.SetInt("Total stars", TotalStars);
        }
       
        

        
    }
}
