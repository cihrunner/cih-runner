﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObstacleOnTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            IObstacleMove obstacleMove = this.transform.parent.GetComponent<IObstacleMove>();
            obstacleMove.MoveObstacle();
        }

    }
}
