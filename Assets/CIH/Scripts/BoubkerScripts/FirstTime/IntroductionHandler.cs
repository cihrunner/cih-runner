﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class IntroductionHandler : MonoBehaviour {

    [SerializeField] private List<GameObject> IntroPages = new List<GameObject>();
    [SerializeField] private Sprite PlaySprite;
    [SerializeField] private Image PageImage;
    [SerializeField] private Image nextBtnImage;
    [SerializeField] private Button PreviousBtn;
    [SerializeField] private GameObject fade;

    private int currentPage = 0;
    private void Start()
    {
        AudioManager.Instance.PlayMusic("Music_Casa");
        StartCoroutine(Intro());
    }


    public IEnumerator Intro()
    {
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < 6; i++)
        {

            fade.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            turnTheNextPage();
            yield return new WaitForSeconds(0.5f);

            fade.SetActive(false);
            yield return new WaitForSeconds(2f);
        }
    }

    public void turnTheNextPage()
    {
        if (currentPage == IntroPages.Count - 1) // last page
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            currentPage++;
        }
        for (int i = 0; i < IntroPages.Count; i++)
        {
            if(i == currentPage)
            {
                IntroPages[i].SetActive(true);
            }
            else
            {
                IntroPages[i].SetActive(false);
            }
        }
    }


}
