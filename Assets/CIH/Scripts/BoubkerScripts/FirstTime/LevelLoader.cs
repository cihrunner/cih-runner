﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LevelLoader : MonoBehaviour {

    public Slider loadingBar;


	public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        yield return new WaitForEndOfFrame();
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadingBar.value = progress;
            Debug.Log(progress);
            yield return null;
        }
    }


    private void Start()
    {
        PlayerPrefs.SetInt("Retry", 0);
        if (!PlayerPrefs.HasKey("FirstTime"))
        {
            StartCoroutine(LoadAsynchronously(2));
            PlayerPrefs.SetInt("FirstTime", 1);
        }
        else
        {
            StartCoroutine(LoadAsynchronously(1));
        }
       
    }
}
