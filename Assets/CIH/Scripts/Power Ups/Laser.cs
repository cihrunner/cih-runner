﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Laser : MonoBehaviour {

	//public List<GameObject> objects = new List<GameObject>();

	RaycastHit[] hits;
	public LayerMask layerMask;

	public float laserTimer=0;
    public float lazerTimeActive;

	public GameObject laserObject;
    public Animator SideRobotAnimController;

	public float totalRechargeTime;
    [HideInInspector]
    public float rechargeTime;

	public Button triggerButton;
	public Image loadingImage;
    public GameObject particalOfDestruction;

    private void Awake()
    {
        lazerTimeActive = ShopWorldMapController.instance.GetPowerUpByName("Laser").TimersPerLevel[PlayerPrefs.GetInt("LazerLevel")];
        SideRobotAnimController = SideRobotAnimController.GetComponent<Animator>();
    }

    // Use this for initialization
    void Start () {

        //Debug.Log ("Laser   " + PlayerPrefs.GetInt ("LaserBought"));

        //Debug.Log ("Total coins   " + PlayerPrefs.GetInt ("Total coins"));

        //if (PlayerPrefs.GetInt ("LaserBought") != 1) {
        //	triggerButton.gameObject.SetActive (false);
        //	loadingImage.gameObject.SetActive (false);
        //} else {
        //	triggerButton.gameObject.SetActive (true);
        //	loadingImage.gameObject.SetActive (true);
        //}
        totalRechargeTime = ShopWorldMapController.instance.GetPowerUpByName("Laser").cooldownPerLevel[PlayerPrefs.GetInt("LazerLevel")];



	}

	// Update is called once per frame
	void Update () {

		//TriggerLaser ();

		CheckRechargeTime ();

		//LaserTest	();

		if (Input.GetKeyDown(KeyCode.V))
			TriggerLaser();
	}


	public void CheckRechargeTime () {

        GetShopPowerUpInfo();
      
        if (totalRechargeTime <= rechargeTime) {

            loadingImage.gameObject.SetActive(true);
			totalRechargeTime += Time.deltaTime;
			triggerButton.interactable = false;
			loadingImage.fillAmount = 1-(totalRechargeTime) / (rechargeTime);
		} else {
            			triggerButton.interactable = true;
            loadingImage.gameObject.SetActive(false);
			ChangeButtonAnim (true);
		}



    }


	public void TriggerLaser () {

		
		StartCoroutine ("StartLaser");
		StartCoroutine("SetLaserTimerNull");
        //StartCoroutine(GetComponent<PlayerControls>().SideAniamtion("Lazer", lazerTimeActive));
		
	}

	IEnumerator StartLaser () {

        SideRobotAnimController.SetBool("LazerOn", true);
        laserTimer = 0;
        yield return new WaitForSeconds (.3f);
        Debug.Log(laserTimer);
        Debug.Log(lazerTimeActive);

		RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer

        laserObject.SetActive(true);

        while (laserTimer < lazerTimeActive) {

			yield return new WaitForEndOfFrame ();
			laserTimer += Time.deltaTime;

			laserObject.SetActive (true);
			if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out hit, Mathf.Infinity, layerMask)) {
				Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * hit.distance, Color.yellow);
				Debug.Log ("Did Hit  " + hit.transform.gameObject.name);

				//Destroy (hit.transform.gameObject);
				DestroyEnnemyFromLaser(hit.transform.gameObject);
                laserTimer = lazerTimeActive + 1;


            } else {
				Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * 1000, Color.white);
				Debug.Log ("Did not Hit");
			}

		}

        yield return new WaitForSeconds(.3f);
        SideRobotAnimController.SetBool("LazerOn", false);

        laserTimer = 0;
		laserObject.SetActive (false);
	}

  

	public void LaserTest () {

		RaycastHit hit;

		if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out hit, Mathf.Infinity, layerMask)) {
			Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * hit.distance, Color.yellow);
			Debug.Log ("Did Hit  " + hit.transform.gameObject.name);

			//Destroy (hit.transform.gameObject);
			DestroyEnnemyFromLaser(hit.transform.gameObject);

		} else {
			Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * 1000, Color.white);
			Debug.Log ("Did not Hit");
		}
	}

	public void DestroyEnnemyFromLaser (GameObject ennemy) {

		//ennemy.GetComponent<EnnemyBehaviour> ().ScoreAdd ();
		//ennemy.GetComponent<EnnemyBehaviour> ().Explode();
		Destroy (ennemy);
        var pClone = Instantiate(particalOfDestruction, ennemy.transform.position, Quaternion.identity);
        Destroy(pClone, 1f);
	}

	public void ChangeButtonAnim (bool animated) {


	}

   

	IEnumerator SetLaserTimerNull () {

		while (totalRechargeTime > 0) {
			yield return new WaitForEndOfFrame ();
			totalRechargeTime -= Time.deltaTime * 5;
		}
	}

    public void GetShopPowerUpInfo()
    {
        if(ShopWorldMapController.instance == null)
        {
            loadingImage.gameObject.SetActive(false);
            return;
        }
        var powers = ShopWorldMapController.instance.powerUps;
        foreach (var item in powers)
        {
            if(item.itemName == "Laser")
            {
                rechargeTime = item.cooldownPerLevel[PlayerPrefs.GetInt(item.playerPrefsItemLevel)];
                lazerTimeActive = item.TimersPerLevel[PlayerPrefs.GetInt(item.playerPrefsItemLevel)];
                
            }
        }
    }


}
