﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTime : MonoBehaviour {

	PlayerControls controller;
	Animator anim;

	public bool isBulletTime;
    public float BulletTimeTimeDuration;

	// Use this for initialization
	void Start () {

		controller = GetComponent<PlayerControls> ();
		anim = controller.animator;
	}
	



    IEnumerator StartBulletTimegg()
    {
        StartBulletTime();
        yield return new WaitForSeconds(BulletTimeTimeDuration);
        EndBulletTime();
    }

	public void TriggerBulletTime () {

        StartCoroutine("StartBulletTimegg");
        StartCoroutine(AudioManager.Instance.PitchDownMusic(0.5f));
        
    }

	public void StartBulletTime () {

		float newSpeed = controller.speed / 2f;
		controller.speed = newSpeed;

		float newAnimSpeed = .5f;
		anim.speed = newAnimSpeed; 
	}

    void PitchDesactivateDown()
    {
        StartCoroutine(AudioManager.Instance.PitchUpMusic(1));
    }


    public void EndBulletTime () {

		float newAnimSpeed = 1f;
		anim.speed = newAnimSpeed;
        controller.speed = controller.speed * 2;
        PitchDesactivateDown();

    }
}
