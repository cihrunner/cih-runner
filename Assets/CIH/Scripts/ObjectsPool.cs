﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  This script is used as a pooling system. It allows us minimise garbage collection by instantiating prefabs at the beginning of the game, and then reusing them
 * 
 * 
 * */

[System.Serializable]

public class PooledObject
{
    public GameObject Object; // The object that will be pulled
    public int amount = 1; // The amount of this object that will be instantiated on start
    public int starApparition;

    //[Range(0,1)]
    //public float probability;
}

public class ObjectsPool : MonoBehaviour
{
    public static ObjectsPool Instance; // Reference to this script. Set to static so that we can invoke it without an object of a class
    public PooledObject[] Objects; // Reference to the PooledObject class 
    private List<GameObject>[] pool; // List of the objects instantiated and handled by the script


    void Start()
    {

        Instance = this; // This allows us to directly call the pooling system via ObjectsPool.Instance.anyfunction 
                         //InstantiateObjects ();
    }

    public void InstantiateObjects()
    {

        // This function is used to instantiate objects in the beginning of the game 

        GameObject objectInstantiation;
        pool = new List<GameObject>[Objects.Length];

        for (int count = 0; count < Objects.Length; count++)
        {

            // It stores every objects filled in the editor in a list
            pool[count] = new List<GameObject>();
            for (int num = 0; num < Objects[count].amount; num++)
            {

                // Then it instantiates a certain amount of every object, depending on the value desired
                objectInstantiation = (GameObject)Instantiate(Objects[count].Object, Vector3.zero, Quaternion.identity);

                // Then it sets all the objects to false 
                objectInstantiation.SetActive(false);

                // Then, the object to which the script is attached is set as the parent of the objects
                objectInstantiation.transform.parent = transform;

                // Then these objects are added to the list "pool", so that we can handle them
                pool[count].Add(objectInstantiation);
            }
        }
    }


    public GameObject Activate(int id, Vector3 position, Quaternion rotation)
    {
        // This function activates an object that is stored on the pool

        for (int count = 0; count < pool[id].Count; count++)
        {
            if (!pool[id][count].activeSelf)
            {
                // First it checks if the object is active, if not, then the gameObject is refered as currObj, and it's transform as currTrans 
                GameObject currObj = pool[id][count];
                Transform currTrans = currObj.transform;

                currObj.SetActive(true); // We activate the gameObject 
                currTrans.position = position;
                currTrans.rotation = rotation;
                return currObj;
            }
        }
        GameObject newObj = Instantiate(Objects[id].Object) as GameObject;
        Transform newTrans = newObj.transform;
        newTrans.position = position;
        newTrans.rotation = rotation;
        newTrans.parent = transform;
        pool[id].Add(newObj);
        return newObj;
    }

    public void Deactivate(GameObject obj)
    {
        // This function disables an object that is stored on the pool

        obj.SetActive(false);
    }
}