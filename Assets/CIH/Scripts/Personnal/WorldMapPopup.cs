﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMapPopup : MonoBehaviour {

	public GameObject popup;
    public GameObject[] Description;
    public GameObject LockPopUp;

	public GameObject leftStar;
	public GameObject middleStar;
	public GameObject rightStar;

	public Sprite completedLeftStar;
	public Sprite completedMiddleStar;
	public Sprite completedRightStar;

	public Sprite emptyLeftStar;
	public Sprite emptyMiddleStar;
	public Sprite emptyRightStar;

	public Text levelText;

	ChargeScene sceneCharge;

	// Use this for initialization
	void Start () {

		sceneCharge = GetComponent<ChargeScene> ();
        AudioManager.Instance.PlayMusic("Music_WorldMap");
        
    }
    // Update is called once per frame
    void Update () {
		
	}
    public void CloseDescription(int index)
    {
        Description[index].SetActive(false);
       
    }

    public void CloseLockPopUp()
    {
        LockPopUp.SetActive(false);
    }

    public void ClosePopUp()
    {
        popup.SetActive(false);
    }

	public void ShowPopup (int levelIndex) {

		sceneCharge.levelToCharge = levelIndex;


		popup.SetActive (false);
		popup.SetActive (true);
		//popup.transform.position = new Vector3 (transform.position.x, transform.position.y, popup.transform.position.z);

		string starUnlockedPrefs = "Level" + (levelIndex).ToString();
		int totalLevelStars = PlayerPrefs.GetInt (starUnlockedPrefs);

		//Debug.Log (PlayerPrefs.GetInt(starUnlockedPrefs));


		EmptyAllStars ();

		if (totalLevelStars >= 1) {
			leftStar.GetComponent<Image> ().sprite = completedLeftStar;
		}

		if (totalLevelStars >= 2) {
			middleStar.GetComponent<Image> ().sprite = completedMiddleStar;
		}

		if (totalLevelStars >= 3) {
			rightStar.GetComponent<Image> ().sprite = completedRightStar;
		}

	}

	public void ChangeLevelName (string levelName) {

		levelText.text = levelName;

	}

	public void EmptyAllStars () { 
		
		leftStar.GetComponent<Image> ().sprite = emptyLeftStar;
		middleStar.GetComponent<Image> ().sprite = emptyMiddleStar;
		rightStar.GetComponent<Image> ().sprite = emptyRightStar;

	}


}
