﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinExplosion : MonoBehaviour
{

    public float explosionPower = 1f;

    Rigidbody rb;

    // Use this for initialization
    void Start()
    {

        rb = GetComponent<Rigidbody>();

        StartCoroutine("Explode");

    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator Explode()
    {

        float power = explosionPower * 40;
        //power = Mathf.Clamp (power, 1, 250);

        yield return new WaitForEndOfFrame();
        rb = GetComponent<Rigidbody>();
        Vector3 direction = new Vector3(Random.Range(-150, 300) * power, Random.Range(150, 500) * power, -Random.Range(100, 500) * power);
        Vector3 pos = gameObject.transform.position;
        //rb.AddExplosionForce (600f, pos, 3f, 4f);
        rb.AddForce(direction);
        //Debug.Log("Explode");

        yield return new WaitForSeconds(2);
        Destroy(gameObject);

    }

}