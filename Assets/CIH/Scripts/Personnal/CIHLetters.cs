﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CIHLetters : MonoBehaviour {

	public int letterIndex ; // 1 for C, 2 for I, 3 for H

	// Use this for initialization
	void Start () {
		
		CheckRecuperatedLetter (); 
	}
	
	// Update is called once per frame
	void Update () {

		//transform.Rotate (0, 2.5f, 0);

		CheckRecuperatedLetter (); 
	}

    public void CheckRecuperatedLetter() {

        if (letterIndex == 1) {
            if (PlayerPrefs.GetInt("CLetter") == 1) {
                Destroy(this.gameObject);
            }
            else
            {
                PlayerPrefs.SetInt("CLetter", 0);
            }
        }

        else if (letterIndex == 2) {
            if (PlayerPrefs.GetInt("ILetter") == 1) {
                Destroy(gameObject);
            }
            else
            {
                PlayerPrefs.SetInt("ILetter", 0);
            }
        }

        else if (letterIndex == 3) {
            if (PlayerPrefs.GetInt("HLetter") == 1) {
                Destroy(gameObject);
            }
            else
            {
                PlayerPrefs.SetInt("HLetter", 0);
            }
        }
        else if (letterIndex == 0)
        {
            return;
        }
        
	}

    void GetLetter()
    {
        if (letterIndex == 1)
        {
            PlayerPrefs.SetInt("CLetter", 1);
        }
        else if (letterIndex == 2)
        {
            PlayerPrefs.SetInt("ILetter", 1);
        }
        else if (letterIndex == 3)
        {
            PlayerPrefs.SetInt("HLetter", 1);
        }
        else
        {
            return;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetLetter();
            AudioManager.Instance.PlaySound("Unlock");
        }
    }
}
