﻿/// <summary>
/// Controller.
/// this script use for control a character
/// </summary>

using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
 
public class NewController : MonoBehaviour {

	public enum DirectionInput{
		Null, Left, Right, Up, Down
	}

	public enum Position{
		Middle, Left, Right	
	}
	 
	public GameObject magnet;
	public float speedMove = 5; 
	public float gravity;
	public float jumpValue;

	[HideInInspector]
	public bool isRoll;
	[HideInInspector]
	public bool isDoubleJump;
	[HideInInspector]
	public bool isMultiply;
	[HideInInspector]
	public CharacterController characterController;

	[HideInInspector]
	public float timeSprint;
	[HideInInspector]
	public float timeMagnet;
	[HideInInspector]
	public float timeMultiply;
	[HideInInspector]
	public float timeJump;

	private bool activeInput;
	private bool jumpSecond;

	private Vector3 moveDir;
	private Vector2 currentPos;

	public bool keyInput;
	public bool touchInput;

	private Position positionStand;
	private DirectionInput directInput; 


	public static NewController instace;

 

	void Start(){
		//Set state character
		instace = this;
		characterController = this.GetComponent<CharacterController>();
 
		jumpSecond = false;
		magnet.SetActive(false);
		Invoke("WaitStart",0.2f);
	}

	//Reset state,variable when character die
	public void Reset(){
		transform.position = new Vector3(0, transform.position.y, -5);
 
		positionStand = Position.Middle;
		jumpSecond = false;
		isRoll = false;
		isDoubleJump = false;
		isMultiply = false;
		magnet.SetActive(false);
		StopAllCoroutines();
		StartCoroutine(UpdateAction());
	}

	void WaitStart(){
		StartCoroutine(UpdateAction());
	}	

	//Update Loop
	IEnumerator UpdateAction(){
		while(true){
			

				if(keyInput)
					KeyInput();

				if(touchInput){
					//TouchInput();
					DirectionAngleInput();
				}
				CheckLane();
				MoveForward();

			yield return 0;	
		}
			
	}



	private void MoveForward(){
		speedMove = 5f;

		if(characterController.isGrounded){
			moveDir = Vector3.zero;
			if(directInput == DirectionInput.Up){
				Jump();
				if(isDoubleJump){
					jumpSecond = true;	
				}
			}
		}else{
			if(directInput == DirectionInput.Down){
				QuickGround();
			}
			if(directInput == DirectionInput.Up){
				if(jumpSecond){
					JumpSeccond();
					jumpSecond = false;
				}
			}

		}
		moveDir.z = 0;
		moveDir += this.transform.TransformDirection(Vector3.forward*speedMove);
		moveDir.y -= gravity * Time.deltaTime;

		characterController.Move(moveDir*Time.deltaTime);
	}

	private void QuickGround(){
		moveDir.y -= jumpValue*3;
	}


	//Jump State
	private void Jump(){
		//Play sfx when jump

		moveDir.y += jumpValue;
	}

	private void JumpSeccond(){
		
		moveDir.y += jumpValue*1.15f;
	}

	private void CheckLane(){
		if(positionStand == Position.Middle){
			if(directInput == DirectionInput.Right){
				if(characterController.isGrounded){
					GetComponent<Animation>().Stop();

				}
				positionStand = Position.Right;	
				//Play sfx when step

			}else if(directInput == DirectionInput.Left){
				if(characterController.isGrounded){
					GetComponent<Animation>().Stop();

				}
				positionStand = Position.Left;	
				//Play sfx when step

			}
			transform.position = Vector3.Lerp(transform.position, new Vector3(0,transform.position.y,transform.position.z), 6 * Time.deltaTime);
		}else if(positionStand == Position.Left){
			if(directInput == DirectionInput.Right){
				if(characterController.isGrounded){
					GetComponent<Animation>().Stop();

				}
				positionStand = Position.Middle;	
				//Play sfx when step

			}
			transform.position = Vector3.Lerp(transform.position, new Vector3(-1.8f,transform.position.y,transform.position.z), 6 * Time.deltaTime);
		}else if(positionStand == Position.Right){
			if(directInput == DirectionInput.Left){
				if(characterController.isGrounded){
					GetComponent<Animation>().Stop();

				}
				positionStand = Position.Middle;
				//Play sfx when step

			}
			transform.position = Vector3.Lerp(transform.position, new Vector3(1.8f,transform.position.y,transform.position.z), 6 * Time.deltaTime);
		}

		if(directInput == DirectionInput.Down){
			GetComponent<Animation>().Stop();

			//Play sfx when roll

		}
	}

	//Key input method
	private void KeyInput()
	{
		if(Input.anyKeyDown)
		{
			activeInput = true;
		}

		if(activeInput)
		{
			if(Input.GetKey(KeyCode.A))
			{
				directInput = DirectionInput.Left;
				activeInput = false;
			}else

				if(Input.GetKey(KeyCode.D))
				{
					directInput = DirectionInput.Right;
					activeInput = false;
				}else

					if(Input.GetKey(KeyCode.W))
					{
						directInput = DirectionInput.Up;
						activeInput = false;
					}else

						if(Input.GetKey(KeyCode.S))
						{
							directInput = DirectionInput.Down;
							activeInput = false;
						}
		}else{
			directInput = DirectionInput.Null;	
		}


	}

	//Touch input method
	private void TouchInput(){
		if(Input.GetMouseButtonDown(0)){
			currentPos = Input.mousePosition;	
			activeInput = true;
		}
		if(Input.GetMouseButton(0)){
			if(activeInput){
				if((Input.mousePosition.x - currentPos.x) > 40){
					directInput = DirectionInput.Right;
					activeInput = false;
				}else if((Input.mousePosition.x - currentPos.x) < -40){
					directInput = DirectionInput.Left;
					activeInput = false;
				}else if((Input.mousePosition.y - currentPos.y) > 40){
					directInput = DirectionInput.Up;
					activeInput = false;
				}else if((Input.mousePosition.y - currentPos.y) < -40){
					directInput = DirectionInput.Down;
					activeInput = false;
				}
			}else{
				directInput = DirectionInput.Null;
			}

		}
		if(Input.GetMouseButtonUp(0)){
			directInput = DirectionInput.Null;	
		}
		currentPos = Input.mousePosition;
	}

	private void DirectionAngleInput(){
		if(Input.GetMouseButtonDown(0)){
			currentPos = Input.mousePosition;
			activeInput = true;
		}


		if(Input.GetMouseButtonUp(0)){
			directInput = DirectionInput.Null;	
			activeInput = false;
		}

	}

	//Sprint Item
	public void Sprint(float speed, float time){
		StopCoroutine("CancelSprint"); 
		timeSprint = time;
		StartCoroutine(CancelSprint());
	}

	IEnumerator CancelSprint(){
		while(timeSprint > 0){
			timeSprint -= 1 * Time.deltaTime;
			yield return 0;
		}
		int i = 0; 
	}

	//Magnet Item
	public void Magnet(float time){
		StopCoroutine("CancleMagnet");
		magnet.SetActive(true);
		timeMagnet = time;
		StartCoroutine(CancleMagnet());
	}

	IEnumerator CancleMagnet(){
		while(timeMagnet > 0){
			timeMagnet -= 1 * Time.deltaTime;
			yield return 0;
		}
		magnet.SetActive(false);
	}

	//Double jump Item
	public void JumpDouble(float time){
		StopCoroutine("CancleJumpDouble");
		isDoubleJump = true;
		timeJump = time;
		StartCoroutine(CancleJumpDouble());
	}

	IEnumerator CancleJumpDouble(){
		while(timeJump > 0){
			timeJump -= 1 * Time.deltaTime;
			yield return 0;
		}
		isDoubleJump = false;
	}

	//Multiply Item
	public void Multiply(float time){
		StopCoroutine("CancleMultiply");
		isMultiply = true;
		timeMultiply = time;
		StartCoroutine(CancleMultiply());
	}

	IEnumerator CancleMultiply(){
		while(timeMultiply > 0){
			timeMultiply -= 1 * Time.deltaTime;
			yield return 0;
		}
		isMultiply = false;
	}
}
