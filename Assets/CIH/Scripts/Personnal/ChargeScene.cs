﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeScene : MonoBehaviour {

    public static ChargeScene instance;
	public int levelToCharge;

	public GameObject map;
	public GameObject world;

	public GameObject levelRecap;

	public GameObject[] worldManagers;

	public GameObject player;
	Paliers paliers;

	int palier1;
	int palier2;
	int palier3;

    public void Awake()
    {
        instance = GetComponent<ChargeScene>();
    }

    // Use this for initialization
    void Start () {

		paliers = player.GetComponent<Paliers> ();
	}
	
	// Update is called once per frame
	void Update () {


	}

	public void ChangeLevelIndex (int levelIndex) {

		levelToCharge = levelIndex - 1;

	}

	public void ChangePalier1 (int newPalier1) {

		palier1 = newPalier1;
	}

	public void ChangePalier2 (int newPalier2) {

		palier2 = newPalier2;
	}

	public void ChangePalier3 (int newPalier3) {

		palier3 = newPalier3;
	}

	public void ChangePlayerPaliers () {

		paliers.palier1Coins = palier1;
		paliers.palier2Coins = palier2;
		paliers.palier3Coins = palier3;
	}


	public void StartLevel () {

		Application.LoadLevel (levelToCharge);
	}

	public void EnablePlayWorld () {

		map.SetActive (false);
		world.SetActive (true);

		levelRecap.SetActive (false);

	}

	public void SetWorldManager (int worldIndex) {

		worldIndex = levelToCharge;

		//GameObject newWorldManager = Instantiate (worldManagers[worldIndex]) as GameObject;
		//newWorldManager.transform.SetParent(world.transform);
		//newWorldManager.gameObject.name = "WorldManager";

		EnablePlayWorld ();
	}
}
