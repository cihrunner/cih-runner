﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGoToBank : MonoBehaviour {

	GameObject bank;
	Rigidbody rb;

	public int guichetIndex;

	GameObject guichet;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody> ();
		bank = GameObject.Find ("Banque");

		FindGuichet ();

		StartCoroutine ("DestroyCoin");
	}

	public void FindGuichet () {

		if (guichetIndex == 1)
			guichet = GameObject.Find ("ATM1");

		if (guichetIndex == 2)
			guichet = GameObject.Find ("ATM3");

		if (guichetIndex == 3)
			guichet = GameObject.Find ("ATM5");

		if (guichetIndex == 4)
			guichet = GameObject.Find ("ATM6");
		
	}
	
	// Update is called once per frame
	void Update () {

		GoToBank ();
	}

	public void GoToBank () {

		//transform.Translate (bank.transform.position);

		rb.transform.position = Vector3.MoveTowards (transform.position, guichet.transform.position, 1.2f); 




	}

	public IEnumerator DestroyCoin () {

		yield return new WaitForSeconds (2f);
		Destroy (gameObject);

	}
}
