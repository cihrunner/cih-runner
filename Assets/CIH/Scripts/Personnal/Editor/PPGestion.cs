﻿using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR

public class PlayerPrefsDelete : EditorWindow
	{
	string myString = "Hello World";
	bool groupEnabled;
	bool myBool = true;
	float myFloat = 1.23f;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Youssef/Delete All Player Prefs")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		PlayerPrefsDelete window = (PlayerPrefsDelete)EditorWindow.GetWindow(typeof(PlayerPrefsDelete));
		Debug.Log ("All player prefs deleted");
		PlayerPrefs.DeleteAll ();
		//window.Show();
	}

	void OnGUI()
	{
		/*
		GUILayout.Label("Base Settings", EditorStyles.boldLabel);
		myString = EditorGUILayout.TextField("Text Field", myString);

		groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
		myBool = EditorGUILayout.Toggle("Toggle", myBool);
		myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
		EditorGUILayout.EndToggleGroup();
*/
		GUILayout.Label("PlayerPrefs have been deleted", EditorStyles.boldLabel);
		Debug.Log ("On GUI !!");
	}
}


#endif

#if UNITY_EDITOR

public class CIHPrefsDelete : EditorWindow
{
	string myString = "Hello World";
	bool groupEnabled;
	bool myBool = true;
	float myFloat = 1.23f;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Youssef/Delete Letters Player Prefs")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		CIHPrefsDelete window = (CIHPrefsDelete)EditorWindow.GetWindow(typeof(CIHPrefsDelete));
		window.Show();
	}

	void OnGUI()
	{
		/*
		GUILayout.Label("Base Settings", EditorStyles.boldLabel);
		myString = EditorGUILayout.TextField("Text Field", myString);

		groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
		myBool = EditorGUILayout.Toggle("Toggle", myBool);
		myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
		EditorGUILayout.EndToggleGroup();
*/
		GUILayout.Label("Letters Player Pref Deleted!!", EditorStyles.boldLabel);
		Debug.Log ("Letters Player Pref Deleted!!");
	}

}

#endif