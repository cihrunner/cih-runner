﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TracksInstantiation : MonoBehaviour {

	public GameObject[] tracks;

	public int instantiationIndex;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.L))
			InstantiateTracks ();
	}

	public void InstantiateTracks () {

		instantiationIndex += 1;

		GameObject newTrack = Instantiate (tracks [0], new Vector3 (4.997068f, 3497.671f, -61.57011f + instantiationIndex * -91.77f), Quaternion.identity);

	}
}
