﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

	public GameObject player;

	public Vector3 offset;

	PlayerControls controls;

	// Use this for initialization
	void Start () {

		controls = player.GetComponent<PlayerControls> ();
		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
