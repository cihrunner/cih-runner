﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ChangeEnvironementVisual : MonoBehaviour {

	#if UNITY_EDITOR

    GameObject environementContainer;

    public GameObject mosqueePart1;
	public GameObject mosqueePart2;


	public GameObject normalPart1;
	public GameObject normalPart2;

	public GameObject spherePart1;
	public GameObject spherePart2;

	public GameObject tweenPart1;
	public GameObject tweenPart2;

	public GameObject colonialPart1;
	public GameObject colonialPart2;

    public GameObject AgencyPart1;
    public GameObject AgencyPart2;

	public GameObject BankPart1;
	public GameObject BankPart2;

	public GameObject BabPart1, BabPart2;
	public GameObject fenaPart1, FenaPart2;
	public GameObject AganouPart1, AganouPart2;
	public GameObject CheckPointMPart1, CheckPointMPart2;
	public GameObject KotobiaPart1, KotobiaPart2;
	public GameObject SceneMPart1, SceneMPart2;


 

	public GameObject fenceJump;
	public GameObject fenceSlide;
	public GameObject robot;
	public GameObject coinsGroup;
	public GameObject tram1;
	public GameObject tram2;
	public GameObject wall;
	public GameObject fenceJumpCoins;
	public GameObject bus;
	public GameObject taxi;

    

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeToEnv(GameObject part1, GameObject part2)
    {
        environementContainer = transform.Find("EnvironementContainer").gameObject; // Find the game Obect that contains the two environements parts
	
		DestroyImmediate (environementContainer.gameObject); // Destroy it

		GameObject newEnvironement = new GameObject ("EnvironementContainer"); // Create a new container

		newEnvironement.transform.SetParent (gameObject.transform); // Set the track as it's parent 
		newEnvironement.transform.position = Vector3.zero; // Set it's pos to 0,0,0


        GameObject newMosqueePart1 = PrefabUtility.InstantiatePrefab(part1) as GameObject;
        Vector3 enterPoint = newMosqueePart1.transform.GetChild(0).position;
        Vector3 exitPoint = newMosqueePart1.transform.GetChild(1).position;

        newMosqueePart1.transform.SetParent(gameObject.transform.Find("EnvironementContainer"));


        GameObject newMosqueePart2 = PrefabUtility.InstantiatePrefab(part2) as GameObject;
        Vector3 enterPointSecondMesh = newMosqueePart2.transform.GetChild(0).position;
        newMosqueePart2.transform.position = newMosqueePart1.transform.position + Vector3.forward * Vector3.Distance(enterPoint, exitPoint) - Vector3.right * ((newMosqueePart1.transform.position.x-exitPoint.x) - (newMosqueePart2.transform.position.x- enterPointSecondMesh.x)) - Vector3.up * ((newMosqueePart1.transform.position.y - exitPoint.y) - (newMosqueePart2.transform.position.y - enterPointSecondMesh.y));
        newMosqueePart2.transform.SetParent(gameObject.transform.Find("EnvironementContainer"));

        

    }
    
    public void ResetEnvirenementContainer()
    {
        GameObject.Find("EnvironementContainer").transform.position = GameObject.Find("reference").transform.position;
    }

    public void ChangeToMosquee () {

        ChangeToEnv(mosqueePart1, mosqueePart2);
    }


	public void ChangeToNormal () {


        ChangeToEnv(normalPart1, normalPart1);
    }

	public void ChangeToSphere () {

        ChangeToEnv(spherePart1, spherePart2);

	}
    public void ChangeToTween () {

        ChangeToEnv(tweenPart1, tweenPart2);
	}


	public void ChangeToColonial () {

        ChangeToEnv(colonialPart1, colonialPart2);

	}

    public void ChangeToAgency()
    {

        environementContainer = transform.Find("EnvironementContainer").gameObject; // Find the game Obect that contains the two environements parts

        DestroyImmediate(environementContainer.gameObject); // Destroy it

        GameObject newEnvironement = new GameObject("EnvironementContainer"); // Create a new container

        newEnvironement.transform.SetParent(gameObject.transform); // Set the track as it's parent 
                                                                   //newEnvironement.transform.position = Vector3.zero; // Set it's pos to 0,0,0

        // Setting the offset


        Vector3 environement1Pos = new Vector3(0.34f , 2.9f,-62.5f);
        //Vector3 environement2Pos = new Vector3(1.03f, 6.17f,-110.14f);



        GameObject newMosqueePart1 = PrefabUtility.InstantiatePrefab(AgencyPart1) as GameObject;
        newMosqueePart1.transform.position = environement1Pos;
        newMosqueePart1.transform.SetParent(gameObject.transform.Find("EnvironementContainer"));


        //GameObject newMosqueePart2 = PrefabUtility.InstantiatePrefab(AgencyPart2) as GameObject;
        //newMosqueePart2.transform.position = environement2Pos;
        //newMosqueePart2.transform.SetParent(gameObject.transform.Find("EnvironementContainer"));

        newEnvironement.transform.position += new Vector3(5.4796f, 0, 0); // Set it's pos to 0,0,0

    }

    public void ChangeToBank () {

        ChangeToEnv(BankPart1, BankPart2);
	}

	public void ChangeToFena () {

        ChangeToEnv(fenaPart1, FenaPart2);

	}

    public void ChangeToKotobia()
    {

        ChangeToEnv(KotobiaPart1, KotobiaPart2);

    }

    public void ChangeToAganouGate()
    {

        ChangeToEnv(AganouPart1, AganouPart2);

    }

    public void ChangeToSceneMarakech()
    {

        ChangeToEnv(SceneMPart1, SceneMPart2);

    }

    public void ChangeToCheckPointM()
    {

        ChangeToEnv(CheckPointMPart1, CheckPointMPart2);

    }

 

	public void AddObstacle (int obstacleIndex) {

		switch (obstacleIndex) {

		case 1:
			GameObject newJumpFence = PrefabUtility.InstantiatePrefab (fenceJump) as GameObject;
			newJumpFence.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newJumpFence);
			break;

		case 2:
			GameObject newSlideFence = PrefabUtility.InstantiatePrefab (fenceSlide) as GameObject;
			newSlideFence.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newSlideFence);
			break;

		case 3:
			GameObject newRobot = PrefabUtility.InstantiatePrefab (robot) as GameObject;
			newRobot.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newRobot);
			break;

		case 4:
			GameObject newTram1 = PrefabUtility.InstantiatePrefab (tram1) as GameObject;
			newTram1.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newTram1);
			break;

		case 5:
			GameObject newTram2 = PrefabUtility.InstantiatePrefab (tram2) as GameObject;
			newTram2.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newTram2);
			break;

		case 6:
			GameObject newCoinGroup = PrefabUtility.InstantiatePrefab (coinsGroup) as GameObject;
			newCoinGroup.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newCoinGroup);
			break;
		
		case 7:
			GameObject newWall = PrefabUtility.InstantiatePrefab (wall) as GameObject;
			newWall.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newWall);
			break;

		case 8:
			GameObject newJumpCoins = PrefabUtility.InstantiatePrefab (fenceJumpCoins) as GameObject;
			newJumpCoins.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newJumpCoins);
			break;

		case 9:
			GameObject newBus = PrefabUtility.InstantiatePrefab (bus) as GameObject;
			newBus.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newBus);
			break;

		case 10:
			GameObject newTaxi = PrefabUtility.InstantiatePrefab (taxi) as GameObject;
			newTaxi.GetComponent<ObjectPlacement> ().PlaceObjectOnTrack ();
			EditorGUIUtility.PingObject (newTaxi);
			break;
		}


	}

	#endif
		
}
