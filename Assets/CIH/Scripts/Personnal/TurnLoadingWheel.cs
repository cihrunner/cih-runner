﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TurnLoadingWheel : MonoBehaviour {

	AsyncOperation async;

	// Use this for initialization
	void Start () {

		StartCoroutine ("LoadLevel");
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.Rotate (0, 0, -4f);

		if (Input.GetKeyDown (KeyCode.Return))
			async.allowSceneActivation = true;

		
	}
		
	public IEnumerator LoadLevel () {

		yield return new WaitForEndOfFrame ();

		async = SceneManager.LoadSceneAsync(3);
	}


}
