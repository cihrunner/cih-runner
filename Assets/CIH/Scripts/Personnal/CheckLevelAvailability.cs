﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CheckLevelAvailability : MonoBehaviour {

	Button button;

    public LevelController levelController;
    public GameObject[] desc;


	public int minimumStarForUnlock;
	public Sprite lockedButtonSprite;
	Image spriteImg;

	public bool isMonumentRelated;
	public GameObject [] monuments;
	public int monumentIndex;

	public int levelIndex;
   

	public Image[] starsImages;
    public Image[] worldMapStarsImage;
	public Sprite unlockStarSprite;
    [SerializeField] private GameObject lockedPopup;

	// Use this for initialization
	void Start () {

		CheckStarsNotation ();
		CheckButton ();

		if (isMonumentRelated) {
			CheckMonument ();
		}
	}


    private void Awake()
    {
        levelController = levelController.GetComponent<LevelController>();
    }


    void OnEnable () {

		SceneManager.sceneLoaded += OnSceneLoaded;
        

	}

	void OnSceneLoaded (Scene scene, LoadSceneMode mode) {

		CheckButton ();
	}

	public void CheckButton () {

        if(this == null)
        {
            return;
        }
        else
        {
            button = GetComponent<Button>();
        }
		

		int totalScore = PlayerPrefs.GetInt ("Total stars");

		if (totalScore < minimumStarForUnlock) {

			button.interactable = false;
			spriteImg = GetComponent<Image> ();
			spriteImg.sprite = lockedButtonSprite;
		} else {
			button.interactable = true;
		}


	}


	public void CheckStarsNotation () {

		string starUnlockedPrefs = "Level" + (levelIndex).ToString();

		int levelStars = PlayerPrefs.GetInt (starUnlockedPrefs);
        Debug.Log("Level" + levelIndex + " " + levelStars);

        if(PlayerPrefs.GetInt("level" + levelIndex + "starsUnlocked") < levelStars)
        {
            StartCoroutine(UnlockStarsForLevel(levelStars));
        }
        else
        {
            if (levelStars >= 1)
            {
                starsImages[0].sprite = unlockStarSprite;
                worldMapStarsImage[0].sprite = unlockStarSprite;
            }

            if (levelStars >= 2)
            {
                starsImages[1].sprite = unlockStarSprite;
                worldMapStarsImage[1].sprite = unlockStarSprite;
            }

            if (levelStars >= 3)
            {
                starsImages[2].sprite = unlockStarSprite;
                worldMapStarsImage[2].sprite = unlockStarSprite;
            }
        }
		

	}

	public void CheckMonument () {

		string starUnlockedPrefs = "Level" + (levelIndex).ToString();

		int levelStars = PlayerPrefs.GetInt (starUnlockedPrefs);

		if (levelStars >= 1) {
			UnlockMonument (monumentIndex);
		}

		//Debug.Log (starUnlockedPrefs + "   " + levelStars);
	}

	void UnlockMonument (int index) {

        //PlayerPrefs prefs with getint 0 or 1 => 0 == false && 1 == true 
        if (PlayerPrefs.HasKey("monument" + index.ToString()))
        {
            monuments[index].gameObject.SetActive(true);
            //the pop already shown
            if (PlayerPrefs.GetInt("monument"+index.ToString()) == 1)
            {
                return;
            }
            else
            {
                for (int i = 0; i < desc.Length; i++)
                {
                    desc[i].SetActive(false);
                }
                monuments[index].gameObject.SetActive(true);
                desc[index].SetActive(true);
                PlayerPrefs.SetInt("monument" + index.ToString(), 1);
            }

        }
        else
        {
            for (int i = 0; i < desc.Length; i++)
            {
                desc[i].SetActive(false);
            }
            monuments[index].gameObject.SetActive(true);
            desc[index].SetActive(true);
            PlayerPrefs.SetInt("monument" + index.ToString(), 1);
        }

	}

    public void lockPopup(int index)
    {
        if (isMonumentRelated  && PlayerPrefs.GetInt("monument" + index.ToString()) == 0)
        {
            
            lockedPopup.transform.GetComponentInChildren<Text>().text = "Pour débloquer ce monument, vous devez finir le niveau " + index; 
            lockedPopup.SetActive(true);
        }
    }

    public IEnumerator UnlockStarsForLevel(int starsCount)
    {
        for (int i = 0; i<starsCount ; i++)
        {
            starsImages[i].sprite = unlockStarSprite;
            worldMapStarsImage[i].sprite = unlockStarSprite;
            yield return new WaitForSeconds(.5f);
        }

        PlayerPrefs.SetInt("level" + levelIndex + "starsUnlocked", starsCount);
    }

    public void OpenMonument(int index)
    {
        desc[index].SetActive(true);
    }
 
}
