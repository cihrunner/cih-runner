﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TramMoving : MonoBehaviour {

	public bool shallMove;

	public float moveSpeed = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (shallMove)
			MoveTram ();
	}

	public void MoveTram () {

		transform.position += new Vector3 (0, 0, moveSpeed * Time.deltaTime);
	}
}
