﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBegin : MonoBehaviour {

	public GameObject fakeAlien;
	public GameObject world;
	public GameObject mainUI;

	GameObject guiManager;
	GUIManagerUGUI manager;

	Animator anim;

	PlayerControls controls;
	GameObject player;

	// Use this for initialization
	void Start () {

		guiManager = GameObject.Find ("GUIManager");
		manager = guiManager.GetComponent<GUIManagerUGUI> ();
		Greetings ();

		player = GameObject.Find ("Player Without GUI");
		controls = player.GetComponent<PlayerControls> ();

		BeginGame ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Greetings () {

		anim = fakeAlien.GetComponent<Animator> ();
		anim.SetBool ("Greetings", true);

	}

	public void BeginGame () {

		fakeAlien.SetActive (false);
		mainUI.SetActive (false);
//		world.SetActive (true); 
		manager.hitMainMenuPlay ();
		controls.StartCoroutine ("EnableJumpAtStart");
	}
}
