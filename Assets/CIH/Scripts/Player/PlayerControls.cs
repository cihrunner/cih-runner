﻿/* 
Infinite Runner Ultimate Presented by Black Gear Studio ©
         Programmed by Subhojeet Pramanik

This script manages the Player Movements, sound, and Player Animation


*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(CapsuleCollider))]
[System.Serializable]
public class PlayerControls : MonoBehaviour
{
    public static PlayerControls instance;
    public GameObject CoinGlow;
    public Button PauseBtn;
    public Button PowerUpButton;
    public Button LaserButton;
    public Button BTButton;
    //Please refer to documentation for public variables
    [HideInInspector]
    public float speed = 10; //The Current Player Speed
    public float stopMovementOnCollisionTime;
    public Transform Target; //The rotation target
    public bool useTarget = true;//Set this to true always to avoid position problems.
    public bool EnableJump = true;
    public bool EnableSlide = true;
    public float RotSpeed = 10;
    public float TurnSpeed = 10;
    public float JumpSpeed = 10;
    public float JumpHeight = 2;
    public AudioClip JumpSound;
    public float SlideTime = 1f;
    public float SlideDivideFactor = 2.5f;
    public AudioClip SlideSound;
    public int CurrentSlot = 0;
    public GameObject explosionPartical;
    public float ResetLocalPositionSpeed = 3f;
    public TrackTypeEnum TrackType;  //The Enum Variable of type of track.
    public float rightvalue; //The maximum movable distance to the right
    public float leftvalue;  //The maximum movable distance to the left
    public LayerMask GroundLayer; //The layer used by all the grounds
    public GameObject DeathParticle;
    public float minSwipeDistX;    //Touch swipe only
    public float minSwipeDistY;
    public bool StaticCamera;      //Does the camera move while jumping
    public GameObject CameraObject;
    public float CameraHeight;     //Height of camera is it is static
    public float StumbleDuration = 3f;
    public float ShakeDuration = 2f;
    public float ShakeMagnitude = 1f;
    public LayerMask EnemyAndWallLayer; //The layer used by all collidable enemies or obstacles.
    public float Deathdist = 0.5f;          //The distance below which to detect beig collided by an obstacle/enemy
    public bool DisableAnimations = false;
    public Animator animator;       //Assign the anamator of your character here
    public bool AutoTurn = false;    //Auto turn on turn triggers. Useful in powerups
    [HideInInspector]
    public GameObject Enemy;
    [HideInInspector]
    public Vector3 enemyPosition;
    [HideInInspector]
    public Vector3 enemybackPosition;      //The enemy position when its not visible
    private bool jump = false; //Whether the player is moving up
    private WorldManager worldmanager;
    [HideInInspector]
    public bool UseEnemy = false;
    public float BackUpOnRevivalDistance = 0.8f;
    private float CacheJumpSpeed;
    public bool CanJump = false;        //Can we jump now
    private Vector3 animatorLocalPos;
    private bool isRotating = false;    //whether Player is taking a turn
    [HideInInspector]
    public bool dead = false;
    [HideInInspector]
    public float LimitedSpeed;
    [HideInInspector]
    public bool isSpeedLimited = false;
    private bool isSliding = false;
    private Vector3 cameraLocalPosition;
    private float Slidetimer = 0f;
    private CapsuleCollider collider;
    private CapsuleCollider capsuleCollider;
    public CapsuleCollider rollCollider;
    private Quaternion rotateQuaternion;
    public enum TrackTypeEnum

    {
        ThreeSlotTrack, FreeHorizontalMovement
    };

    private GameObject tempref; //The temporary refrence gameobject used to calculate the center of the Track
    //public GameObject shadow;
    private Vector2 startPos;

    [HideInInspector]
    public bool canChangeSlot = true;


    private bool isStumbling = true; //Whether Player is stumbling/stripping now
    private float StumbleTimer = 0f;
    private bool isinit = false;
    public enum GameState
    {
        Stopped, Playing, Pause, Dead  //The four game states 
    };

    public GameState CurrentGameState; //The current game state. Setting this to PlayerControls.GameState.Playing makes the player start running.
    public bool canDetectSwipe = true;

    private bool ishaking = false;
    [HideInInspector]
    public List<SpeedandDistance> SpeedDist = new List<SpeedandDistance>();
    [HideInInspector]
    public float Distance = 0f;  //Not the actual Distance. Actual distance is managed by PlayerScore.cs
    private int CurrentCount = 0;
    [HideInInspector]
    public float TargetSpeed; //The next target speed with which player will transition to
    float HorizontalMoveValue;
    public bool FastRun = false;
    private float nextChangeDistance;
    float PartialDist;

    public float coinsCollected;
    public float coinsSaved;
    public float coinsNotInBank;
    PlayerScoreUGUI playerScore;
    public GameObject coinExplosionPrefab;

    public bool movementStopped;
    public bool bankMovement;
    string lastCollidedTag;
    public GameObject lastCollidedGameobject;

    public GameObject coinsToBank;

    public Text coinsInBankIUI;
    public Text coinsTakenUI;
    public GameObject shadow;

    public GameObject alienModel;

    public bool canRealSlide;

    public bool laneMovementBlocked;
    PlayerPoweUpsUGUI powerupscontroller;
    Rigidbody rigidBody;

    //public float normalGravity = 45f;
    public float jumpImpulse = 84f;

    bool sendData = false;

    int swipInt;



    public AudioClip[] coinsCollectedSFX;
    public AudioClip[] swipeSFX;
    public AudioClip[] jumpSFX;
    public AudioClip[] slideSFX;

    public Text countUpText;



    void init()
    {
        if (!DisableAnimations)
            animator.SetBool("GameStart", true);
    }


    public void StartCharacterAnimations()
    {

        animator.SetBool("GameStart", true);
        //Debug.Log ("Seted to true");
    }

    void Awake()
    {
        instance = GetComponent<PlayerControls>();
        isSpeedLimited = false;
        canChangeSlot = true;
        Distance = 0f;
        nextChangeDistance = SpeedDist[0].Distance; //Setting the first speed as target
        TargetSpeed = SpeedDist[0].Speed;

        powerupscontroller = GetComponent<PlayerPoweUpsUGUI>();
        //Application.targetFrameRate = 60;
    }
    //Added Script ****************************************************************************
    public void StopGame()
    {
        CurrentGameState = GameState.Stopped;
        isinit = false;
    }

    public void StartGame()
    {
        CurrentGameState = GameState.Playing;
        isinit = true;

    }
    //*****************************************************************************************
    private void OnEnable()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level" + PlayerPrefs.GetInt("CurrentLevelSelected"));
        sendData = false;
    }

    private void OnDisable()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level", PlayerPrefs.GetInt("MoneyAvailable"));
    }
    void Start()
    {

        rigidBody = GetComponent<Rigidbody>();

        //        worldmanager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WorldManager>();
        cameraLocalPosition = CameraObject.transform.localPosition;
        //Target = worldmanager.StartTarget;
        //Physics.gravity = new Vector3(0, -350f, 0); // Ca c'est si le joueur est en mode (canJump == true)
        Physics.gravity = new Vector3(0, -70f, 0);

        ishaking = false;
        isinit = false;
        CurrentGameState = GameState.Stopped; //Start Game with stopped state
        CurrentSlot = 0; //Middle is the current horizontal position
        tempref = new GameObject();
        Instantiate(tempref, transform.position, transform.rotation);
        tempref.transform.parent = this.transform;
        if (!DisableAnimations)
            animatorLocalPos = animator.transform.localPosition;
        CacheJumpSpeed = JumpSpeed;
        capsuleCollider = GetComponent<CapsuleCollider>();

        playerScore = GetComponent<PlayerScoreUGUI>();

        StartCoroutine("SetCountUpText");

        //coinsInBankIUI = GameObject.Find ("CoinsInBankText").GetComponent<Text>();
        //coinsTakenUI = GameObject.Find ("CoinsTakenText").GetComponent<Text>();

        //StartCoroutine ("EnableJumpAtStart");

    }


    IEnumerator SetCountUpText()
    {
        PauseBtn.interactable = false;
        PowerUpButton.interactable = false;
        LaserButton.interactable = false;
        BTButton.interactable = false;

        yield return new WaitForSeconds(0.65f);
        AudioManager.Instance.PlaySound("PowerUpAdd");
        countUpText.text = "3";

        yield return new WaitForSeconds(0.65f);
        AudioManager.Instance.PlaySound("PowerUpAdd");
        countUpText.text = "2";

        yield return new WaitForSeconds(0.65f);
        AudioManager.Instance.PlaySound("PowerUpAdd");
        countUpText.text = "1";

        yield return new WaitForSeconds(0.65f);
        AudioManager.Instance.PlaySound("PowerUpAdd");
        countUpText.text = "GO !";

        yield return new WaitForSeconds(0.3f);
        countUpText.gameObject.SetActive(false);

        PauseBtn.interactable = true;
        PowerUpButton.interactable = true;
        LaserButton.interactable = true;
        BTButton.interactable = true;
    }


    void Update()
    {


        //if (CanJump && !isSliding)
        //	Physics.gravity = new Vector3 (0, -350f, 0);
        //else {
        //	Physics.gravity = new Vector3 (0, - normalGravity, 0);
        //}

        //if (isSliding)
        //	transform.position = new Vector3 (transform.position.x, Mathf.Clamp (transform.position.y, 1.3913f, 1.3913f), transform.position.z);



        if (CurrentGameState == GameState.Pause)
        { //What to do in Pause
            canDetectSwipe = false;
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f; //Time scale to normal
        }
        if (CurrentGameState == GameState.Playing)
        { //What to do in Playing state
            if (FastRun == false)
            { //Assigning the next speed based on distance
                Distance += speed * Time.deltaTime;
                if (Distance >= nextChangeDistance && nextChangeDistance != 0f)
                {
                    ++CurrentCount;
                    nextChangeDistance = SpeedDist[CurrentCount].Distance;
                    TargetSpeed = SpeedDist[CurrentCount].Speed;
                }
            }
            if (isSpeedLimited == false)
                speed = Mathf.Lerp(speed, TargetSpeed, 5 * Time.deltaTime);//Changing current speed smoothly to targetspeed
            else
                speed = Mathf.Lerp(speed, LimitedSpeed, 5 * Time.deltaTime);
            if (!isinit)
            {//Init scripts on Player Start
                init();
                isinit = true;
            }
            CheckDeath(); //Checking whether player is about to collide or is colliding with enemy or wall
            if (isStumbling == true)
            {  //Things to do in stumbling
                StumbleTimer += Time.deltaTime;

                if (UseEnemy == true)
                {
                    Enemy.transform.localPosition = Vector3.Slerp(Enemy.transform.localPosition, enemyPosition, 5f * Time.deltaTime);
                    Enemy.SetActive(true); //Bring the enemy forward
                    if (dead == false)
                    {
                        if (StumbleTimer > StumbleDuration - 1)
                            Enemy.transform.localPosition = Vector3.Slerp(Enemy.transform.localPosition, enemybackPosition, 5f * Time.deltaTime);
                    }

                }
                if (StumbleTimer > StumbleDuration)
                {
                    StumbleTimer = 0f;
                    isStumbling = false;
                }
            }
            else
            {
                if (UseEnemy == true)
                {
                    if (Enemy.activeInHierarchy == true)
                        Enemy.SetActive(false);
                }
            }


            RaycastHit hitg;
            if (StaticCamera == true && ishaking == false)
            {
                if ((Physics.Raycast(transform.position, Vector3.down, out hitg, 5f, GroundLayer)))
                {  //Assigning camera position if it is not static


                    CameraObject.transform.position = new Vector3(CameraObject.transform.position.x, hitg.point.y + CameraHeight, CameraObject.transform.position.z);
                    CameraObject.transform.localPosition = new Vector3(cameraLocalPosition.x, CameraObject.transform.localPosition.y, cameraLocalPosition.z);

                }
            }




            //Code for Jump Starts.
            if (EnableJump && (Input.GetKeyDown(KeyCode.UpArrow)) && CanJump == true && dead == false)
            {
                RealJump();
                CanJump = false;

                //GetComponent<AudioSource>().PlayOneShot(jumpSFX[Random.Range(0, jumpSFX.Length -1)]);
                SwipeControls.swipeIndex = 0;

            }



            if (!DisableAnimations && jump == true)
            { //The process of jumping

                // animator.SetBool("Jump", true);
                animator.transform.localPosition = Vector3.Slerp(animator.transform.localPosition, animatorLocalPos, Time.deltaTime * ResetLocalPositionSpeed); //Resting position while jumping

            }
            else if (jump == false && !DisableAnimations && animator.GetBool("Jump") == true)
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, Vector3.down, out hit, 100, GroundLayer))
                {
                    if (hit.distance < 1.2f)
                    {
                        CanJump = true;
                        if (!DisableAnimations)
                        {
                            //animator.SetBool("Jump", false);

                            animator.gameObject.transform.rotation = Quaternion.Slerp(animator.gameObject.transform.rotation, transform.rotation, Time.deltaTime * 50);
                            animator.transform.localPosition = Vector3.Slerp(animator.transform.localPosition, animatorLocalPos, ResetLocalPositionSpeed * Time.deltaTime);
                        }
                    }
                }
            }
            else if (jump == false)
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, Vector3.down, out hit, 100, GroundLayer))
                {
                    if (hit.distance < 1.2f)
                    {
                        CanJump = true;
                    }
                }
            }
            if (!DisableAnimations && animator.GetBool("Jump") == false)
            { //Reseting animator position after jump is complete

                animator.transform.localPosition = Vector3.Slerp(animator.transform.localPosition, animatorLocalPos, ResetLocalPositionSpeed * Time.deltaTime);

            }

            //Code for Jump Ends
            //Code for Slide Starts

            

            if (EnableSlide && (Input.GetKeyDown(KeyCode.DownArrow)) && dead == false && isSliding == false && CanJump)
            {
                //if(!DisableAnimations)
                //animator.SetBool("Slide", true);
                RealSlide();

                isSliding = true;

                //            float f = capsuleCollider.height;
                //            capsuleCollider.height /= SlideDivideFactor; //Reducing player height and the center accordingly
                //            capsuleCollider.center = new Vector3(0, -capsuleCollider.height / SlideDivideFactor -1, 0);
                //            GetComponent<AudioSource>().PlayOneShot(SlideSound);
                //swipeIndex = 0; 
                //transform.position = new Vector3 (transform.position.x, transform.position.y + 1f, transform.position.z);

                //alienModel.transform.position = new Vector3 (alienModel.transform.position.x, 10, alienModel.transform.position.z);
                //Debug.Log ("Sliding"); 

            }
            if (isSliding == true)
            {
                Slidetimer += Time.deltaTime;


                if (Slidetimer >= SlideTime)
                {
                    isSliding = false;
                    if (!DisableAnimations)
                        //animator.SetBool("Slide", false);
                        Slidetimer = 0f;
                    CanJump = true;
                    //capsuleCollider.height *= SlideDivideFactor;
                    //capsuleCollider.center = new Vector3(0, 0, 0); 
                    //capsuleCollider.enabled = true;
                    //rollCollider.enabled = false;

                }
            }

            //Code for Slide Ends
            if (isRotating == true)
            {
                GetComponent<Rigidbody>().rotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, rotateQuaternion, TurnSpeed * Time.deltaTime); //Roate the player if input detected on turns
                if (Quaternion.Angle(GetComponent<Rigidbody>().rotation, rotateQuaternion) < 1)
                    isRotating = false;
            }
            if (useTarget == true)
            {
                Target = worldmanager.CurrentTarget;

                if (Vector3.Angle(transform.forward, Target.forward) > 0.1)
                {

                    transform.forward = Vector3.Slerp(transform.forward, Target.forward, Time.deltaTime * RotSpeed); //Alinging player's forward with the forward of the current player face direction target
                }

            }

            if (!laneMovementBlocked)
                SlotSystemMove();
            //AlignToCenter();
            Quaternion q = new Quaternion();
            q.eulerAngles = new Vector3(0, 0, 0);
            if (!DisableAnimations)
                animator.transform.localRotation = q;//Reseting animator local rotation
                                                     //Anthing to do after death
            if (dead == true)
            {
                isStumbling = true;

                Instantiate(DeathParticle, transform.position, transform.rotation);

            }
            canDetectSwipe = true;
        }
        else if (CurrentGameState == GameState.Dead && UseEnemy == true)
        {///What to do in dead state
            Enemy.SetActive(true);
            Enemy.transform.localPosition = Vector3.Slerp(Enemy.transform.localPosition, enemyPosition, 5f * Time.deltaTime);
        }

    }

    //handling side animations
    public IEnumerator SideAniamtion(string key, float time)
    {

        animator.SetBool(key, true);
        yield return new WaitForSeconds(time);
        animator.SetBool(key, false);

    }

    public void GoRight()
    {

        float SideWayEnemyCheckDist = 1.5f; //Maximum Distance to check whether there's an enemy on our side
                                            // animator.SetBool("Slide", false);
        int rand = Random.Range(0, 4);
        AudioManager.Instance.PlaySound("Swipe" + (rand + 1));
        //GetComponent<AudioSource>().PlayOneShot(swipeSFX[Random.Range(0, swipeSFX.Length -1)]);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit, SideWayEnemyCheckDist, EnemyAndWallLayer))
        {
            if (hit.distance < SideWayEnemyCheckDist) //If there is a wall on the side then stumble. If already stumbling then die
            {
                ishaking = true;
                StartCoroutine("Shake");
                ReduceSpeed();
                if (isStumbling)
                {
                    //Die();
                }
                isStumbling = true;
            }
        }
        if (CurrentSlot == 0)
        {
            StartCoroutine(SideAniamtion("RightSide", 0.3f));
            //Debug.Log("Left");
            CurrentSlot = 1;

        }
        else if (CurrentSlot == -1)
        {
            StartCoroutine(SideAniamtion("RightSide", 0.3f));
            //   Debug.Log("Left");
            CurrentSlot = 0;

        }
        else if (CurrentSlot == 1)
        {//If there is a wall on the side then stumble. If already stumbling then die
            ishaking = true;
            StartCoroutine("Shake");
            ReduceSpeed();
            if (isStumbling)
            {
                //Die();
            }
            isStumbling = true;
        }

        SwipeControls.swipeIndex = 0;

    }


    public void GoLeft()
    {

        //Debug.Log ("Got left");

        float SideWayEnemyCheckDist = 1.5f; //Maximum Distance to check whether there's an enemy on our side

        int rand = Random.Range(0, 3);
        AudioManager.Instance.PlaySound("Swipe" + (rand + 1));

        //GetComponent<AudioSource>().PlayOneShot(swipeSFX[Random.Range(0, swipeSFX.Length -1)]);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.right, out hit, SideWayEnemyCheckDist, EnemyAndWallLayer))
        {
            if (hit.distance < SideWayEnemyCheckDist)
            {
                ishaking = true;
                StartCoroutine("Shake");
                ReduceSpeed();
                if (isStumbling)
                {//If there is a wall on the side then stumble. If already stumbling then die
                 // Die();
                }
                isStumbling = true;
            }
        }
        if (CurrentSlot == 0)
        {
            StartCoroutine(SideAniamtion("LeftSide", 0.3f));
            //  Debug.Log("Left");
            CurrentSlot = -1;

        }
        else if (CurrentSlot == 1)
        {
            StartCoroutine(SideAniamtion("LeftSide", 0.3f));
            // Debug.Log("Left");
            CurrentSlot = 0;

        }
        else if (CurrentSlot == -1)
        {//If there is a wall on the side then stumble. If already stumbling then die
            ishaking = true;
            StartCoroutine("Shake");
            ReduceSpeed();
            if (isStumbling)
            {
                //Die();
            }
            isStumbling = true;
        }
        SwipeControls.swipeIndex = 0;

    }



    void SlotSystemMove()
    { //Slot movement are the movement of the Player in horizontal direction
      //All sideways movement calculations are here.
        float SideWayEnemyCheckDist = 1.5f; //Maximum Distance to check whether there's an enemy on our side
        if (dead == false && isRotating == false && canChangeSlot == true)
        {
            if (TrackType == TrackTypeEnum.ThreeSlotTrack)
            {

                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    SwipeControls.instance.EffectSide.SetActive(true);
                    SideEffect.ActivateOnGoLeft();
                    GoRight();

                }
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {

                    SwipeControls.instance.EffectSide.SetActive(true);
                    SideEffect.ActivateOnGoRight();
                    GoLeft();

                }

            }
            else if (TrackType == TrackTypeEnum.FreeHorizontalMovement)
            {
                RaycastHit hit;
                bool change = true;
                if (Physics.Raycast(transform.position, transform.right * Mathf.Sign(Input.acceleration.x), out hit, SideWayEnemyCheckDist, EnemyAndWallLayer))
                {
                    if (hit.distance < SideWayEnemyCheckDist)
                    {
                        change = false;
                    }

                }//Change Sensitivity from here
                if (change == true)
                    PartialDist = (leftvalue * Input.acceleration.x * 2f);
                if (PartialDist > leftvalue)
                    PartialDist = leftvalue;
                if (PartialDist < -leftvalue)
                    PartialDist = -leftvalue;
                HorizontalMoveValue = PartialDist;
                if (SwipeControls.swipeIndex == 3 || Input.GetKeyDown(KeyCode.RightArrow))
                {//If there is a wall on the side then stumble. If already stumbling then die
                    HorizontalMoveValue = 2f;
                    ishaking = true;
                    StartCoroutine("Shake");
                    ReduceSpeed();
                    if (isStumbling)
                    {
                        //Die();
                    }
                    isStumbling = true;
                }
                else if (SwipeControls.swipeIndex == 4 || Input.GetKeyDown(KeyCode.LeftArrow))
                {//If there is a wall on the side then stumble. If already stumbling then die
                    HorizontalMoveValue = -2f;
                    ishaking = true;
                    StartCoroutine("Shake");
                    ReduceSpeed();
                    if (isStumbling)
                    {
                        //Die();
                    }
                    isStumbling = true;
                }
            }
        }
    }
    public void Respawn()  //Respawn Player at Respawn point
    {
        isSpeedLimited = false;
        canChangeSlot = true;
        Distance = 0f;
        nextChangeDistance = SpeedDist[0].Distance; //Setting the first speed as target
        TargetSpeed = SpeedDist[0].Speed;
        PlayerPoweUpsUGUI pu;
        transform.position -= BackUpOnRevivalDistance * transform.forward;
        CurrentSlot = 0;
        animator.SetBool("Dead", false);
        animator.SetBool("GameStart", true); //Play dead animation
        dead = false;
        CurrentGameState = GameState.Playing; //Change gamestate to dead
        GetComponent<Rigidbody>().isKinematic = false;
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPoweUpsUGUI>())
        {
            pu = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPoweUpsUGUI>();
            pu.ActivateState(PlayerPoweUpsUGUI.State.Invincible);
        }
    }
    void ReduceSpeed()
    { //Reduce player speed on stumble.
        if (CurrentCount == 0)
            Distance = 0f;
        else
        {
            --CurrentCount;
            if (CurrentCount >= 1)
            {
                Distance = SpeedDist[CurrentCount - 1].Distance;
                TargetSpeed = SpeedDist[CurrentCount].Speed;
                speed = SpeedDist[CurrentCount].Speed;
            }
            else
            {
                speed = SpeedDist[CurrentCount].Speed;
                TargetSpeed = SpeedDist[CurrentCount].Speed;
                Distance = 0f;
            }
        }
    }
    void AlignToCenter()
    {//Aligning to the perfect position. Here center IS NOT THE CENTER OF THE TRACK. its a refrence position to calculate the proper horizontal position 

        tempref.transform.position = Target.position;
        tempref.transform.localPosition = new Vector3(tempref.transform.localPosition.x, 0, 0);
        Vector3 value = tempref.transform.position;
        if (TrackType == TrackTypeEnum.ThreeSlotTrack)
        {
            //Calculating proper position based on slot
            if (CurrentSlot == 1)
            {
                value += transform.right * rightvalue;
            }
            else if (CurrentSlot == -1)
            {
                value -= transform.right * leftvalue;
            }
        }
        else if (TrackType == TrackTypeEnum.FreeHorizontalMovement)
        {
            value += transform.right * HorizontalMoveValue;


        }
        GetComponent<Rigidbody>().position = Vector3.Slerp(GetComponent<Rigidbody>().position, value, 10 * Time.deltaTime);

    }
    //You can change Player Turn Controls from Here
    public void Rotate(Vector3 pos, ref bool canrot, int rotType = 0, Transform target = null, Transform target1 = null)
    {
        if (dead == false && CurrentGameState == GameState.Playing)
        {
            if (rotType == 0)
            {//both side turn
                if ((Input.GetKeyDown(KeyCode.LeftArrow) || SwipeControls.swipeIndex == 4) || AutoTurn == true)
                {
                    if (target != null)
                    {
                        worldmanager.CurrentTarget = target;
                    }
                    transform.position = new Vector3(pos.x, transform.position.y, pos.z);
                    isRotating = true;
                    rotateQuaternion = GetComponent<Rigidbody>().rotation * Quaternion.Euler(0, -90, 0);
                    canrot = false;
                }
                else if ((Input.GetKeyDown(KeyCode.RightArrow) || SwipeControls.swipeIndex == 3) || AutoTurn == true)
                {
                    if (target != null)
                    {
                        worldmanager.GetComponent<WorldManager>().CurrentTarget = target1;
                    }
                    transform.position = new Vector3(pos.x, transform.position.y, pos.z);
                    isRotating = true;
                    rotateQuaternion = GetComponent<Rigidbody>().rotation * Quaternion.Euler(0, 90, 0);
                    canrot = false;
                }
            }
            if (rotType == 1)
            { //Right turn
                if ((Input.GetKeyDown(KeyCode.RightArrow) || SwipeControls.swipeIndex == 3) || AutoTurn == true)
                {
                    if (target != null)
                    {
                        worldmanager.GetComponent<WorldManager>().CurrentTarget = target;
                    }
                    transform.position = new Vector3(pos.x, transform.position.y, pos.z);
                    isRotating = true;
                    rotateQuaternion = GetComponent<Rigidbody>().rotation * Quaternion.Euler(0, 90, 0);
                    canrot = false;
                }
            }
            if (rotType == -1)
            { //Left Turn
                if ((Input.GetKeyDown(KeyCode.LeftArrow) || SwipeControls.swipeIndex == 4) || AutoTurn == true)
                {
                    if (target != null)
                    {
                        worldmanager.GetComponent<WorldManager>().CurrentTarget = target;
                    }
                    transform.position = new Vector3(pos.x, transform.position.y, pos.z);
                    isRotating = true;
                    rotateQuaternion = GetComponent<Rigidbody>().rotation * Quaternion.Euler(0, -90, 0);
                    canrot = false;
                }
            }
        }
    }
    void FixedUpdate()
    {


        if (CurrentGameState == GameState.Playing)
        {
            if (isRotating == false && dead == false)
            {
                if (!movementStopped && !bankMovement)
                    GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * speed * Time.deltaTime); //Moving the player forward 

                else if (movementStopped)
                {
                    GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * speed / 2 * Time.deltaTime); //Moving the player forward 
                }

                if (bankMovement)
                {
                    GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * speed / 4 * Time.deltaTime); //Moving the player forward 
                }

            }
            AlignToCenter(); //Aligning to the perfect position. Here center IS NOT THE CENTER OF THE TRACK. its a refrence position to calculate the proper horizontal position 

            if (dead == false)
            {
                if (jump == true)
                {
                    Jump();
                    GetComponent<Rigidbody>().useGravity = false;//Turn off gravity while jumping
                }
                else
                {
                    GetComponent<Rigidbody>().useGravity = true; //Turn on gravity after jump
                }
            }
        }

    }
    IEnumerator shadowDesapear(float timer)
    {
        shadow.SetActive(false);
        yield return new WaitForSeconds(timer);
        shadow.SetActive(true);
    }

    public void RealJump()
    {

        if (CanJump && !isSliding)
        {
            //	Debug.Log ("Real Jump");
            StartCoroutine(SideAniamtion("Jump", 0.55f));
            rigidBody.AddForce(new Vector3(0, jumpImpulse, 0), ForceMode.Impulse);
            StartCoroutine(shadowDesapear(0.45f));
            //rigidBody.velocity = Vector3.up * jumpForce;
            int rand = Random.Range(0, 3);
            AudioManager.Instance.PlaySound("Jump" + (rand + 1));
            CanJump = false;
        }

    }

    void Jump()
    {

        if (!isSliding)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, Vector3.down, out hit, 100, GroundLayer))
            {
                if (hit.distance > JumpHeight)
                {//Jumping until we get to the maximum jump height

                    jump = false;
                    JumpSpeed = CacheJumpSpeed;

                }

                //  GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.up * JumpSpeed * Time.deltaTime);
                GetComponent<Rigidbody>().AddForce(new Vector3(0, 3, 0), ForceMode.Impulse);
            }
        }
    }

    void CheckDeath()
    {

        RaycastHit hit;
        float factor = 1.2f; //Increase this value if you are getting collision misses
        Vector3 v = new Vector3(0, 0f, 0);
        if (dead == false)
        {
            if (Physics.Raycast(transform.localPosition + v - transform.forward * factor, transform.forward, out hit, 10f, EnemyAndWallLayer))
            {

                if (hit.distance < 1 * factor + Deathdist)
                {
                    if (coinsCollected > 0)
                        StartCoroutine("CoinsFall");
                    //Debug.Log("fall");
                    else
                    {
                        Die();
                    }
                }
            }
        }

    }






    public void Die()
    {//What to do on death
        ishaking = true;

        AudioManager.Instance.PlaySound("PlayerHurt");
        AudioManager.Instance.PlaySound("Explosion");

        StartCoroutine("Shake"); //Shake the screen
        if (!DisableAnimations)
        {
            animator.SetBool("Dead", true); //Play dead animation
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.6f, transform.position.z);
            animator.SetBool("GameStart", false);
        }
        dead = true;
        CurrentGameState = GameState.Dead; //Change gamestate to dead
        GetComponent<Rigidbody>().isKinematic = true;

        if (!sendData)
        {
            OnLevelFinishSendData.instance.SendDataToDataHolder();
            sendData = true;
        }

        Paliers palier = GetComponent<Paliers>();
        palier.FillDeathStats();
        palier.SetNewTotalScore();

        DieOnGround();
        shadow.SetActive(false);


    }



    IEnumerator Shake()
    { //Shake the camera
        ishaking = true;
        float elapsed = 0.0f;

        Vector3 originalCamPos = CameraObject.transform.localPosition;

        while (elapsed < ShakeDuration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / ShakeDuration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= ShakeMagnitude * damper;
            y *= ShakeMagnitude * damper;

            CameraObject.transform.localPosition = new Vector3(x + originalCamPos.x, y + originalCamPos.y, originalCamPos.z);

            yield return null;
        }
        ishaking = false;
        CameraObject.transform.localPosition = originalCamPos;
    }




    public IEnumerator CoinsFall()
    {

        yield return new WaitForEndOfFrame();
        playerScore.CurrentPlayerCoin = 0;
        StartCoroutine("StopMovement");
        CoinExplosion();
        yield return new WaitForSeconds(2f);
        coinsCollected = 0;
        coinsNotInBank = 0;
    }

    IEnumerator StopMovement()
    {

        yield return new WaitForEndOfFrame();
        movementStopped = true;
        //		lastCollidedGameobject.SetActive (false);
        yield return new WaitForSeconds(stopMovementOnCollisionTime);
        movementStopped = false;
        Die();

    }

    private void CoinExplosion()
    {

        GameObject[] totalCoins = new GameObject[(int)coinsCollected];

        int maxToInstantiate = 20;
        int toInstantiate;
        //controlling a maximum amount of coins to instantiate for performance perposes
        if (maxToInstantiate < totalCoins.Length)
        {
            toInstantiate = maxToInstantiate;
        }
        else
        {
            toInstantiate = totalCoins.Length;
        }

        for (int i = 0; i < toInstantiate; i++)
        {

            //			cubes.Length = baseLifePoints;
            //			GameObject[] particles = new GameObject[baseLifePoints];
            totalCoins[i] = coinExplosionPrefab;
            GameObject parpar = Instantiate(totalCoins[i], transform.position, Quaternion.identity) as GameObject;
            //parpar.transform.parent = transform.gameObject.transform;
            parpar.GetComponent<CoinExplosion>().explosionPower = 2;
            //particles[i].GetComponent<Rigidbody> ().AddExplosionForce (5f, transform.position, 2.5f); 


        }

    }


    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag != "Ground" && col.gameObject.tag != "Default" && col.gameObject.tag != "Banque" && col.gameObject.tag != "TramKill" && !col.gameObject.CompareTag("Slidable") && !GetComponent<OnCollisionEffect>().effectRunning && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.Invincible && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.FastRun)
        {

            if (coinsCollected > 0)
            {
                col.gameObject.SetActive(false);
                StartCoroutine("StopMovement");
                StartCoroutine("CoinsFall");
                StartCoroutine(TriggerExplosion(col.transform.position, 3f));
                GetComponent<OnCollisionEffect>().StartCoroutine("EffectofInvisibility");


                //  Debug.Log ("Hited  " + col.gameObject.name);

            }
            else
            {
                Die();
            }
        }

        if (col.gameObject.tag != "Ground" && col.gameObject.tag != "Default" && col.gameObject.tag != "Banque" && col.gameObject.tag != "TramKill" && col.gameObject.CompareTag("Slidable") && !isSliding && !GetComponent<OnCollisionEffect>().effectRunning && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.Invincible && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.FastRun)
        {
            if (coinsCollected > 0)
            {
                col.gameObject.SetActive(false);
                StartCoroutine("StopMovement");
                StartCoroutine("CoinsFall");
                StartCoroutine(TriggerExplosion(col.transform.position, 3f));
                StartCoroutine(TriggerExplosion(col.transform.position, 3f));
                GetComponent<OnCollisionEffect>().StartCoroutine("EffectofInvisibility");

                // Debug.Log("Hited  " + col.gameObject.name);

            }
            else
            {
                Die();
            }
        }


        if (col.gameObject.tag == "TramKill" && !GetComponent<OnCollisionEffect>().effectRunning && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.Invincible && powerupscontroller.CurrentPowerState != PlayerPoweUpsUGUI.State.FastRun)
            Die();

        //if (coinsCollected == 0)
        //Debug.Log (" col layer  " + col.gameObject.layer);
        if (col.gameObject.CompareTag("Slidable") && isSliding)
        {
            StartCoroutine(slidingCollisionHandler(col));
        }


    }


    IEnumerator slidingCollisionHandler(Collision col)
    {
        col.gameObject.GetComponent<MeshCollider>().enabled = false;
        yield return new WaitForSeconds(0.4f);
        col.gameObject.GetComponent<MeshCollider>().enabled = true;
    }



    public IEnumerator TriggerExplosion(Vector3 position, float time)
    {
        explosionPartical.transform.position = position;
        explosionPartical.SetActive(true);
        yield return new WaitForSeconds(time);
        explosionPartical.SetActive(false);
    }

    public void OnTriggerEnter(Collider col)
    {

        //Debug.Log ("Trigger");

        if (col.gameObject.tag == "Banque")
        {
            //	StartCoroutine ("BanqueEnter");
        }

        if (col.gameObject.tag == "CIHLetter")
        {
            CIHLetterCollided(col.gameObject);
        }

    }

    public void CIHLetterCollided(GameObject letter)
    {

        int letterIndex = letter.GetComponent<CIHLetters>().letterIndex;

        if (letterIndex == 1)
        {
            PlayerPrefs.SetInt("CLetter", 1);
        }

        if (letterIndex == 2)
        {
            PlayerPrefs.SetInt("ILetter", 1);
        }

        if (letterIndex == 3)
        {
            PlayerPrefs.SetInt("HLetter", 1);
        }

        Destroy(letter);
    }


    public IEnumerator BanqueEnter()
    {

        yield return new WaitForEndOfFrame();
        coinsSaved += coinsCollected;
        float maxCoinToBank = coinsNotInBank;

        maxCoinToBank = Mathf.Clamp(maxCoinToBank, 0, 20);

        float actuelTotalCoins = PlayerPrefs.GetInt("Total coins");
        PlayerPrefs.SetFloat("Total coins", actuelTotalCoins + coinsSaved);
        //Debug.Log ("Entering banque");	
        bankMovement = true;
        UpdateCoinsInBankUI();

        canChangeSlot = false;
        CurrentSlot = 0;

        while (maxCoinToBank > 0)
        {

            yield return new WaitForSeconds(0.1f);
            GameObject bankCoins = Instantiate(coinsToBank, transform.position, Quaternion.identity) as GameObject;
            bankCoins.GetComponent<CoinGoToBank>().guichetIndex = Random.Range(1, 5);
            maxCoinToBank -= 1;

        }

        canChangeSlot = true;

        //coinsCollected = 0;
        coinsNotInBank = 0;
        //Debug.Log ("Ended");
        bankMovement = false;
        StopCoroutine("BanqueEnter");


    }

    public IEnumerator EnableJumpAtStart()
    {

        yield return new WaitForSeconds(0.5f);
        EnableJump = true;
        StopCoroutine("EnableJumpAtStart");


    }


    public void UpdateCoinsInBankUI()
    {

        coinsInBankIUI.text = coinsSaved.ToString();

    }



    public void UpdateCoinsTakenUI()
    {

        coinsTakenUI.text = coinsCollected.ToString();

    }

    public void RealSlide()
    {

        if (CanJump && !isSliding)
        {

            StartCoroutine(shadowDesapear(0.8f));
            StartCoroutine(SideAniamtion("Slide", 0.8f));
            isSliding = true;
            //GetComponent<AudioSource> ().PlayOneShot (SlideSound);
            //GetComponent<AudioSource>().PlayOneShot(slideSFX[Random.Range(0, slideSFX.Length -1)]);
            int rand = Random.Range(0, 2);
            AudioManager.Instance.PlaySound("Slide" + (rand + 1));
            SwipeControls.swipeIndex = 0;

            //rollCollider.enabled = true;
            //capsuleCollider.enabled = false;

            //alienModel.transform.position = new Vector3 (alienModel.transform.position.x,alienModel.transform.position.y + 1, alienModel.transform.position.z);


        }

    }

    private void OnCollisionStay(Collision other)
    {
        if (!other.gameObject.CompareTag("Ground"))
        {
            CanJump = false;
        }

    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            CanJump = false;
        }
    }

    void DieOnGround()
    {

        RaycastHit hit;

        transform.position = new Vector3(transform.position.x, 2.1f, transform.position.z);

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, 0))
        {

            GameObject groundObject = hit.transform.gameObject;
            transform.position = groundObject.transform.position;
            Debug.Log(hit.transform.gameObject.name);
        }

    }



}
[System.Serializable]
public class SpeedandDistance
{
    public float Distance;//Set Distance to zero if the Speed Continues till Infinity
    public float Speed;


}



