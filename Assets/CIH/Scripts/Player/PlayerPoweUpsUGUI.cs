﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerPoweUpsUGUI : MonoBehaviour {
    public enum State {
        None,
        Invincible,
        CoinMagnet,
        FastRun
    };
    private GameObject Player;
    private PlayerControls pc;
    private PlayerScoreUGUI ps;
    public State CurrentPowerState;
    public float InvincibleDuration;
    public float InvincibleScore;
   // public GameObject InvincibleParticles;
    public float FastRunDuration;
    public float FastRunSpeed;
    public float FastRunScore;
    public float CoinMagnetDuration;
  //  public GameObject CoinMagnetParticle;
    public float CoinMagnetScore;
    public AudioClip PowerUpSound;
    private float timer = 0f;
    private State prevState;
    float t;
    float mt;
    float mtimer = 0f;
    private bool Secondpoweractive = false;
    private float CachedSpeed;
    float AnimateInterval = 0.1f;
    float animatetimer = 0f;

    public Button PowerUpButton;

    void Start() {
        Player = GameObject.FindGameObjectWithTag("Player");
        pc = Player.GetComponent<PlayerControls>();
        ps = Player.GetComponent<PlayerScoreUGUI>();

    }

    private void OnCollisionEnter(Collision other)
    {
        if (CurrentPowerState == State.Invincible)
        {
            if (other.transform.parent.gameObject.name.Contains("Bus"))
            {
                var tmp = pc.jumpImpulse;
                pc.jumpImpulse += 15f;
                pc.RealJump();
                pc.jumpImpulse = tmp;
            }
            else if (other.gameObject.CompareTag("TramKill"))
            {
                var tmp = pc.jumpImpulse;
                pc.jumpImpulse += 15f;
                pc.RealJump();
                pc.jumpImpulse = tmp;
            }
           
        }
    }


    void Update () {
		if(pc.CurrentGameState==PlayerControls.GameState.Playing){
	if(CurrentPowerState!=State.None){
                PowerUpButton.interactable = false;
			    timer+=Time.deltaTime;
                Debug.Log(timer);
				if(timer<t&&(CurrentPowerState==State.FastRun||CurrentPowerState==State.Invincible)){
					animatetimer+=Time.deltaTime;
					if(animatetimer>AnimateInterval){
						animatetimer=0f;
						//if(InvincibleParticles.activeInHierarchy==true)
						//	InvincibleParticles.SetActive(false);
						//else
						//	InvincibleParticles.SetActive(true);
					}
				}
					
                    if(timer>t){
					CurrentPowerState=State.None;
					DeactivatePrevState();
					}


			}
            PowerUpButton.interactable = true;

        }
	}
	public void ActivateState(State state){
	if(pc.CurrentGameState==PlayerControls.GameState.Playing&&CurrentPowerState==State.None){
			DeactivatePrevState();

			timer=0f;
			CurrentPowerState=state;
			switch(CurrentPowerState){
			case State.CoinMagnet:
				t=GetConsomable("Magnet");
				CoinMagnetActivate();
				prevState=State.CoinMagnet;
				break;
			case State.FastRun:
				t= GetConsomable("Invincibilité"); 
				FastRunActivate();
				prevState=State.FastRun;
				break;
			case State.Invincible:
                   t = GetConsomable("Accélération");
				InvincibilityActivate();
				t=InvincibleDuration;
				prevState=State.Invincible;
				break;

			};
            AudioManager.Instance.PlaySound("PowerUp");
            //GetComponent<AudioSource>().PlayOneShot(PowerUpSound);
		}
	}
	void DeactivatePrevState() {
		switch(prevState){
		case State.CoinMagnet:
			CoinMagnetDeactivate();
            break;
		case State.Invincible:
			InvincibilityDeactivate();
			break;
		case State.FastRun:
			FastRunDeactivate();
			break;
		};
	}
	void CoinMagnetActivate() {
		//CoinMagnetParticle.SetActive(true);
		ps.addScore(CoinMagnetScore);
	}
	void CoinMagnetDeactivate() {

		//CoinMagnetParticle.SetActive(false);
	}
	void InvincibilityActivate() {
		ps.addScore(InvincibleScore);
		//InvincibleParticles.SetActive(true);

	}
	void InvincibilityDeactivate(){
		//InvincibleParticles.SetActive(false);
	}
	void FastRunActivate(){
		ps.addScore(FastRunScore);
		pc.AutoTurn=true;
		pc.FastRun=true;
		CachedSpeed=pc.TargetSpeed;
		pc.TargetSpeed=FastRunSpeed;
		//InvincibleParticles.SetActive(true);
	}
	void FastRunDeactivate(){
		pc.TargetSpeed=CachedSpeed;
		pc.FastRun=false;
		pc.AutoTurn=false;
		//InvincibleParticles.SetActive(false);
	}

    public float GetConsomable(string consomableName)
    {
        var powers = ShopWorldMapController.instance.consomables;
        float timerConsomable = 2;
        foreach (var item in powers)
        {
            if (item.itemName == consomableName)
            {
                timerConsomable = item.timesToUseInGamePerLevel[PlayerPrefs.GetInt(item.playerPrefsItemLevel)];


            }
        }
        return timerConsomable;
    }
}
