﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Paliers : MonoBehaviour {

	public float actualCoinsCollected; 

	PlayerControls controls;

	public float palier1Coins;
	public float palier2Coins;
	public float palier3Coins;

	public bool palier1Reached;
	public bool palier2Reached;
	public bool palier3Reached;

	bool palier1Invoked;
	bool palier2Invoked;
	bool palier3Invoked;

	public Text palierText;

	public Image palier1Full;
	public Image palier2Full;
	public Image palier3Full;

	public Image deathPalier1Empty;
	public Image deathPalier2Empty;
	public Image deathPalier3Empty;

	public Sprite deathPalier1Full;
	public Sprite deathPalier2Full;
	public Sprite deathPalier3Full;

	public GameObject winScreen;

    public GameObject gui;
	GUIManagerUGUI guiManager;

    [HideInInspector]
	public int currentPalier;
	int accomplishedPalier;

	public GameObject runnerChar;

	UnlockTotalLevelStars totalLevelStars;

	public GameObject recap;

	bool recapScreenInvoked;

	public GameObject[] stars;
	public GameObject starTookText;

	public Sprite[] starTookPhrases;
	bool textCalled;

	public Text winText;
	public GameObject[] winStars;
    bool dataSent = false;


    // Use this for initialization
    void Start () {

		controls = GetComponent<PlayerControls> ();
		currentPalier = 1;

		guiManager = gui.GetComponent<GUIManagerUGUI> ();

		totalLevelStars = GetComponent<UnlockTotalLevelStars> ();
	}

    private void OnEnable()
    {
        dataSent = false;
    }
    // Update is called once per frame
    void Update () {

		CheckCoinsCollected ();
		CheckPaliers (); 
		fillBars (currentPalier);

		if (controls.CurrentGameState == PlayerControls.GameState.Dead && palier3Reached && !recapScreenInvoked) {
			//Debug.Log ("Show recap");
			//recap.SetActive (true);
			StartCoroutine ("ShowRecapScreen");
            
            recapScreenInvoked = true;
		}
        
        
	}

	IEnumerator ShowRecapScreen () {

		yield return new WaitForSeconds (1f);
		recap.SetActive (true);

	}

	void CheckCoinsCollected () {

		actualCoinsCollected = controls.coinsCollected;

	}

	public void CheckPaliers () {

		if (actualCoinsCollected >= palier1Coins && !palier1Invoked) {
			StartCoroutine (PalierReacheed (1));
			accomplishedPalier = 1;
		}

		if (actualCoinsCollected >= palier2Coins && !palier2Invoked) {
			StartCoroutine (PalierReacheed (2));
			accomplishedPalier = 2;
		}
		
		if (actualCoinsCollected >= palier3Coins && !palier3Invoked) {
			StartCoroutine (PalierReacheed (3));
            palier3Invoked = true;
			accomplishedPalier = 3;
		}
	}

	public IEnumerator PalierReacheed (int index) {

		yield return new WaitForEndOfFrame ();

		PalierReachedFeedback (index - 1);

		if (index == 1) {
			yield return new WaitForSeconds (0.3f);
			palierText.enabled = true;
			palierText.text = "Palier 1 reached !";
			palier1Invoked = true;
			palier1Reached = true;
			currentPalier = 2;
			yield return new WaitForSeconds (1.3f);
			palierText.enabled = false;
		}

		if (index == 2) {
			yield return new WaitForSeconds (0.3f);
			palierText.enabled = true;
			palierText.text = "Palier 2 reached !";
			palier2Reached = true;
			palier2Invoked = true;
			currentPalier = 3;
			yield return new WaitForSeconds (1.3f);
			palierText.enabled = false;
		}

		if (index == 3) {
            GetComponent<PlayerPoweUpsUGUI>().ActivateState(PlayerPoweUpsUGUI.State.Invincible);
            controls.animator.SetBool("RunToStop", true);
            for (int i = 0; i < 10; i++)
            {
                controls.speed = controls.speed / 2f;
                Debug.Log(controls.speed);
                
                yield return new WaitForSeconds(.1f);

                palierText.enabled = true;
                palierText.text = "Palier 3 reached !";
                palier3Reached = true;
                palier3Invoked = true;
            }   
            yield return new WaitForSeconds (0.1f);
            

            palierText.enabled = false;
			palier3Invoked = true;
			ShowWinScreen ();
            
            StopGame ();
		}
	}

	void PalierReachedFeedback (int palierIndex) {

		stars[palierIndex].SetActive(true);

		if (!textCalled) {
			StartCoroutine ("ShowStarTookText");
			textCalled = true;
		}

	}

	IEnumerator ShowStarTookText () {

		yield return new WaitForEndOfFrame ();
		starTookText.SetActive(true);
        int randText = Random.Range(0, starTookPhrases.Length);
        starTookText.GetComponent<Image>().sprite = starTookPhrases[randText];
		yield return new WaitForSeconds (1f);
		starTookText.SetActive (false);

		textCalled = false;
		StopCoroutine ("ShowStarTookText");

	}


	public void fillBars (int palierIndex) {

		if (palierIndex == 1) {
			palier1Full.fillAmount = actualCoinsCollected / palier1Coins;
		}

		if (palierIndex == 2) {
			palier2Full.fillAmount = actualCoinsCollected / palier2Coins;
		}

		if (palierIndex == 3) {
			palier3Full.fillAmount = actualCoinsCollected / palier3Coins;
		}

	}

	public void FillDeathStats () {

		if (palier1Reached) {
			deathPalier1Empty.sprite = deathPalier1Full;
		}

		if (palier2Reached) {
			deathPalier2Empty.sprite = deathPalier2Full;
		}

		if (palier3Reached) {
			deathPalier3Empty.sprite = deathPalier3Full;
		}
	}

	public void ShowWinScreen () {

        winScreen.SetActive (true);
        
        //calling recap scene
        //SceneManager.LoadScene(1);
        SetNewTotalScore ();

		winText.text = "PERFECT !";
        if (!dataSent)
        {
            OnLevelFinishSendData.instance.SendDataToDataHolder();
            dataSent = true;
        }
        //for (int i = 0; i < winStars.Length; i++) {

        //	winStars [i].SetActive (true);
        //}

    }

	public void SetNewTotalScore () {

	//totalLevelStars.AddLevelStars (accomplishedPalier);

	}


	public void StopGame () {

		controls.enabled = false;
		//runnerChar.GetComponent<Animator> ().SetBool ("EndRun", true);

		guiManager.hitGamePlayPause ();

	}

}
