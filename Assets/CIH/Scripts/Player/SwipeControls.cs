﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControls : MonoBehaviour
{
    public static SwipeControls instance;

    public GameObject EffectSide;
    public float minSwipeDist;    //Touch swipe only
    public static int swipeIndex;

    public static bool lockSwipe;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    private void Awake()
    {
        instance = GetComponent<SwipeControls>();
    }

    private void Update()
    {
        Swipe();
    }

    public void Swipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Moved && !lockSwipe)
            {
                Vector2 currentTouchPos = new Vector2(t.position.x, t.position.y);
                Debug.Log(Mathf.Abs(Vector2.Distance(currentTouchPos, firstPressPos)) > minSwipeDist);
                if (Mathf.Abs(Vector2.Distance(currentTouchPos, firstPressPos)) > minSwipeDist)
                {
                    //save ended touch 2d point
                    secondPressPos = new Vector2(t.position.x, t.position.y);

                    //create vector from the two points
                    currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                    //normalize the 2d vector
                    currentSwipe.Normalize();

                    //swipe upwards
                    if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                    {
                        PlayerControls.instance.RealJump();
                        swipeIndex = 1;
                        //Debug.Log("up swipe");
                    }
                    //swipe down
                    if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                    {
                        PlayerControls.instance.RealSlide();
                           swipeIndex = 2;
                        //Debug.Log("down swipe");
                    }
                    //swipe left
                    if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                    {
                        EffectSide.SetActive(true);
                        SideEffect.ActivateOnGoRight();
                        PlayerControls.instance.GoLeft();
                        swipeIndex = 4;
                        //Debug.Log("left swipe");
                    }
                    //swipe right
                    if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                    {
                        EffectSide.SetActive(true);
                        SideEffect.ActivateOnGoLeft();
                        PlayerControls.instance.GoRight();
                        
                        swipeIndex = 3;
                        //Debug.Log("right swipe");
                    }
                    lockSwipe = true;
                }
            }
            else if (t.phase == TouchPhase.Ended)
            {
                lockSwipe = false;

            }
        }
    }



}



