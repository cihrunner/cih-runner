﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  This script is used to generate path during the run 
 * 
 *  The main problematic was that we can't instantiate a whole track, because we don't know how many times the player will go throught hoops and earn extra points 
 *  So the idea behind this script is to coutinuously instantiate tracks when the player collides with specefic triggers
 *  These tracks are spaced between each other by a determined offset.  
 * 
 *  The amount of track instantiated every time the player hits a trigger is manually determined (by default it is 3)
 *  but once the amount of points earned by the player gets closer to the amount of points needed to complete the level, then the tracks are instantiated one by one 
 *  
 *  Then, when the difference between the amount of points earned by the player and the amount of points required to complete the level is significantly small 
 *  We finally instantiate the final track
 * 
 * 
 * */

public class GeneratePath : MonoBehaviour
{


    public GameObject firstTrack; // The last track gameObject 

    GameObject lastGenerated;

    [HideInInspector] public int generationIndex = 1; // Incremented by one everytime we generate a path

    [HideInInspector] public int disableIndex = -1; // Same thing than generationIndex, but this is used for disabling path generated
    int disableIndexModulo;// Every time this int is modulo 4, we disable a track

    [Range(1, 10)]
    public int nbLevelToGenerate = 3; // The amount of level to generate every time the player hits a trigger


    [Range(1, 25)]
    public int levelsToGenerateAtLaunch = 3; // The amount of level to generate before the player starts the game 


    [Range(0.1f, 2f)]
    public float timeBeforeLevelGenerationAtLaunch = 0.2f; // The amount of time to wait before generating another path at the begining of the game

    [Range(15, 30f)]
    public float maximumStepDifference = 20; // The difference needed between player score and the score required to complete the level to instantiate the final track


    [Range(4, 12)]
    public int stepBeforeDisablingTracks = 6;


    bool lastTrackInstantiated; // Used to determine if the last track has been instantiated

    public PooledObject[] tracksToInstantiate;
    public List<GameObject> tracks = new List<GameObject>(); // List of the tracks instantiated. This list is used to put them in the pool when they are behind the player 
    public List<int> zeroStarsTracks, oneStarTracks, twoStarsTracks, ThreeStarsTracks;
   
    [Range(0.1f, 55f)]
    public float trackOffset = 6.3f; // The offset between every track. If you wish to modify this value, consider modifying player forward speed


    [Range(1, 30)]
    public int multipleGenerationAllowed = 5; // If the player makes more steps than this number, then we'll start instantiating tracks one by one and not 3 by 3
                                              // This is done to make the final track appear smothly whenn the player reach the score needed to complete the level
                                              // We should consider making this number higher depending on the level reached out  

    CharacterController controller; // Reference to the CharacterController script
    GameObject player; // Reference to the player gameObject 

    public float trackToDestroy = -6;

    Paliers paliers;
   
    GameObject worldCube;
    private Chunk past, present, futur;
    public PlayerControls playerControls;

    void Start()
    {
        
        // Before every run, we generate a certain amount of paths
        //StartCoroutine ("GeneratePathsAtStart");

        paliers = GetComponent<Paliers>();

        InstantiateFirstPath();
       //PathGeneration();
        trackToDestroy = -tracksToInstantiate.Length + 1;
        player = this.gameObject;
    }

    void GetLevelsbyStar()
    {
        oneStarTracks = new List<int>();
        zeroStarsTracks = new List<int>();
        twoStarsTracks = new List<int>();
        ThreeStarsTracks = new List<int>();
        for (int i = 0; i < tracksToInstantiate.Length; i++)
        {
            if (tracksToInstantiate[i].starApparition == 0)
            {
                zeroStarsTracks.Add(i);
            }
            else if (tracksToInstantiate[i].starApparition == 1)
            {
                oneStarTracks.Add(i);
            }
            else if (tracksToInstantiate[i].starApparition == 2)
            {
                twoStarsTracks.Add(i);
            }
            else if (tracksToInstantiate[i].starApparition == 3)
            {
                ThreeStarsTracks.Add(i);
            }
        }
    }

    IEnumerator GeneratePathsAtStart()
    {

        // This coroutine is used to generate first paths before the player can start his run
        for (int i = 0; i < levelsToGenerateAtLaunch; i++)
        {
            yield return new WaitForSeconds(timeBeforeLevelGenerationAtLaunch);
            PathGeneration();
        }

    }


    private void OnEnable()
    {
        ObjectsPool.Instance.Objects = tracksToInstantiate;
        ObjectsPool.Instance.InstantiateObjects();
        GetLevelsbyStar();
    }

    void OnTriggerExit(Collider col)
    {

        if (col.gameObject.name.Contains("ExitTrigger") && col.transform.parent.parent.gameObject.name.Contains("Present"))
        {
            //Debug.Log ("PAth generation");
            PathGeneration();

        }

    }

    public void PathGeneration()
    {

        Chunk TmpCurrent = present;
        present = futur;
        present.chunkGO.name = "Present";
        //destroy past
        if(past != null)
        {
            DestroyTracks(past.chunkGO.gameObject);
        }
        past = TmpCurrent;
        past.chunkGO.name = "Past";
        Instantiation();
    }

    Chunk GenerateChunk(int tracksCount, string chunkName)
    {
        List<GameObject> Trackinstantiated = new List<GameObject>();
        Chunk chunk = new Chunk();
        chunk.chunkGO = new GameObject();
        chunk.chunkGO.transform.parent = GameObject.Find("World").transform;
        chunk.chunkGO.name = chunkName;
        //instantiate all tracks needed 
        Debug.Log(paliers.currentPalier);
        for (int i = 0; i < tracksCount; i++)
        {
            if(paliers.currentPalier == 0   )
            {
                int rand = Random.Range(0, zeroStarsTracks.Count);
                Trackinstantiated.Add(ObjectsPool.Instance.Activate(zeroStarsTracks[rand], Vector3.zero, Quaternion.identity));
            }
            else if(paliers.currentPalier == 1)
            {
                int rand = Random.Range(0, zeroStarsTracks.Count);
                Trackinstantiated.Add(ObjectsPool.Instance.Activate(zeroStarsTracks[rand], Vector3.zero, Quaternion.identity));
            }
            else if (paliers.currentPalier == 2)
            {
                int rand = Random.Range(0, oneStarTracks.Count );
                Trackinstantiated.Add(ObjectsPool.Instance.Activate(oneStarTracks[rand], Vector3.zero, Quaternion.identity));
            }
            else
            {
                int rand = Random.Range(0, twoStarsTracks.Count);
                Trackinstantiated.Add(ObjectsPool.Instance.Activate(twoStarsTracks[rand], Vector3.zero, Quaternion.identity));
            }
        }

        foreach (GameObject item in Trackinstantiated)
        {
            item.transform.parent = chunk.chunkGO.transform;
        }

        for (int i = 0; i < Trackinstantiated.Count-1; i++)
        {
            
            var startPoint = FindInChildren(Trackinstantiated[i].transform, "EnterTrigger");
            var endPoint = FindInChildren(Trackinstantiated[i].transform, "ExitTrigger");
            var newStartPoint = FindInChildren(Trackinstantiated[i+1].transform, "EnterTrigger");
            Trackinstantiated[i+1].transform.position = Trackinstantiated[i].transform.position - Vector3.forward * ((Trackinstantiated[i].transform.position.z - endPoint.position.z) - (Trackinstantiated[i].transform.position.z - newStartPoint.position.z)) - Vector3.right * ((Trackinstantiated[i].transform.position.x - endPoint.position.x) - (Trackinstantiated[i].transform.position.x - newStartPoint.position.x));
    
            Trackinstantiated[i].SetActive(true);
        }
        chunk.EnterTrigger = FindInChildren(Trackinstantiated[0].transform, "EnterTrigger");
        chunk.ExitTrigger = FindInChildren(Trackinstantiated[Trackinstantiated.Count - 1].transform, "ExitTrigger");
        return chunk;
    }

    void Instantiation()
    {
        var startPoint = present.EnterTrigger;
        var endPoint = present.ExitTrigger;
        futur = GenerateChunk(2, "Futur");
        // Every path is separed by an offset. We multiply this offset by the generation index to get the right zPosition of the path
        //worldCube.transform.position = new Vector3 (3.37f, 0, -91f * generationIndex);
        futur.chunkGO.transform.position = present.chunkGO.transform.position;
        futur.chunkGO.transform.SetParent(GameObject.Find("World").transform);

        var newStartPoint = futur.EnterTrigger;

        futur.chunkGO.transform.position = present.chunkGO.transform.position - Vector3.forward * ((present.chunkGO.transform.position.z - endPoint.position.z) - (present.chunkGO.transform.position.z - newStartPoint.position.z)) - Vector3.right * ((present.chunkGO.transform.position.x - endPoint.position.x) - (present.chunkGO.transform.position.x - newStartPoint.position.x)) /*- Vector3.up * ((lastGenerated.transform.position.y - endPoint.position.y) - (worldCube.transform.position.y - newStartPoint.position.y))*/;
    }

    void InstantiateFirstPath()
    {

        //generate Present
        present = new Chunk();
        present.chunkGO = new GameObject();
        GameObject go = Instantiate(firstTrack, transform.position + Vector3.down*1f, Quaternion.identity);
       
        present.EnterTrigger = FindInChildren(go.transform, "EnterTrigger");
        present.ExitTrigger = FindInChildren(go.transform, "ExitTrigger");
        present.chunkGO.transform.SetParent(GameObject.Find("World").transform);
        present.chunkGO.name = "Present";
        go.transform.parent = present.chunkGO.transform;
        Instantiation();
        GameObject.Find("BaseTrack").SetActive(false);

        // Every path is separed by an offset. We multiply this offset by the generation index to get the right zPosition of the path
        //  worldCube.transform.position =BaseTrack;
        GetComponent<PlayerControls>().Target = present.EnterTrigger;
    }

    void DestroyTracks(GameObject trackToDestroy)
    {
         // We get the track to disable from the list "tracks"
        ObjectsPool.Instance.Deactivate(trackToDestroy); // Then we disable it via the ObjectsPool 
        trackToDestroy.transform.SetParent(GameObject.Find("ObjectPool").transform);
    }


    Transform FindInChildren(Transform target, string name)
    {
        foreach (Transform child in target)
        {
            if (child.name.Contains(name))
            {
                return child;
            }
        }
        //Debug.Log("no endpoint found ");
        return null;
    }



	void CheckForBusLayer (GameObject instantiatedTrack) {

		foreach (Transform child in transform) {

			Debug.Log (child.gameObject.name);
		}


	}

    public class Chunk
    {
        public GameObject chunkGO;
        public Transform EnterTrigger, ExitTrigger;

        

    }
}