iOS Native Touch 
Sirawat Pitaksarit / Exceed7 Experiments
Contact : 5argon@exceed7.com
----

This is for other changes that is not in BREAKING_CHANGES.md
API changes occur on left most version number change according to [Semantic Versioning](https://semver.org/).

# Version 2.1.0

## New freebies : NativeTouchTracker

It is zipped in the Extra folder. Feed it a lot of `NativeTouchData` in sequence and you would be able to poll data from it just like Unity's `Input.touches`.
There are tons of preprocessor directives inside that make the whole thing works magically on both Android and iOS.
This struct is an example of how my take of interpreting native touches would go.
There are several discarded data still based on the need of my own game, you can take a look at it as a guideline for creating your own wrapper.

Requirements (And why it stays in a zip file)
- A reference to `Unity.Collections` and `Unity.Mathematics` package, making it goes well with C# Jobs, ECS, and Burst compilation.
- An understanding of how native collection works, this is a struct full of them. You also have to `Dispose` it.
- Incremental Compiler package for C#7.0 syntax.