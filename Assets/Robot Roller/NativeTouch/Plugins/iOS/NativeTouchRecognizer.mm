//#define LOG_TOUCH
//#define LOG_UNITY_MESSAGE

//With this on you will gain no new data at all, but each callback can contains overlapping data with other callbacks.
//e.g. If moved and ended callback invoked at the same time each one might have the same A and B data. (A being moved, B being ended)
//With multitouch you will get a Stationary phase for other finger staying still at that moment.
//But at the same time it is also possible to infer stationary phase without this. And anyways you cannot get
//Stationary with single touch.
//#define ALL_TOUCHES

#import "NativeTouchRecognizer.h"
#import "UnityAppController.h"
#import "UnityView.h"
#import "UnityInterface.h"

@implementation NativeTouchRecognizer

//Singleton instance variable
NativeTouchRecognizer* gestureRecognizer;

UnityView* unityView;
CGRect screenSize;
CGFloat screenScale;

struct NativeTouchData
{
    int callbackType;
    float x;
    float y;
    float previousX;
    float previousY;
    int phase;
    double timestamp;
    int pointerId; //not available but has to be there to align with Android
    int nativelyGenerated;
};

struct NativeTouchDataFull
{
    int callbackType;
    float x;
    float y;
    float previousX;
    float previousY;
    int phase;
    double timestamp;
    int pointerId; //not available but has to be there to align with Android
    int nativelyGenerated;
    
    //-- Full mode only structs --
    
    int tapCount;
    int type;
    float force;
    float maximumPossibleForce;
    float majorRadius;
    float majorRadiusTolerance;
    float altitudeAngle;
    float azimuthAngleInView;
};

typedef void (*NativeTouchFullDelegate)(NativeTouchDataFull datas);
typedef void (*NativeTouchMinimalDelegate)(NativeTouchData datas);

NativeTouchFullDelegate fullCallback;
NativeTouchMinimalDelegate minimalCallback;;
bool isFullMode;
bool isDisableUnityTouch;

+ (void) StopNativeTouch
{
    [unityView removeGestureRecognizer:gestureRecognizer];
}

+ (void) StartNativeTouch
{
    UnityAppController* uiApp = GetAppController();
    
    unityView = [uiApp unityView];
    screenScale = [[UIScreen mainScreen]scale];
    screenSize = [unityView bounds];
    
#ifdef LOG_UNITY_MESSAGE
    NSLog(@"Starting native touch - Screen : %@ Scale : %f",NSStringFromCGRect(screenSize),screenScale);
#endif
    
    if(gestureRecognizer == nil)
    {
        gestureRecognizer = [[NativeTouchRecognizer alloc] init];
    }
    [unityView addGestureRecognizer:gestureRecognizer];
    
}

+ (double) GetNativeTouchTime
{
    return [[NSProcessInfo processInfo] systemUptime];
} 

+(CGPoint) scaledCGPoint:(CGPoint)point
{
    //Retina display have /2 scale and have a smallest unit of pixel as 0.5.
    //This will multiply it back and eliminate the floating point
    
    //0,0 is at the top left of your orientation.
    
    return CGPointMake(point.x*screenScale, point.y*screenScale);
}

#ifdef LOG_TOUCH
+(void) logTouches:(NSSet<UITouch*> *) touches
{
    for(UITouch* touch in touches) {
        NSLog(@"#%d Loc:%@ Prev:%@ Radius:%f Phase:%d",
              i,
              NSStringFromCGPoint([NativeTouchRecognizer scaledCGPoint:[touch locationInView:nil]]),
              NSStringFromCGPoint([NativeTouchRecognizer scaledCGPoint:[touch previousLocationInView:nil]]),
              [touch majorRadius],
              [touch phase]);
    }
}
#endif

//This is a utility method for logging
+(const char*) encodeTouch: (UITouch*) touch
{
    CGPoint location = [NativeTouchRecognizer scaledCGPoint:[touch locationInView:nil]];
    CGPoint plocation = [NativeTouchRecognizer scaledCGPoint:[touch previousLocationInView:nil]];
    return [[NSString stringWithFormat:@"XY [%d, %d] PrevXY [%d, %d] %@",
             (int)location.x,
             (int)location.y,
             (int)plocation.x,
             (int)plocation.y,
             [NSString stringWithCString:[NativeTouchRecognizer phaseToString:[touch phase]]]
             ]UTF8String];
    //Phase : Began Moved Stationary Ended Cancelled : 0 1 2 3 4
}

+(const char*) phaseToString: (int) phase
{
    switch (phase) {
        case 0: return "Began";
            case 1: return "Moved";
            case 2: return "Stationary";
            case 3: return "Ended";
            case 4: return "Cancelled";
        default: return "???";
    }
}

+(void) sendTouchesToUnity:(NSSet<UITouch*> *) touches callbackType:(int) type
{
#ifdef LOG_UNITY_MESSAGE
    NSLog(@"-- Callback type : %@ --", [NSString stringWithCString:[NativeTouchRecognizer phaseToString:type]]);
#endif
    for(UITouch* touch in touches)
    {
#ifdef LOG_UNITY_MESSAGE
        NSLog(@"%@",[NSString stringWithCString:[NativeTouchRecognizer encodeTouch:touch] encoding:NSUTF8StringEncoding]);
#endif
        
        CGPoint location = [NativeTouchRecognizer scaledCGPoint:[touch locationInView:nil]];
        CGPoint previousLocation = [NativeTouchRecognizer scaledCGPoint:[touch previousLocationInView:nil]];
        
        if(isFullMode)
        {
            NativeTouchDataFull ntd;
            ntd.callbackType = type;
            ntd.x = (float)location.x;
            ntd.y = (float)location.y;
            ntd.previousX = (int)previousLocation.x;
            ntd.previousY = (int) previousLocation.y;
            ntd.phase = (int)[touch phase];
            ntd.timestamp = [touch timestamp];
            ntd.nativelyGenerated = 1;
            
            ntd.tapCount = (int) [touch tapCount];
            ntd.type = (int)[touch type];
            ntd.force = (float)[touch force];
            ntd.maximumPossibleForce = (float)[touch maximumPossibleForce];
            ntd.majorRadius = (float)[touch majorRadius];
            ntd.majorRadiusTolerance = (float)[touch majorRadiusTolerance];
            ntd.altitudeAngle = (float)[touch altitudeAngle];
            ntd.azimuthAngleInView = (float)[touch azimuthAngleInView:nil];
            
            fullCallback(ntd);
        }
        else
        {
            NativeTouchData ntd;
            ntd.callbackType = type;
            ntd.x = (float)location.x;
            ntd.y = (float)location.y;
            ntd.previousX = (int)previousLocation.x;
            ntd.previousY = (int) previousLocation.y;
            ntd.phase = (int)[touch phase];
            ntd.timestamp = [touch timestamp];
            ntd.nativelyGenerated = 1;
            
            minimalCallback(ntd);
        }
    }
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
#ifdef LOG_TOUCH
    [NativeTouchRecognizer logTouches:touches];
#endif
    
#ifdef ALL_TOUCHES
    [NativeTouchRecognizer sendTouchesToUnity:[event allTouches] callbackType:0];
#else
    [NativeTouchRecognizer sendTouchesToUnity:touches callbackType:0];
#endif
    
    if(!isDisableUnityTouch)
    {
        UnitySendTouchesBegin(touches, event);
    }
}

-(void) touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
#ifdef LOG_TOUCH
    [NativeTouchRecognizer logTouches:touches];
#endif
    
#ifdef ALL_TOUCHES
    [NativeTouchRecognizer sendTouchesToUnity:[event allTouches] callbackType:3];
#else
    [NativeTouchRecognizer sendTouchesToUnity:touches callbackType:3];
#endif
    
    if(!isDisableUnityTouch)
    {
        UnitySendTouchesEnded(touches, event);
    }
}

-(void) touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
#ifdef LOG_TOUCH
    [NativeTouchRecognizer logTouches:touches];
#endif
    
#ifdef ALL_TOUCHES
    [NativeTouchRecognizer sendTouchesToUnity:[event allTouches] callbackType:1];
#else
    [NativeTouchRecognizer sendTouchesToUnity:touches callbackType:1];
#endif
    
    if(!isDisableUnityTouch)
    {
        UnitySendTouchesMoved(touches, event);
    }
}

-(void) touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
#ifdef LOG_TOUCH
    [NativeTouchRecognizer logTouches:touches];
#endif
    
#ifdef ALL_TOUCHES
    [NativeTouchRecognizer sendTouchesToUnity:[event allTouches] callbackType:4];
#else
    [NativeTouchRecognizer sendTouchesToUnity:touches callbackType:4];
#endif
    
    if(!isDisableUnityTouch)
    {
        UnitySendTouchesCancelled(touches, event);
    }
}

@end

extern "C" {
    
    void _StopNativeTouch() {
        [NativeTouchRecognizer StopNativeTouch];
    }
    
    void _StartNativeTouch(NativeTouchFullDelegate fullDelegate, NativeTouchMinimalDelegate minimalDelegate, int fullMode, int disableUnityTouch) {
        
        fullCallback = fullDelegate;
        minimalCallback = minimalDelegate;
        isDisableUnityTouch = disableUnityTouch == 1 ? true : false;
        isFullMode = fullMode == 1 ? true : false;
        
        [NativeTouchRecognizer StartNativeTouch];
    }
    
    double _GetNativeTouchTime() {
        return [NativeTouchRecognizer GetNativeTouchTime];
    }
    
}
