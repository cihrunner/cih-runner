iOS Native Touch 
Sirawat Pitaksarit / Exceed7 Experiments
Contact : 5argon@exceed7.com
----

This is for users who opened the project with a new version and found their project with errors.
API changes occur on left most version number change according to [Semantic Versioning](https://semver.org/).

# Version 2.1.0

## Bug fixes

- The previous version's Android touch sends wrong touch phase in the case of 3 or more touches. It is corrected now.
- Previously iOS sends duplicated touch events and it is possible to get Stationary phase because I wrongly used `[event allTouches]` instead of `touches`
in the native side.
- Previously Android sends Cancelled phase only for the "main" touch in the pack and other touches fixed as "ambiguous Moved" (Might be Moved or implied Stationary). Now only for Cancelled phase there is a special rule so that all touches in the pack are considered Cancelled instead of Moved. Otherwise there's no way to tell which touch came together with the Cancelled touch. 

# Version 2.0.0

## Static callback signature changed to just one `struct`

It is much easier than before that you have to declare 5 `int`s or something and that was very error prone.
This struct has a nice C# property so that each platform access only its valid fields.

Also because of this Unity's `Touch` support has been dropped. The mode is just full mode or not-full mode. (minimal mode)

## Android

If you put `#if` to get Android out of the way before, now it is not needed.

## Minimal or full mode specified via `StartOption` instead

There is a new overload for `Start()` which accepts some options, `Start(startOption)`.

## The touch continues to Unity

Previous version completely disable Unity touch. This version to get the same behaviour you have to use the new `StartOption` when calling `Start(startOption)`.

## iOS now correctly returns stationary phase touch on multitouch scenario

The previous version has a bug. When you have multiple fingers down and move one finger you only get one move phase in the callback.
Now in addition, you get other fingers as stationary phase as well. Outside of multitouch scenario you can never receive a stationary phase.

In Android there is no stationary phase at all. Please read [Callback Details](http://exceed7.com/native-touch/callback-details.html) in the website.
